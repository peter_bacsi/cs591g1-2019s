import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# Constants

alpha = 1.1   # prey growth rate 
beta = 0.4    # prey death rate 
gamma = 0.4   # predator death rate 
delta = 0.1   # predator growth rate 
T_MAX = 20
dt = 1e-3
t = np.arange(0,T_MAX,dt)
x = np.zeros_like(t, dtype=np.float)
y = np.zeros_like(t, dtype=np.float)
x0 = 10       # initial prey population
y0s = [1, 2, 5, 7, 10, 12, 15] # initial predator population

fig, ax = plt.subplots(figsize=(6,6))
ax.grid(True) # Show a grid.
# ax.legend()

# Plot the curves.
for y0 in y0s: 
	# x and y are initially arrays of the type of t, filled with zeros.
    x[0], y[0] = x0, y0

    for i, tt in enumerate(t):  # enumerate returns essentially a list of pairs: ignore tt.
        if i == 0:
            continue
		# Prey grows by alpha, decreased by beta times predators (ignore natural causes)
        x[i] = x[i-1] + (alpha * x[i-1] - beta * x[i-1] * y[i-1]) * dt
		# Predator increased by delta times prey, decreased by natural causes
        y[i] = y[i-1] + (delta * x[i-1] * y[i-1] - gamma * y[i-1]) * dt

    """
	All curves go into the same plot.
	The labels are in TeX style.
	"""
    ax.plot(x, y, label='$y_0=%d$' % y0)

"""
Animation.
ax.plot() returns a list of lines, each consisting possibly of several segments.
Now just one line in the list.
"""
line, = ax.plot([], [],
				'ro') # format

def update(i):
    line.set_data(x[i], y[i])
    return line,

def init():
    line.set_data([], [])
    return line,

# Recompute a middle curve for animation.
y[0] = 12

for i, tt in enumerate(t):
	if i == 0:
		continue
	# Prey grows by alpha, decreased by beta times predators (ignore natural causes)
	x[i] = x[i-1] + (alpha * x[i-1] - beta * x[i-1] * y[i-1]) * dt
	# Predator increased by delta times prey, decreased by natural causes
	y[i] = y[i-1] + (delta * x[i-1] * y[i-1] - gamma * y[i-1]) * dt

ani = animation.FuncAnimation(fig, update,
							  np.arange(0,len(t),10), # Every 10th step shown
							  interval=1, # Delay between frames.
							  blit=True,   # For speed, don't change what is fixed. 
							  init_func=init)

fig.savefig('predator_prey.svg', bbox_inches='tight', transparent=True)
# Big and slow movie:
# ani.save('predator_prey.mp4', fps=15) 
plt.show()
