"""
===========================
The double pendulum problem
===========================

This animation illustrates the double pendulum problem.
"""

# Double pendulum formula translated from the C code at
# http://www.physics.usyd.edu.au/~wheat/dpend_html/solve_dpend.c

from numpy import sin, cos
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation

# Some constants:
G = 9.8  # acceleration due to gravity, in m/s^2
L1 = 1.0  # length of pendulum 1 in m
L2 = 1.0  # length of pendulum 2 in m
M1 = 1.0  # mass of pendulum 1 in kg
M2 = 1.0  # mass of pendulum 2 in kg

# Sampling interval.
dt = 0.05


"""
The differential equation.
derivs must be defined before calling integrate.odeint(), which uses it as a parameter.
Not that the derivative does not depend on the time t, but a second argument is required.
"""
def derivs(state, t):

	th1,w1,th2,w2 = state
	ds_dt = np.zeros_like(state)
	ds_dt[0] = w1

	del_ = th2 - th1
	den1 = (M1 + M2) * L1 - M2 * L1 * cos(del_) * cos(del_)
	ds_dt[1] = (M2 * L1 * w1 * w1 * sin(del_) * cos(del_) +
				M2 * G * sin(th2) * cos(del_) +
				M2 * L2 * w2 * w2 * sin(del_) -
				(M1 + M2) * G * sin(th1))/den1

	ds_dt[2] = w2

	den2 = (L2/L1) * den1
	ds_dt[3] = (-M2 * L2 * w2 * w2 * sin(del_) * cos(del_) +
				(M1 + M2) * G * sin(th1) * cos(del_) -
				(M1 + M2) * L1 * w1 * w1 * sin(del_) -
				(M1 + M2) * G * sin(th2))/den2

	return ds_dt

def solution_orbit():
	"""
	Integrate the 4-dimensional differential equation defined by derivs above.
	"""

	# th1 and th2 are the initial angles (degrees)
	# w1 and w2 are the initial angular velocities (degrees per second)
	th1 = 120.0
	w1 = 0.0
	th2 = -10.0
	w2 = 0.0
	# Initial 4-dimensional state.
	state = np.radians([th1, w1, th2, w2])
	t = np.arange(0.0, 20, dt)
	return integrate.odeint(derivs, state, t)

y = solution_orbit()
x1 = L1 * sin(y[:, 0]) # y[:,0] = Array of first coordinates of array of 4D vectors.
y1 = -L1 * cos(y[:, 0])
x2 = L2 * sin(y[:, 2]) + x1 
y2 = -L2 * cos(y[:, 2]) + y1


# Structures of the figure.

fig = plt.figure()
ax = fig.add_subplot(111, autoscale_on=False, xlim=(-2, 2), ylim=(-2, 2))
ax.grid()

"""
ax.plot() returns a list of lines, each consisting possibly of several segments.
Now just one line in the list.
"""
line, = ax.plot([], [],
				'o-', # format
				lw=2)

time_template = 'time = %.1fs'
time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

"""
Functions used by the FuncAnimation(): update(i) and init().
Depend implicitly on
line, time_template, time_text, x1,y1,x2,y2,
defined above.
"""
def update(i):
	# x and y coordinates of the three points of the pendulum.
    thisx = [0, x1[i], x2[i]]
    thisy = [0, y1[i], y2[i]]

    line.set_data(thisx, thisy)
    time_text.set_text(time_template % (i * dt))
    return line, time_text

def init():
    line.set_data([], [])
    time_text.set_text('')
    return line, time_text

# We could not separate into a main function all the executed parts: here is the rest.

ani = animation.FuncAnimation(fig, update, np.arange(1, len(y)),
							  interval=25, # Delay between frames.
							  blit=True,   # For speed, don't change what is fixed. 
							  init_func=init)

# ani.save('double_pendulum.mp4', fps=15)
plt.show()

