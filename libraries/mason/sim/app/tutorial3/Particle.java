package sim.app.tutorial3;

import sim.engine.*;
import sim.util.*;

public class Particle implements Steppable {
    private static final long serialVersionUID = 1;

    public boolean randomize = false;
    public int xdir;  // -1, 0, or 1
    public int ydir;  // -1, 0, or 1
    
    public Particle(int xdir, int ydir) {
        this.xdir = xdir;
        this.ydir = ydir;
    }

    public void step(SimState state) {
        Tutorial3 tut = (Tutorial3)state;
        Int2D location = tut.particles.getObjectLocation(this);

        // leave a trail
        tut.trails.field[location.x][location.y] = 1.0;
        
        // Randomize my direction if requested
        if (randomize) {
            xdir = tut.random.nextInt(3) - 1;
            ydir = tut.random.nextInt(3) - 1;
            randomize = false;
        }

        int newx = location.x + xdir;
        int newy = location.y + ydir;

        if (newx < 0) {
            newx++; 
            xdir = -xdir; 
        }
        else if (newx >= tut.trails.getWidth()) {
            newx--; 
            xdir = -xdir; 
        }
        if (newy < 0) {
            newy++ ;
            ydir = -ydir; 
        }
        else if (newy >= tut.trails.getHeight()) {
            newy--;
            ydir = -ydir;
        }

         // set my new location
        Int2D newloc = new Int2D(newx,newy);
        tut.particles.setObjectLocation(this,newloc);

        // randomize everyone at that location if need be

        Bag p = tut.particles.getObjectsAtLocation(newloc);
        if (p.numObjs > 1) {
            for(int x=0;x < p.numObjs;x++)
                ((Particle)(p.objs[x])).randomize = true;
            }
        }
    }