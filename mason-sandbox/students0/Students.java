package students0;

import sim.engine.*;

// 2.
import sim.util.*;
import sim.field.continuous.*;

// 6.
import sim.field.network.*;


public class Students extends SimState
{
    public Students(long seed)
	{
        super(seed);
	}
	
    public static void main(String[] args)
	{
		doLoop(Students.class, args);
		System.exit(0);
	}

	// 2.
	public Continuous2D yard = new Continuous2D(1.0,100,100);
    public int numStudents = 50;

	// 3.
	double forceToSchoolMultiplier = 0.01;
    double randomMultiplier = 0.1;

	// 6.
	public Network buddies = new Network(false); // false says: undirected graph.


	// 2.
	public void start()
	{
        super.start();
        yard.clear();

		// 6.
        buddies.clear();

		// 2.
        // add some students to the yard
        for(int i = 0; i < numStudents; i++)
		{
            Student student = new Student();
			// Location must be Double2D, an immutable type (to be usable as hash key).
			double x = yard.getWidth() * 0.5 + random.nextDouble() - 0.5;
			double y = yard.getHeight() * 0.5 + random.nextDouble() - 0.5;
			Double2D location = new Double2D(x,y);
            yard.setObjectLocation(student, location);
			// 6.
			buddies.addNode(student);

			// 3.
			schedule.scheduleRepeating(student);
		}

		// 6.
		// define like/dislike relationships
		// Look up Bag and Network.getAllNodes() in the classdocs
		// A Bag is a set to which you can add and from which you can remove objects,
		// but whose elements you can also index via get(i).
		Bag students = buddies.getAllNodes(); 
		for(int i = 0; i < students.size(); i++) {
			Object student = students.get(i);
			// Random friend, different from yourself:
			Object studentB = null;
			do
				studentB = students.get(random.nextInt(students.numObjs));
			while (student == studentB);
			
			double buddiness = random.nextDouble();
			buddies.addEdge(student, studentB, new Double(buddiness));
			// Random enemy.
			do
				studentB = students.get(random.nextInt(students.numObjs));
			while (student == studentB);
			buddiness = random.nextDouble();
			buddies.addEdge(student, studentB, new Double( -buddiness));
	    }
			
	}
}
