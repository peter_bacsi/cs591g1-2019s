package students0;

import sim.engine.*;
// 3.
import sim.field.continuous.*;
import sim.util.*;

// 7.
import sim.field.network.*;


// Steppable is an interface, not a class, so we say "implements", not "extends".
public class Student implements Steppable
{
	// 7.
	public static final double MAX_FORCE = 3.0;

	// 10.
	double friendsClose = 0.0; // initially very close to my friends
	double enemiesCloser = 10.0; // WAY too close to my enemies
	public double getAgitation() { return friendsClose + enemiesCloser; }

	// 3.
    public void step(SimState state)
	{
		Students students = (Students) state;
        Continuous2D yard = students.yard;
        Double2D me = students.yard.getObjectLocation(this); // the yard knows about me

		// 3.
		// Gacs: all "forces" are in fact velocities.
        MutableDouble2D sumForces = new MutableDouble2D();

		// 10.
		friendsClose = enemiesCloser = 0.0;
		
        // Add in a force towards the center of the yard.
		double x = (yard.width * 0.5 - me.x) * students.forceToSchoolMultiplier;
		double y = (yard.height * 0.5 - me.y) * students.forceToSchoolMultiplier;
        sumForces.addIn(new Double2D(x,y));
        // add a bit of randomness
		x = students.randomMultiplier * (students.random.nextDouble() * 1.0 - 0.5);
		y = students.randomMultiplier * (students.random.nextDouble() * 1.0 - 0.5);
        sumForces.addIn(new Double2D(x,y));

		// 7.
		// Add forces attracting to friends and repulsing from enemies.
		MutableDouble2D forceVector = new MutableDouble2D();
		Bag out = students.buddies.getEdges(this, null);
        int len = out.size();
        for(int buddy = 0 ; buddy < len; buddy++){
            Edge e = (Edge)(out.get(buddy));
            double buddiness = ((Double)(e.info)).doubleValue();
            // getOtherNode() returns the opposite end from me.
			// Works also with directed edges: can be the "to" end or "from" end.
            Double2D him = students.yard.getObjectLocation(e.getOtherNode(this));
            if (buddiness >= 0)  // The further I am from him the more I want to go to him...
			{
                forceVector.setTo((him.x - me.x) * buddiness, (him.y - me.y) * buddiness);
                if (forceVector.length() > MAX_FORCE) // ... up to a limit.
                    forceVector.resize(MAX_FORCE);
				// 10.
				friendsClose += forceVector.length();
			}
            else  // The nearer I am to him the more I want to get away from him, up to a limit.
			{
				// Gacs: because buddiness<0, this is a force towards me.
                forceVector.setTo((him.x - me.x) * buddiness, (him.y - me.y) * buddiness);
                if (forceVector.length() > MAX_FORCE) // Gacs: this is really about distance
                    forceVector.resize(0.0);
                else if (forceVector.length() > 0)
                    forceVector.resize(MAX_FORCE - forceVector.length());  // invert the size
				// 10.
				enemiesCloser += forceVector.length();
			}
            sumForces.addIn(forceVector);
		}

		// 3.
		/* 
		   In physics, forces cause accelerations, affecting velocities only indirectldy.
		   What is called "force" here is the velocity vector itself: the amount 
		   by which the location will change in one step.
		*/
        sumForces.addIn(me);
		// Must be immutable, so Double2D.		
        students.yard.setObjectLocation(this, new Double2D(sumForces));  
 	}

}
