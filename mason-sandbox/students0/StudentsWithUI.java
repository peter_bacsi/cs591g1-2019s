package students0;

// 4.
import sim.engine.*;
import sim.display.*;

// 5.
import sim.portrayal.continuous.*;
import sim.portrayal.simple.*;
import javax.swing.*;
import java.awt.Color;

import java.awt.Color;

// 8.
import sim.portrayal.network.*;

// 9.
import sim.field.network.*;
import sim.portrayal.*;
import java.awt.*;

// // 10.
// import sim.portrayal.*;

// 4.
public class StudentsWithUI extends GUIState
{
	// 5.
	public Display2D display;
    public JFrame displayFrame;
    ContinuousPortrayal2D yardPortrayal = new ContinuousPortrayal2D();

	// 8.
	NetworkPortrayal2D buddiesPortrayal = new NetworkPortrayal2D();
	
	// 4.
    public static void main(String[] args)
	{
		StudentsWithUI vid = new StudentsWithUI();
		Console c = new Console(vid);
		c.setVisible(true);
	}
	public StudentsWithUI() { super(new Students(System.currentTimeMillis())); }
	public StudentsWithUI(SimState state) { super(state); }
	public static String getName() { return "Student Schoolyard Cliques"; }

	// 5.
    public void setupPortrayals()
	{
        Students students = (Students) state;
        // tell the portrayals what to portray and how to portray them
        yardPortrayal.setField( students.yard );
        // yardPortrayal.setPortrayalForAll(new OvalPortrayal2D());
		// 10.
		yardPortrayal.setPortrayalForAll(new OvalPortrayal2D()
			{
				private static final long serialVersionUID = 1;
				public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
				{
					Student student = (Student)object;
					int agitationShade = (int) (student.getAgitation() * 255 / 10.0);
					if (agitationShade > 255) agitationShade = 255;
					paint = new Color(agitationShade, 0, 255 - agitationShade);
					super.draw(object, graphics, info);
				} });

		// 8.
		buddiesPortrayal.setField( new SpatialNetwork2D( students.yard, students.buddies ) );
		buddiesPortrayal.setPortrayalForAll(new SimpleEdgePortrayal2D());

		/*
		  In order to get colored edges, extend SimpleEdgePortrayal2D.
		  Look up the source code in sim.portrayal.network.
		 */
		// buddiesPortrayal.setPortrayalForAll(new SimpleEdgePortrayal2D());
		buddiesPortrayal.setPortrayalForAll(new SimpleEdgePortrayal2D()
			{
				// Each new class (even this inner, anonymous one, needs this for serialization.
				private static final long serialVersionUID = 1;

				// Override draw(): first set some instance properties, then call super.draw()/
				public void draw(Object obj, Graphics2D graphics, DrawInfo2D info)
				{
					// For adjustable thickness, set these properties:
					shape = SHAPE_LINE_ROUND_ENDS;
					setAdjustsThickness(true);

					// For adjustable color, set the toPaint and fromPain properties:
					
					// Create a color map, just as for cellular automata: 0->red, 1->blue
					sim.util.gui.SimpleColorMap myColorMap =
						new sim.util.gui.SimpleColorMap(
					  new Color[] {Color.red, Color.blue});

					// Get the edge info as a double:
					sim.field.network.Edge e = (Edge)obj;
					double val = ((Double)(e.info)).doubleValue();
					
					// red for negative, blue for positive:
					toPaint = myColorMap.getColor((int)(val+1));
					fromPaint = toPaint;

					super.draw(obj, graphics, info);
				}
				
			}
			);

		// 5.
        // reschedule the displayer
        display.reset();
        display.setBackdrop(Color.white);
		display.repaint();
	}

	public void start()
	{
		super.start();
		setupPortrayals();
	}

    public void load(SimState state)
	{
		super.load(state);
		setupPortrayals();
	}

	public void init(Controller c)
	{
        super.init(c);
        display = new Display2D(600,600,this);
        display.setClipping(false);
		displayFrame = display.createFrame();
		displayFrame.setTitle("Schoolyard Display");
		c.registerFrame(displayFrame); // so the frame appears in the "Display" list
		displayFrame.setVisible(true);
		display.attach( yardPortrayal, "Yard" );

		// 8.
		display.attach( buddiesPortrayal, "Buddies");		
	}

	public void quit()
	{
		super.quit();
		if (displayFrame!=null) displayFrame.dispose();
		displayFrame = null;
		display = null;
	}

}
