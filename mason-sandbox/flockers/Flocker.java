/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information

  Reformatted for readability by P. Gacs.
*/

package flockers;
import sim.engine.*;
import sim.field.continuous.*;
import sim.util.*;

public class Flocker implements Steppable,
								sim.portrayal.Orientable2D // used by the GUI
{
    private static final long serialVersionUID = 1;

    public Double2D loc = new Double2D(0,0);
    public Double2D velo = new Double2D(0,0); 	// Velocity defines orientation.
    public Continuous2D space;
    public Flockers flock;

    public Flocker(Double2D location, Flockers f) {
		loc = location;
		flock = f;
		space = flock.space;
	}
    
    public Bag getNeighbors()
	{
        return space.getNeighborsExactlyWithinDistance(loc,
		  flock.neighborhoodRadius, // The distance. 
		  true // Toroidal (world wrap).
													   );
	}
	
	/*
	  Required by sim.portrayal.Orientable2D. 
	  Will allow to change the orientation by a mouse.
	 */ 
    public double orientation2D() 
	{
        if (velo.x == 0 && velo.y == 0) return 0;
        return Math.atan2(velo.y, velo.x);
	}	
    public void setOrientation2D(double angle) 	// In radians.
	{
        velo = new Double2D(Math.cos(angle), Math.sin(angle));
	}
	public double getOrientation() { return orientation2D(); }

	// Dead means not moving.
    public boolean dead = false;
    public boolean isDead() { return dead; }
    public void setDead(boolean val) { dead = val; }

	// Her it is the same as velocity.
    public Double2D momentum()
	{
        return velo;
	}

	// Mean momentum of all live bag elements. 
    public Double2D meanMom(Bag b, Continuous2D space)
	{
        if (b==null || b.numObjs == 0) return new Double2D(0,0);
        
        double x = 0; 
        double y = 0;
        int i = 0;
        int count = 0; // Only count the live ones. 
        for(i=0; i<b.numObjs; i++) {
            Flocker other = (Flocker)(b.objs[i]);
            if (!other.dead) {
                Double2D m = ((Flocker)b.objs[i]).momentum();
                count++;
                x += m.x;
                y += m.y;
			}
		}
        if (count > 0) { x /= count; y /= count; }
        return new Double2D(x,y);
	}

	/*
	  Vector pointing from the current flocker
	  to the center (of gravity) of all live other flockers in the bag.
	*/
    public Double2D neigborsCenter(Bag b, Continuous2D space)
	{
        if (b==null || b.numObjs == 0) return new Double2D(0,0);
        
        double x = 0; 
        double y = 0;        

        int count = 0;
        int i = 0;
        for(i=0; i<b.numObjs; i++) {
            Flocker other = (Flocker)(b.objs[i]);
            if (!other.dead) {
                double dx = space.tdx(loc.x, other.loc.x); // tdx is toroidal distance.
                double dy = space.tdy(loc.y, other.loc.y);
                count++;
                x += dx;
                y += dy;
			}
		}
        if (count > 0) { x /= count; y /= count; }
        return new Double2D(-x, -y);
	}
 
	// Sum of repulsion forces proportional to the approx inverse distances
    public Double2D repulsion(Bag b, Continuous2D space)
	{
        if (b==null || b.numObjs == 0) return new Double2D(0,0);
        double x = 0;
        double y = 0;
        
        int i = 0;
        int count = 0;

        for(i=0; i<b.numObjs; i++) {
            Flocker other = (Flocker)(b.objs[i]);
            if (other != this )	{
                double dx = space.tdx(loc.x, other.loc.x);
                double dy = space.tdy(loc.y, other.loc.y);
                double lensquared = dx*dx+dy*dy;
                count++;
				// A repulsion force proportional to the approx inverse distance
                x += dx/(lensquared*lensquared + 1);
                y += dy/(lensquared*lensquared + 1);
			}
		}
        if (count > 0) { x /= count; y /= count; }
        return new Double2D(x, y);      
	}

    public void step(SimState state)
	{        
        if (dead) return;
        
        Bag b = getNeighbors();
            
        Double2D repul = repulsion(b, space);
        Double2D nbCtr = neigborsCenter(b, space);
        Double2D rD = flock.rndDir();
        Double2D mMom = meanMom(b, space);
        Double2D mome = momentum();

        double dx = flock.cohesionWt * nbCtr.x
			+ flock.avoidanceWt * repul.x
			+ flock.consistencyWt * mMom.x
			+ flock.randomnessWt * rD.x
			+ flock.momentumWt * mome.x;
        double dy = flock.cohesionWt * nbCtr.y
			+ flock.avoidanceWt * repul.y
			+ flock.consistencyWt* mMom.y
			+ flock.randomnessWt * rD.y
			+ flock.momentumWt * mome.y;
                
        // Scale to the jump size:
        double dis = Math.sqrt(dx*dx+dy*dy);
        if (dis>0){
            dx = (dx / dis) * flock.jump;
            dy = (dy / dis) * flock.jump;
		}
        velo = new Double2D(dx,dy);
        loc = new Double2D(
		  space.stx(loc.x + dx),
		  space.sty(loc.y + dy));
        space.setObjectLocation(this, loc);
	}
 
}
