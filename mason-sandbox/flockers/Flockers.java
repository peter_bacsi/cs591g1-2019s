/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information

  Reformatted for readability by P. Gacs.
*/

package flockers;

import sim.engine.*;
import sim.util.*;
import sim.field.continuous.*;

public class Flockers extends SimState
{
    private static final long serialVersionUID = 1;

    public Continuous2D space;

	// Weight of various components influencing the motion of each flocker:
	
	// Importance of staying close to the neighbors.
    public double cohesionWt = 0.1;
    public double getCohesionWt() { return cohesionWt; }
    public void setCohesionWt(double val) { if (val >= 0.0) cohesionWt = val; }

	// Not bumping into the neighbors.
    public double avoidanceWt = 400.0;
    public double getAvoidanceWt() { return avoidanceWt; }
    public void setAvoidanceWt(double val) { if (val >= 0.0) avoidanceWt = val; }

	// Scale of random direction change.
    public double randomnessWt = 0.05;
    public double getRandomnessWt() { return randomnessWt; }
    public void setRandomnessWt(double val) { if (val >= 0.0) randomnessWt = val; }

	// Keeping to the mean direction of neighbors.
    public double consistencyWt = 1;
    public double getConsistencyWt() { return consistencyWt; }
    public void setConsistencyWt(double val) { if (val >= 0.0) consistencyWt = val; }

	// Weight given to one's own velocity.
    public double momentumWt = 1.0;
    public double getMomentumWt() { return momentumWt; }
    public void setMomentumWt(double val) { if (val >= 0.0) momentumWt = val; }

	// Other parameters
	
    public int numFlockers = 200;
    public int getNumFlockers() { return numFlockers; }
    public void setNumFlockers(int val) { if (val >= 1) numFlockers = val; }

    public double width = 250;
    public double getWidth() { return width; }
    public void setWidth(double val) { if (val > 0) width = val; }

    public double height = 250;
    public double getHeight() { return height; }
    public void setHeight(double val) { if (val > 0) height = val; }

    public double neighborhoodRadius = 10;
    public double getNeighborhoodRadius() { return neighborhoodRadius; }
    public void setNeighborhoodRadius(double val) { if (val > 0) neighborhoodRadius = val; }

    public double deadFlockerProbability = 0.1;
    public double getDeadFlockerProbability() { return deadFlockerProbability; }
    public void setDeadFlockerProbability(double val) {
		if (val >= 0.0 && val <= 1.0) deadFlockerProbability = val;
	}
        
    public double jump = 0.7;  // how far do we move in a timestep?
    
    public Double2D[] getLocations()
	{
        if (space == null) return new Double2D[0];
        Bag b = space.getAllObjects();
        if (b==null) return new Double2D[0];
        Double2D[] locs = new Double2D[b.numObjs];
        for(int i =0; i < b.numObjs; i++)
            locs[i] = space.getObjectLocation(b.objs[i]);
        return locs;
	}

    public Double2D[] getInvertedLocations()
	// Reflected around the x=y axis.
	{
        if (space == null) return new Double2D[0];
        Bag b = space.getAllObjects();
        if (b==null) return new Double2D[0];
        Double2D[] locs = new Double2D[b.numObjs];
        for(int i =0; i < b.numObjs; i++)
		{
            locs[i] = space.getObjectLocation(b.objs[i]);
            locs[i] = new Double2D(locs[i].y, locs[i].x);
		}
        return locs;
	}

    public Flockers(long seed)
	{
        super(seed);
	}
    
    public void start()
	{
        super.start();

		/*
		  Set up the space.  It looks like a discretization
		  of about neighborhoodRadius / 1.5 is close to optimal for us.
		  Hmph,that's 16 hash lookups! I would have guessed that 
		  neighborhoodRadius * 2 (which is about 4 lookups on average)
		  would be optimal.  Go figure.
		*/
        space = new Continuous2D(neighborhoodRadius / 1.5, // The size of a "patch" (hash bucket).
		  width, height);
        
        // Make some flockers and schedule them.  A few will be dead.
        for(int x=0;x<numFlockers;x++){
            Double2D location = new Double2D(
			  random.nextDouble() * width, random.nextDouble() * height);
            Flocker flocker = new Flocker(location, this);
            if (random.nextBoolean(deadFlockerProbability))
				flocker.dead = true;
            space.setObjectLocation(flocker, location);
            schedule.scheduleRepeating(flocker);
		}
	}

    public static void main(String[] args)
	{
        doLoop(Flockers.class, args);
        System.exit(0);
	}

	// Random direction.
    public Double2D rndDir()
	{
		double angle = random.nextDouble() * 2 * Math.PI;
        return new Double2D(Math.cos(angle), Math.sin(angle));
	}
	
}
