/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information

  Reformatted for readability by P. Gacs.
*/

package flockers;

import sim.engine.*;
import sim.field.continuous.*;

import sim.display.*;
import sim.portrayal.continuous.*;
import javax.swing.*;
import java.awt.*;
import sim.portrayal.simple.*;
import sim.portrayal.SimplePortrayal2D;

public class FlockersWithUI extends GUIState
{
    public Display2D display;
    public JFrame displayFrame;

	int ourDisplaySize = 750;

    public static void main(String[] args)
	{
        new FlockersWithUI().createController();  // randomizes by currentTimeMillis
	}

    public Object getSimulationInspectedObject() { return state; }  // non-volatile

    ContinuousPortrayal2D flockersPortrayal = new ContinuousPortrayal2D();
	// To show the trail of a selected flocker:
	ContinuousPortrayal2D trailsPortrayal = new ContinuousPortrayal2D(); 
    
    public FlockersWithUI()
	{
        super(new Flockers(System.currentTimeMillis()));
	}
    
    public FlockersWithUI(SimState state) 
	{
        super(state);
	}

    public static String getName() { return "Flockers"; }

    public void start()
	{
        super.start();
        setupPortrayals();
	}

    public void load(SimState state)
	{
        super.load(state);
        setupPortrayals();
	}
        
    public void setupPortrayals()
	{
        Continuous2D space = ((Flockers)state).space;
        Object[] agentArr = space.allObjects.objs;

        flockersPortrayal.setField(space);
        trailsPortrayal.setField(space);
        
        // Random colors and four times the default size.
		int n = agentArr.length;
        for(int x=0; x<n; x++){
			/* 
			   Must be inside the loop in order to give individual colors.
			   Will not draw the child Oriented..(), uses it only to learn about selection.
			 */
            TrailedPortrayal2D trailed = new TrailedPortrayal2D( this,
			  new OrientedPortrayal2D(  
				new SimplePortrayal2D(),
				0, // offset (min length)
				4.0, // scale
				new Color(
				  128 + guirandom.nextInt(128),
				  128 + guirandom.nextInt(128),
				  128 + guirandom.nextInt(128)),
				OrientedPortrayal2D.SHAPE_COMPASS),
			  trailsPortrayal,
			  100 // Length of trail.
															 );
			/*
			  TrailedPortrayal added to BOTH field portrayals, to be selected even when moving.  
			  Needed because MovablePortrayal2D bypasses the selection mechanism,
			  but then sends selection to its own child portrayal. 
			  But we need selection sent to the simple portrayals in both field portrayals, 
			  even after moving.  So we wrap the TrailedPortrayal 
			  in both field portrayals.  It's okay because the TrailedPortrayal 
			  will only draw itself in the trailsPortrayal passed into its constructor.
			*/

			Object o = agentArr[x];
                        
            flockersPortrayal.setPortrayalForObject(o,
			  // Draws the child:
			  new AdjustablePortrayal2D(
				new MovablePortrayal2D(trailed)));
			// Only draws the trail:
            trailsPortrayal.setPortrayalForObject(o, trailed );
		}
        
        // Confine display size into our limits.
        double w = space.getWidth();
        double h = space.getHeight();
        if (w == h)	{
			display.insideDisplay.width = display.insideDisplay.height = ourDisplaySize;
		}
        else if (w > h)	{
			display.insideDisplay.width = ourDisplaySize;
			display.insideDisplay.height = ourDisplaySize * (h/w);
		}
        else if (w < h)
		{
			display.insideDisplay.height = ourDisplaySize;
			display.insideDisplay.width = ourDisplaySize * (w/h);
		}
            
        display.reset();
        display.repaint();
	}

    public void init(Controller c)
	{
        super.init(c);

        display = new Display2D(750,750,this);
        display.setBackdrop(Color.black);

        displayFrame = display.createFrame();
        displayFrame.setTitle("Flockers");
        c.registerFrame(displayFrame);
        displayFrame.setVisible(true);

        display.attach( trailsPortrayal, "Trails" );
		// This overlays the trails, because it is attached later:
        display.attach( flockersPortrayal, "Behold the Flock!" );
	}
        
    public void quit()
	{
        super.quit();
        
        if (displayFrame!=null) displayFrame.dispose();
        displayFrame = null;
        display = null;
	}
}
