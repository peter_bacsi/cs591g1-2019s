/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
  Some cosmetic changes for exposition by Peter Gacs.
*/

package tutorial1and2;

import sim.engine.*;
import sim.display.*;
import sim.portrayal.grid.*;

import java.awt.*;
import javax.swing.*;

public class CaGui extends GUIState
{
    public Display2D display;

	CaState caState;
	
    FastValueGridPortrayal2D gridPortrayal = new FastValueGridPortrayal2D();

    public CaGui()
	{
		super(new CaState(System.currentTimeMillis()));
		caState = (CaState)state;
	}
    
    public static String getName() { return "CA"; }

	/*
	  Commenting this out will cause MASON to look
	  for a file called "index.html" in the same directory -- 
	  included for consistency with the other demo applications.
	*/
	/*
	   public static Object getInfo()
	   {
	   return 
	   "<H2>Conway's Game of Life</H2>" +
	   "<p>... with a B-Heptomino"; 
	   }
	*/   

    public static void main(String[] args)
	{
        CaGui caGui = new CaGui();
        caGui.createController();
	}
    
    public void setupPortrayals()
	{
        gridPortrayal.setField(((CaState)state).grid);
        gridPortrayal.setMap(
		  new sim.util.gui.SimpleColorMap(
			new Color[] {new Color(0,0,0,0), // transparent
						 Color.blue}));
	}
    
    public void start()
	{
        super.start();      
        setupPortrayals();  
        display.reset();    
        display.repaint();  
	}
    
	// Called when you load a saved state:
    public void load(SimState state)
	{
        super.load(state);
        setupPortrayals();
        display.reset();    
        display.repaint(); 
	}

	// c is typically the console.
    public void init(Controller c)
	{
        super.init(c);
		// displayWidth, etc. have been computed in caState, even though CaState.main() can run without GUI.
        display = new Display2D(caState.displayWidth, caState.displayHeight,this);
		JFrame displayFrame = display.createFrame();
        c.registerFrame(displayFrame);   // This makes it appear in the "Display" list.
        displayFrame.setVisible(true);

        display.attach(gridPortrayal,"CA");
        display.setBackdrop(Color.white);
	}

	// Defining this method creates a Model tab in the console, showing some (JavaBeans) properties
	// the object in question: now caState.
	public Object getSimulationInspectedObject()
    {
        return caState;
    }
}
