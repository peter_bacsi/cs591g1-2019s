/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information

  Slightly modified by Peter Gacs.
*/

package tutorial1and2;

import sim.engine.*;
import sim.field.grid.*;

public class LifeCa implements Steppable
{
    private static final long serialVersionUID = 1;

    // the width and height will change later
    public IntGrid2D tempGrid = new IntGrid2D(0,0);

    public void step(SimState state)
	{
        CaState caState = (CaState)state;

		/* Since each cell will apply its transition simultaneously,
		   copy the grid into tempGrid. Note that
		   tempGrid = caState.grid 
		   would not perform a copy, just give another name to the same grid.
		*/
        tempGrid.setTo(caState.grid);
        
        int count;
        int width = tempGrid.getWidth();
        int height = tempGrid.getHeight();
        for(int x=0;x<width;x++)
            for(int y=0;y<height;y++)
			{
                count = 0;
                // Count the number of live neighbors, including yourself.
                for(int dx = -1; dx < 2; dx++)
                    for(int dy = -1; dy < 2; dy++)
                        count += tempGrid.field[tempGrid.stx(x+dx)][tempGrid.sty(y+dy)];

                // If the count is 2 or less, or 5 or higher, die
                // else if the count is 3 exactly and dead, become live.
                // Else keep your state.
                        
                if (count <= 2 || count >= 5)
                    caState.grid.field[x][y] = 0;
                else if (count == 3)
                    caState.grid.field[x][y] = 1;
			}
	}
}
