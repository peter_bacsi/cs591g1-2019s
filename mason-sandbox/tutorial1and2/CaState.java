/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information
*/

package tutorial1and2;

import sim.engine.*;
import sim.field.grid.*;
import ec.util.*;
import sim.util.TableLoader;

import java.io.*;
import java.nio.file.*;
import java.util.*;

// The class should be public, to make surely accessible by SimpleProperties.
public class CaState extends SimState
{
    private static final long serialVersionUID = 1;

	static String packageName = "tutorial1and2";
	static String configDirName = "configs";
	static String inputDirName = "inputs";
	static String configFileName = "ca.properties";

	static Path caConfigFile = Paths.get(packageName, configDirName, configFileName);

    Properties config = null; // Initialized in the constructor.
	
	String modelName; 
	static String initFileName;  // For the initial global state of the cellular automaton.
    
    public IntGrid2D grid;
    public int gridWidth = 100;
    public int gridHeight = 100;

	int displayHeightMax = 700; // If it is bigger the window will be scrolled.
	int displayHeight = displayHeightMax; 
	
	int displayWidthMax = 870;	
	int displayWidth = displayWidthMax;	

	Long seed;

	static double initDensity; // The density of 1's if the initial state is a random 0-1 state.

	// Noise levels are JavaBeans properties, so they will be displayed in the Model inspector of the GUI.
	double noiseLevel0 = 0;
	public double getNoiseLevel0() { return noiseLevel0;}
	public void setNoiseLevel0(double x) { noiseLevel0 = x;}

	double noiseLevel1 = 0;
	public double getNoiseLevel1() { return noiseLevel1;}
	public void setNoiseLevel1(double x) { noiseLevel1 = x;}
	
	
	/*
	   A sample global start state for Life.
	   In general, of course we will not hardcode the initial state.

       A b-heptomino looks like this:
        X
       XXX
       X XX
	   In the array below it looks turned sideways since the first index is the x coordinate.
	*/
		
    public static final int[][] b_heptomino = new int[][]
	{
		{0, 1, 1},
		{1, 1, 0},
		{0, 1, 1},
		{0, 0, 1}
	};
	// We could have loaded it from a file instead.
	public static final int[][] defaultPattern = b_heptomino;

	// The definition of neighborhoods is a little more complex, in order to make them
	// choosable from the inspector: it is at the end of the file.
	
    public CaState(long seed)
    {
        super(seed);
		// Don't reload the configuration.
		if (null == config){
			config = new Properties();
			loadConfig(config, caConfigFile);
		}
		propsFromConfig();
		if ("" != initFileName){
			try{
				gridFromFile(initFileName);
			} catch (IOException x) {
				x.printStackTrace();
			}
		} else {
			grid = new IntGrid2D(gridWidth, gridHeight);
			initializeGrid(defaultPattern);
		}
    }
    
    public void start()
    {
        super.start();

		switch(modelName)
		{
		case "life":
			schedule.scheduleRepeating(new LifeCa());
			break;
		case "vote":
			schedule.scheduleRepeating(new VoteCa());
			break;
		}
    }

	// With command line argument -help you can find what arguments can be given.
	// main2() below shows what would be done with the -checkpoint argument.
    public static void main(String[] args)
    {
        doLoop(CaState.class, args);
        System.exit(0);
    }

	// Stick a pattern in the center of the grid.
    void initializeGrid(int[][] pattern)
    {
		// Should fit into the grid!
		if (pattern.length > grid.field.length)
			return;  // Without complaint...
		int start = grid.field.length/2 - pattern.length/2;
        for(int x = 0; x < pattern.length; x++){
			if (pattern[x].length > grid.field[x].length)
				return;
			int start_x = grid.field[x].length/2 - pattern[x].length/2;
            for(int y = 0; y < pattern[x].length; y++)
                grid.field[x + start][y + start_x] = pattern[x][y];
		}
    }
    
    public static void loadConfig(Properties config, Path configFile)
    {
		BufferedReader configStream = null;
		try {
			configStream = Files.newBufferedReader(configFile);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		try{
			config.load(configStream);
			configStream.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
    }

	static void storeConfig(Properties config, Path savedConfigFile, String comment)
    {
		BufferedWriter configStream;
		try {
			configStream = Files.newBufferedWriter(savedConfigFile);
			config.store(configStream, comment);
			configStream.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
    }

    private void propsFromConfig()
    {
		modelName = config.getProperty("model-name", "");
		if (modelName.isEmpty()){
			System.err.println("model-name missing.");
			System.exit(1);
		}
		initFileName = config.getProperty("init-file","");
		initDensity = Double.parseDouble(config.getProperty("init-density","0.5"));

 		gridWidth = Integer.parseInt(config.getProperty("width","0"));

		String nbhoodTypeString = config.getProperty("neighborhood", "Neumann neighbors");		
		neighborhoodType = findNeighborhoodType(nbhoodTypeString);
		neighbors2d = neighborhoodTypes2d[neighborhoodType];

		noiseLevel0 = Double.parseDouble(config.getProperty("noise-level-0", "0"));
		noiseLevel1 = Double.parseDouble(config.getProperty("noise-level-1", "0"));
	}

	int gridHeightFromWidth(int width)
	{
		return (int) gridWidth*displayHeightMax/displayWidthMax;
	}

    void gridFromFile(String initFileName) throws IOException
    {
		int[][] arrayFromFile = null;
		FileInputStream initFileStream = null;
		Path initFilePath = Paths.get(packageName, configDirName, inputDirName, initFileName);
		initFileStream = new FileInputStream(initFilePath.toFile()); // throws IOException
		// catch (IOException ex) {
		// 		System.err.println(ex.getMessage());
		// 		return false;
		// }
		try {
			// The first coordinate is the column index of the picture!
			arrayFromFile = TableLoader.loadPNMFile(initFileStream);
		} catch (IOException | NullPointerException e) {
			System.err.println("loadPNMFile failed: "+e.getMessage());
			e.printStackTrace();
		}
		if (null == arrayFromFile) { // test needed to avoid warnings
			System.err.println("arrayFromFile is null");
			System.exit(1);
		}
		// Rows correspond to columns in the picture?
		gridWidth = arrayFromFile.length;
		// Compute fitting display dimensions from the grid dimensions.
		double widthFactor = (double) displayWidthMax/gridWidth;
		gridHeight = arrayFromFile[0].length;
		double heightFactor = (double) displayHeightMax/gridHeight;
		double factor = Math.min(widthFactor,heightFactor);
		displayWidth = (int)(gridWidth*factor);
		displayHeight = (int)(gridHeight*factor);
		grid = new IntGrid2D(arrayFromFile);
    }
		

	/* 
	   The following code (allowing to choose a neighborhood from the inspector) 
	   may look a little complex; you can skip reading it first.
	*/

	String neighborhoodName;
	final String[] neighborhoodNames2d =
	{
		"Neumann neighbors",
		"Moore neighbors",
		"NEC neighbors",
		"N-SW-SE neighbors"
	};

	int[][] neighbors2d;
	final int[][] neighborsNeumann = {{0,0}, {-1,0},{0,-1}, {0,1}, {1,0}};
	final int[][] neighborsMoore = {{0,0}, {-1,-1}, {-1,0}, {-1,1}, {0,-1},
									{0,1}, {1,-1}, {1,0}, {1,1} };
	final int[][] neighborsNEC = {{0,0}, {0,1}, {1,0}};
	final int[][] neighborsNsWsE = {{0,1}, {-1,-1}, {1,-1}};
	final int[][][] neighborhoodTypes2d =
	{ neighborsNeumann, neighborsMoore, neighborsNEC, neighborsNsWsE};

	/*
	  The integer neighborhoodType is a JavaBean property, so it will be displayed in the Model 
	  inspector of the GUI.  Its possible values are 0,1,2,3 (given symbolic names like NEUMANN_NEIGHBORS).
	  The function domNeghborhoodType(), returning the array neighborhoodNames2d of 4 names
	  lets the inspector show a chooser with the list of these names: choosing one assigns 
	  the corresponding integer val to neighborhoodType.
	  setNeighborhoodType(val) chooses the corresponding neighborhood from the list neighborhoodTypes2d.
	 */

	private int neighborhoodType;
	// Accessor methods should be public and not static, to make them accessible by SimpleProperties.
	public int getNeighborhoodType(){ return neighborhoodType; }
	public void setNeighborhoodType(int val)
	{
		neighborhoodType = val;
		neighbors2d = neighborhoodTypes2d[neighborhoodType];
	}
	public String[] domNeighborhoodType(){
		return neighborhoodNames2d;
	}
	
	final int NEUMANN_NEIGHBORS = 0;
	final int MOORE_NEIGHBORS = 1;
	final int NEC_NEIGHBORS = 2;
	final int NSWSE_NEIGHBORS = 3;

	public int findNeighborhoodType(String typeString)
	{
		switch(typeString){
		case "Neumann neighbors": return  NEUMANN_NEIGHBORS;
		case "Moore neighbors":	return MOORE_NEIGHBORS;
		case "NEC neighbors": return NEC_NEIGHBORS;
		case "N-SW-SE neighbors": return NSWSE_NEIGHBORS;
		}
		return 0;
	}

	//    Two other versions for main().

	public static void main1(String[] args)
	{
		// Current time for seeding the randomness generator.
        CaState caState = new CaState(System.currentTimeMillis()); 
        caState.start();
        long steps = 0;
        while(steps < 5000)
		{
            if (!caState.schedule.step(caState))
                break;
            steps = caState.schedule.getSteps();
            if (steps % 500 == 0)
                System.out.println("Steps: " + steps + " Time: " + caState.schedule.getTime());
		}
        caState.finish();
        System.exit(0);
	}
    
    
    public static void main2(String[] args)
	{
        CaState caState = null;
        
        // Should we load from checkpoint?  This little chunk of code shows the idea of
        // how to check for this.
        
        for(int x=0;x<args.length-1;x++)  // "-checkpoint" can't be the last string
            if (args[x].equals("-checkpoint"))
			{
                SimState state = SimState.readFromCheckpoint(new java.io.File(args[x+1]));
                if (state == null)
					// there was an error -- it got printed out to the screen, so just quit
                    System.exit(1);
                else if (!(state instanceof CaState))
					// Wrong simulation stored in the file!
				{
                    System.out.println("Checkpoint contains some other simulation: " + state);
                    System.exit(1);
				}
                else // we're ready to lock and load!  
                    caState = (CaState)state;
			}
        
        // ...or should we start fresh?
        if (caState==null)  // no checkpoint file requested
		{
            caState = new CaState(System.currentTimeMillis());
            caState.start();
		}
        
        long steps = 0;
        while(steps < 5000)
		{
            if (!caState.schedule.step(caState))
                break;
            steps = caState.schedule.getSteps();
            if (steps % 500 == 0)
			{
                System.out.println("Steps: " + steps + " Time: " + caState.schedule.getTime());
                String s = steps + ".CaState.checkpoint";
                System.out.println("Checkpointing to file: " + s);
                caState.writeToCheckpoint(new java.io.File(s));
			}
		}
        caState.finish();
        System.exit(0);  // make sure any threads finish up
	}

}

