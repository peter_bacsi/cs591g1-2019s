package tutorial1and2;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import ec.util.*;
import sim.util.TableLoader;

/*
  Some utilities to save to, load from or create a .png file, to be used as initial
  state of a cellular automaton.
  All variables and methods are static, so no constructor is needed.
*/
public class MakeInit
{
	private static MersenneTwisterFast random = new MersenneTwisterFast();

	static String packageName = "tutorial1and2";
	static String configDirName = "configs";
	static String inputDirName = "inputs";
	static String initConfigFileName = "init-state.properties";
	static String sourceInitFileName;
	static String initFileDirName  = Paths.get(packageName, configDirName, inputDirName).toString();
	
	static Path initConfigFile = Paths.get(packageName, configDirName, initConfigFileName);

	private static Properties initConfig = new Properties();
	private static int numStates;
	private static int regionWidth;
	private static int regionHeight = 1;
	private static int panelWidth;
	private static int panelHeight = 1;
	private static int horizShift;
	private static int vertShift;

    private static void propsFromConfig()
    {
		try {
			numStates = Integer.parseInt(initConfig.getProperty("num-states"));
		} catch (NumberFormatException nfx) {
			System.err.println(nfx.getMessage());
			System.err.println("num-states is missing.");
			System.exit(1);
		}
		try {
			regionWidth = Integer.parseInt(initConfig.getProperty("region-width"));
			System.err.println("regionWidth = "+regionWidth);
		} catch (NumberFormatException nfx) {
			System.err.println(nfx.getMessage());
			System.err.println("region-width is missing.");
			System.exit(1);
		}
		try {
			regionHeight = Integer.parseInt(initConfig.getProperty("region-height"));
		} catch (NumberFormatException nfx) {
			regionHeight = 1;
		}

		try {
			panelWidth = Integer.parseInt(initConfig.getProperty("panel-width"));
		} catch (NumberFormatException nfx) {
			System.err.println(nfx.getMessage());
			System.err.println("panel-width is missing.");
			System.exit(1);
		}
		try {
			panelHeight = Integer.parseInt(initConfig.getProperty("panel-height"));
		} catch (NumberFormatException nfx) {
			panelHeight = 1;
		}
		try {
			horizShift = Integer.parseInt(initConfig.getProperty("horiz-shift"));
		} catch (NumberFormatException nfx) {
			horizShift = (panelWidth - regionWidth)/2;
		}
		if (horizShift + regionWidth > panelWidth){
			System.err.println("Region does not fit into panel horizontally.");
			System.exit(1);
		}
		try {
			vertShift = Integer.parseInt(initConfig.getProperty("vert-shift"));
		} catch (NumberFormatException nfx) {
			vertShift = (panelHeight - regionHeight)/2;
		}
		if (vertShift + regionHeight > panelHeight){
			System.err.println("Region does not fit into panel vertically.");
			System.exit(1);
		}
		sourceInitFileName = initConfig.getProperty("source-init-file","");
	}

	static void savePanel(Path toPath, byte[] panel, int panelWidth, int panelHeight, int numStates)
	{
		if (panelWidth*panelHeight != panel.length){
			System.err.println("Panel length is not equal width times height.");
			System.exit(1);
		}
		String preamble = "P5 "+panelWidth+" "+panelHeight+" "+(numStates-1)+"\n";
		OutputStream fOut = null;
		try {
			fOut = Files.newOutputStream(toPath);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		try {
			fOut.write(preamble.getBytes());
			fOut.write(panel);
			fOut.close();
		} catch (IOException|NullPointerException x) {
			x.printStackTrace();
		}
	}

	static void flipRotateFile(Path f)
	// Uses the Unix program "convert", from ImageMagick.
	// You will probably need to change its filename before using this.
	{
		
		String convertProgFileName = Paths.get("/usr", "local", "bin", "convert").toString();
		String convertedName = f.toString().replace(".pgm", ".pgm");
		String command = convertProgFileName + " " + f.toString() + " -flip -rotate 90 "+convertedName;
		try {
			Runtime.getRuntime().exec(command);
		} catch (IOException | SecurityException x) {
			x.printStackTrace();
		}
	}

	/*
	   Create an initial CA field, a rectangular region,
	   with content either loaded from a source or randomized,
	   shifted horizontally and vertically within a rectangular panel.
	*/
	public static void main(String[] args)
    {
		CaState.loadConfig(initConfig, initConfigFile);
		propsFromConfig();

		int[][] arrayFromFile = {};

		FileInputStream sourceInitFileStream = null;
		if ("" != sourceInitFileName){
			Path sourceInitFilePath = Paths.get(initFileDirName, sourceInitFileName);
			try {
				sourceInitFileStream = new FileInputStream(sourceInitFilePath.toFile());
			} catch (IOException ex) {
				System.err.println(ex.getMessage());
				System.exit(1);
			}
			try {
				// The first coordinate is the column index of the picture.
				arrayFromFile = TableLoader.loadPNMFile(sourceInitFileStream);
			} catch (IOException | NullPointerException ex) {
				System.err.println("loadPNGFile failed: "+ex.getMessage());
				ex.printStackTrace();
			}
			if (null != arrayFromFile){
				if (regionHeight != arrayFromFile.length){
					System.err.println("Region height mismatch.");
					System.exit(1);
				}
				if (regionWidth != arrayFromFile[0].length){
					System.err.println("Region width mismatch.");
					System.exit(1);
				}
			} else {
				System.err.println("arrayFromFile is null");
				System.exit(1);
			}
		} else {
			System.err.println("No source init file.");
		}
 		byte[] panel = new byte[panelWidth*panelHeight];

		for (int i = 0; i < vertShift; i++)
			for (int j = 0; j< panelWidth; j++)
				panel[panelWidth*i+j] = (byte)0;
		if (null == arrayFromFile) {
			System.err.println("arrayFromFile is null");
			System.exit(1);
		} else {
			for (int i = vertShift; i < vertShift + regionHeight; i++){
				for (int j = 0; j < horizShift; j++)
					panel[panelWidth*i+j] = (byte)0;
				if ("" == sourceInitFileName)
					for (int  j = horizShift; j < horizShift + regionWidth; j++){
						panel[i*panelWidth + j] = (byte)(random.nextInt(2));
						// System.err.print(panel[i*panelWidth + j]);
					}
				else 
					for (int  j = horizShift; j < horizShift + regionWidth; j++){
						try {
							panel[i * panelWidth + j] = (byte) arrayFromFile[i - vertShift][j - horizShift];
						} catch (NullPointerException n) {
							System.err.println("arrayFromFile is malformed");
							System.exit(1);
						}
					}
				for (int j = horizShift + regionWidth; j < panelWidth; j++)
					panel[panelWidth*i+j] = (byte)0;
			}
		}
		for (int i = vertShift + regionHeight; i < panelHeight; i++)
			for (int j = 0; j< panelWidth; j++)
				panel[panelWidth*i+j] = (byte)0;

		// "island-random" should be changed to something meaningful
		// in case there was a source init file.
		String initFileName = "island-random"+numStates+"-"+
			regionWidth+"x"+regionHeight+"in"+panelWidth+"x"+panelHeight+".pgm";
		Path initFilePath = Paths.get(initFileDirName,initFileName);
		savePanel(initFilePath, panel, panelWidth, panelHeight, numStates);
	}
}
