package tutorial1and2;

import sim.engine.*;
import sim.field.grid.*;

import java.util.*;

public class VoteCa implements Steppable
{
    private static final long serialVersionUID = 1L;

    // the width and height will change later
    public IntGrid2D tempGrid = new IntGrid2D(0,0);

    private double noiseLevel0 = 0;
	public double getNoiseLevel0(){ return noiseLevel0;}
	public void setNoiseLevel0(double level){
		noiseLevel0 = level;
	}
	
    private double noiseLevel1 = 0;
	public double getNoiseLevel1(){ return noiseLevel1;}
	public void setNoiseLevel1(double level){
		noiseLevel1 = level;
	}

	private double threshold;

	public void step(SimState state)
	{
		CaState caState = (CaState)state;
		// First copy the grid into tempGrid.
		tempGrid.setTo(caState.grid);

		noiseLevel0 = caState.noiseLevel0;
		noiseLevel1 = caState.noiseLevel1;
		
		int[][] field = caState.grid.field;
		int[][] tempField = tempGrid.field;
		int width = tempGrid.getWidth();
		int height = tempGrid.getHeight();
		int count;
		int vertFlip = -1;  // If you want the y coordinates to start from the bottom.
		// threshold must be set here if it needs to be updated with neighborhood change.
		threshold = caState.neighbors2d.length/2;
		for(int x=0;x<width;x++)
			for(int y=0;y<height;y++)
			{
				count = 0;
				for (int[] neighbor : caState.neighbors2d)
					count += tempField[tempGrid.stx(x+neighbor[0])][tempGrid.sty(y+vertFlip*neighbor[1])];
				if (threshold<count) field[x][y] = 1;
				else field[x][y] = 0;
				Double noise = caState.random.nextDouble();
				if (noiseLevel0 > noise)
					field[x][y] = 0;
				if (noiseLevel1 > 1 - noise)
					field[x][y] = 1;
			}
	}
}
