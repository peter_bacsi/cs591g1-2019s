// Gacs: this project was started by Umang Desai.

package backyard1;

import sim.engine.*;
import sim.field.grid.*;
import ec.util.*;
import sim.util.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class Backyard extends SimState
{
 	private static final long serialVersionUID = 1L;

	Long seed;

	// Fully qualified name since sim.util.Properties also exists.
	java.util.Properties config = null;

	// Shows whether the model was restarted.
	boolean restart = false;
	public boolean getRestart() { return restart; }
	public void setRestart(boolean val) { restart = val; }

	static String resourceDirName = "workspace";
	static String outputDirName = "outputs";
	static String savedChartFileName = "saved-chart.pdf";
	static Path configFile = Paths.get(resourceDirName, "backyard.properties");

    int gridHeight = 200;
	public int getGridHeight(){ return gridHeight; }
	public void setGridHeight(int height){ gridHeight = height; }
	
    int gridWidth = 200;
	public int getGridWidth(){ return gridWidth; }
	public void setGridWidth(int width){ gridWidth = width; }

	// If a property is not initialized, the inspector gets null pointer exception.
	// Do a Refresh in the inspector before actually using it.

	// This grid stores the grass height at each of its position.
    public DoubleGrid2D gridForGrass = new DoubleGrid2D(gridWidth,gridHeight);
	// This grid stores the rabbits (most positions have none).
    public SparseGrid2D gridForBackyard = new SparseGrid2D(gridWidth, gridHeight);

	double[] grassDensity = new double[gridWidth*gridHeight]; // for a histogram
	public double[] getGrassDensity()
	{
		double[][] grassField = gridForGrass.field;
        for(int j = 0; j < gridHeight; j++) {
            for (int k = 0; k < gridWidth; k++) {
				grassDensity[j*gridHeight + k] = grassField[j][k];
			}
        }
		return grassDensity;
	}
		
	Bag rabbits = new Bag();

    private int numRabbitsInit = 3; //default value
	public int getNumRabbitsInit(){ return numRabbitsInit;}
	public void setNumRabbitsInit(int num){ numRabbitsInit = num;}

    private int numRabbits = numRabbitsInit;
	public int getNumRabbits(){ return rabbits.numObjs;}

	private double grassGrowth = 0.00035;
	public double getGrassGrowth(){ return grassGrowth; }
	public void setGrassGrowth(double g){ grassGrowth = g; }

	private int calcPeriod = 30; // When to recompute the total grass.
	public int getCalcPeriod(){ return calcPeriod; }
	public void setCalcPeriod(int g){ calcPeriod = g; }

	// The initial grass is randomly distributed between min and max.
	private double grassMin = 0;
	private double grassMax = 1;

	// The maximum energy a rabbit can accumulate by eating.
	static double energyMax = 4;
    public double getEnergyMax() { return energyMax; }
    public void setEnergyMax(double energyMax) {
        if (energyMax >= 0) {
            Backyard.energyMax = energyMax;
        }
    }

	static double energyLoss = 0.2; // In each jump.
	public double getEnergyLoss() { return energyLoss; }
	public void setEnergyLoss(double c) { energyLoss = c; }

    static int rabbitJumpSize = 2;
	public int getRabbitJumpSize(){ return rabbitJumpSize; }
	public void setRabbitJumpSize(int j){ if (j>= 0) rabbitJumpSize = j; }

	static double reproductionRate = 0.01;
    public double getReproductionRate() { return reproductionRate; }
    public void setReproductionRate(double reproductionRate) {
        if (reproductionRate >= 0) {
            Backyard.reproductionRate = reproductionRate;
        }
    }

	double grassTotApprox = 0;
	int historySumNumRabbits = 0;
	int historySumSqNumRabbits = 0;
	double historyStdevRabbits = 0;

	/*
	  Load the initialization from the preferences here and not in start(), so that
	  they don't get overridden in start() if you changed them.
	*/
    public Backyard(long seed)
	{
        super(seed);
		this.seed = seed;

		// Don't reload the configuration.
		if (null == config){
			config = new java.util.Properties();
			loadConfig(config, configFile);
		}
		// In start() we only initialize again if Restart is checked.
		initialState();
    }

    public static void main(String[] args)
    {
        doLoop(Backyard.class, args);
        System.exit(0);
    }

	/* 
	   scheduleRepeating() returns an object of type Stoppable, that can be used
	   to stop the repetition: we will use it when killing a rabbit.
	*/
	void scheduleRabbits()
	{
		int n = rabbits.numObjs;
		for (int i=0; i<n; i++){
			Rabbit r = (Rabbit) rabbits.get(i);
			r.stopper = schedule.scheduleRepeating(r); 
		}
	}

	/*
	  Ordinary removal from a bag involves a linear search.
	  To avoid it, each rabbit remembers its index in the bag, and then
	  uses the procedure below.
	  This hack relies on knowing that an index will change only
	  when a removed rabbit is replaced with the last one.
	  (I did not measure whether this makes a difference in the present application.)
	*/
	void removeFastFromRabbits(Rabbit r)
	{
		int n = rabbits.numObjs;
		if (1 >= n)
			rabbits.remove(r);
		else {
			int i = r.index;
			((Rabbit)rabbits.getValue(n-1)).index = i;
			rabbits.remove(i);
		}
	}

	void addNewRabbit(Int2D location)
	{
		Rabbit r = new Rabbit();
		r.index = rabbits.numObjs;
		r.backyard = this;
		r.stopper = schedule.scheduleRepeating(r); 
		rabbits.add(r);
		gridForBackyard.setObjectLocation(r, location);
	}

	// Override as needed.
	void initialState()
	{
		propsFromConfig();
        gridForBackyard = new SparseGrid2D(gridWidth, gridHeight);
        gridForGrass = new DoubleGrid2D(gridWidth, gridHeight);
		grassDensity = new double[gridWidth * gridHeight];
		
		rabbits = new Bag();

        for(int i=0 ; i < numRabbitsInit ; i++) {
			addNewRabbit(new Int2D(random.nextInt(gridWidth),random.nextInt(gridHeight)));
        }
		double[][] grassField = gridForGrass.field;
        for(int j = 0; j < gridHeight; j++) {
            for (int k = 0; k < gridWidth; k++) {
				grassField[j][k] = grassMin + (grassMax - grassMin)*random.nextDouble();
			}
        }
	}

	// In case you want to do something before restarting:

	void restart(){} 

    public void start() {

		grassTotApprox = 0;
		historySumNumRabbits = 0;
		historySumSqNumRabbits = 0;
		historyStdevRabbits = 0;
		
		boolean restart = getRestart();
		if (restart && 0L != seed)
			random.setSeed(seed); // to restart exactly.

	    super.start();

		if (restart){
			initialState();  // This schedules the rabbits, too.
			restart();
		} else scheduleRabbits();

		/* 
		   An agent performing some global tasks, like growing the grass
		   and computing some sums.
		   An anonymous class modeled on the decreaser of trails of particles 
		   in Tutorial 3.
		*/
        Steppable grower = new Steppable()  
			{
				private static final long serialVersionUID = 1;

				public void step(SimState state)
				{
					int steps = (int)state.schedule.getSteps();

					numRabbits = rabbits.numObjs;
					historySumNumRabbits += numRabbits;
					historySumSqNumRabbits += numRabbits * numRabbits;

					gridForGrass.add(grassGrowth);
					gridForGrass.upperBound(1);
					
					// Recalculate some things only every calcPeriod steps.
					if (0 == (int)state.schedule.getSteps() % calcPeriod) {
						double meanRabbits = historySumNumRabbits / (1.0 + steps);
						double meanSqRabbits = historySumSqNumRabbits / (1.0 + steps);
						historyStdevRabbits = Math.sqrt(meanSqRabbits - meanRabbits*meanRabbits);
						
						int grassHeight = gridForGrass.getHeight();;
						int grassWidth = gridForGrass.getWidth();;
						double[][] grassField = gridForGrass.field;

						grassTotApprox = 0;
						for(int j = 0; j < grassHeight; j++) {
							for (int k = 0; k < grassWidth; k++) 
								grassTotApprox += grassField[j][k];
						}
					}
				}
			};
        schedule.scheduleRepeating(Schedule.EPOCH, // The start of the schedule
		  2, // Ordering (to execute after those with ordering 1).
		  grower, // What to schedule.
		  1 // Time between repeat steps.
								   );		
    }

	// Called at the end of the simulation.
    public void finish()
	{
		// kill() can also be called by one of the agents to finish everything.
        kill();  
	}

    private void propsFromConfig()
    {
		numRabbitsInit = Integer.parseInt(config.getProperty("init-num-rabbits", "10"));
		rabbitJumpSize = Integer.parseInt(config.getProperty("rabbit-jump-size", "2"));
	}

    public static void loadConfig(java.util.Properties config, Path configFile)
    {
		System.out.println("Loading "+ configFile.toString());
		BufferedReader configStream = null;
		try {
			configStream = Files.newBufferedReader(configFile);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		try{
			config.load(configStream);
			configStream.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
    }

	static void storeConfig(java.util.Properties config, Path savedConfigFile, String comment)
    {
		BufferedWriter configStream;
		try {
			configStream = Files.newBufferedWriter(savedConfigFile);
			config.store(configStream, comment);
			configStream.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
    }

}
