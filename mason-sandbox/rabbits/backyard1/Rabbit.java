package backyard1;

import sim.engine.*;
import sim.util.*;
import ec.util.*;

public class Rabbit implements Steppable
{
	private static final long serialVersionUID = 1L;

	Backyard backyard; // Gets assigned by Backyard.addNewRabbit()	

	int index; // in the bag called rabbits.
	//  A Stoppable is returned when scheduling: can remove the agent from the schedule.
	Stoppable stopper; 

	private double energy = 0.5;  // If it decreases below 0 the rabbit dies.
	
	// These are just renamed from Backyard, the main model.
	static double energyMax; 
	static double energyLoss;
    static int jumpSize;
	static double reproductionRate;

    Rabbit()
	{
		energyMax = Backyard.energyMax;
		energyLoss = Backyard.energyLoss;
		jumpSize = Backyard.rabbitJumpSize;
		reproductionRate = Backyard.reproductionRate;
	}

    public void step(SimState state)
	{
        Int2D location = backyard.gridForBackyard.getObjectLocation(this);

		energy -= energyLoss;
		// Eat all grass at the location that you can (after the energy loss).
		double newEnergy = energy + backyard.gridForGrass.field[location.x][location.y];
		if (energyMax < newEnergy) newEnergy = energyMax;
		// Decrease the grass by the amount actually eaten:
        backyard.gridForGrass.field[location.x][location.y] -= (newEnergy - energy);
		if (newEnergy < 0) {
			die();
			return; // needed to really lose the rabbit.
		}
		energy = newEnergy;
		if (reproduce(energy))
			backyard.addNewRabbit(location);
		jump(location);
    }

	private void jump(Int2D location)
	{
        int x = location.x;
        int y = location.y;

        int newx = x - jumpSize + backyard.random.nextInt(2*jumpSize+1);
        int newy = y - jumpSize + backyard.random.nextInt(2*jumpSize+1);

		// Explicit world wrap.
        if (newx < 0)
            newx += backyard.gridWidth;
        if (newx >= backyard.gridWidth)
            newx -= backyard.gridWidth;
        if (newy < 0)
            newy += backyard.gridHeight;
        if (newy >= backyard.gridHeight)
            newy -= backyard.gridHeight;
        backyard.gridForBackyard.setObjectLocation(this, new Int2D(newx, newy));
	}

	// Reproduce with a probability depending on the energy.
	boolean reproduce(double energy)
	{
		return (backyard.random.nextDouble() < reproductionRate*energy/energyMax);
	}

	private void die()
	{
		stopper.stop(); // Remove from the schedule.
		backyard.removeFastFromRabbits(this);
		backyard.gridForBackyard.remove(this);
	}

	
}
