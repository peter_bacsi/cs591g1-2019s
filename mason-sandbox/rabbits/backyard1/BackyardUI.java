package backyard1;

import sim.engine.*;
import sim.display.*;
import sim.portrayal.grid.*;
import sim.portrayal.*;
import sim.util.*;
import sim.util.media.chart.*;
import sim.util.media.*;
import sim.util.gui.Utilities;

import org.jfree.data.xy.*; // For time series.

import java.io.*;
import java.nio.file.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.lang.*;
import java.text.*; // for DecimalFormat

public class BackyardUI extends GUIState
{
	private static final long serialVersionUID = 1L;

    public static String getName() { return "Rabbits and Grass Demo"; }

    public static Object getInfo()
	{
        return "<H2>Rabbits and Grass</H2>"+
			"<p> There is a field of grass consisting of identical-sized patches. "+
			"Rabbits can eat the grass which then slowly regrows. "+
			"<p> Each rabbit eats the grass it finds at its location (increasing its energy), "+
			"and then jumps randomly within a certain distance. "+
			"Each jump decreases the enegy. "+
			"The rabbit gives birth to a new rabbit with some probability "+
			"depending on its energy. "+
			"The rabbit dies if it runs out of energy."
			;
    }

	private int displayWidth = 700;
	private int displayHeight = 700;
	private double grassTotScaling = 100;

	/* 
	   Mason provides a way to create a chart from the user interface 
	   (click at the key symbols). Here we implement a way to do it from the program.
	 */
	org.jfree.data.xy.XYSeries rabbitSeries;    // The data series we'll add to.
	org.jfree.data.xy.XYSeries grassSeries;    
	org.jfree.data.xy.XYSeries historyStdevRabbitsSeries;    
    sim.util.media.chart.TimeSeriesChartGenerator chart;

    public Display2D display;
    public JFrame displayFrame;
	public JFrame chartFrame;

    SparseGridPortrayal2D rabbitsPortrayal = new SparseGridPortrayal2D();
    FastValueGridPortrayal2D grassPortrayal = new FastValueGridPortrayal2D("Grass");

	Backyard bky;

    public BackyardUI()
	{
		super(new Backyard(System.currentTimeMillis()));
		bky = (Backyard)state;
	}

    public static void main(String[] args)
    {
		new BackyardUI().createController();
	}

    public void start()
	{
        super.start();
        setupPortrayals();

		chart.removeAllSeries();
        rabbitSeries = new org.jfree.data.xy.XYSeries("Rabbits series", false);
        grassSeries = new org.jfree.data.xy.XYSeries("Grass series", false);
        historyStdevRabbitsSeries = new org.jfree.data.xy.XYSeries(
		  "Stdev Rabbits series", false);
        chart.addSeries(rabbitSeries, null);
        chart.addSeries(grassSeries, null);
        SeriesAttributes sa = chart.addSeries(historyStdevRabbitsSeries, null);
		((TimeSeriesAttributes)sa).setStrokeColor(Color.black);

		/* 
		   An anonymous agent to update the chart.
		   Look at the documentation of the scheduleRepeatingImmediatelyAfter() 
		   method of GUIState.
		*/
        scheduleRepeatingImmediatelyAfter(new Steppable()
            {
				private static final long serialVersionUID = 1L;

				public void step(SimState state)
				{
					double x = state.schedule.getSteps(); 
					double y = bky.rabbits.numObjs;
					double z = bky.grassTotApprox / grassTotScaling;
					double s = bky.historyStdevRabbits;
               
					if (x >= Schedule.EPOCH && x < Schedule.AFTER_SIMULATION)
					{
						rabbitSeries.add(x, y, true);
						grassSeries.add(x, z, true);
						historyStdevRabbitsSeries.add(x, s, true);
						/*
						  We're in the model thread now, so we shouldn't directly
						  update the chart.  Instead we request an update to occur the next
						  time that control passes back to the Swing event thread.
						*/
						chart.updateChartLater(state.schedule.getSteps());
					}
				}
			});
	}

	// Called when the current simulation ends.
	public void finish()
	{
		state.finish();
        super.finish();

		chart.update(state.schedule.getSteps(), true);
		chart.repaint();
        chart.stopMovie();
	}

	// Called by the console when the user ends everything.
    public void quit() {
        super.quit();
		
        if (displayFrame != null)
			displayFrame.dispose();
        displayFrame = null;
        display = null;
		
		chart.update(state.schedule.getSteps(), true);
        chart.repaint();
        chart.stopMovie();
        if (chartFrame != null)	chartFrame.dispose();
        chartFrame = null;
    }

    public void init(Controller c) {
        super.init(c);
		// Display.
        display = new Display2D(displayWidth,displayHeight,this);
        displayFrame = display.createFrame();
        c.registerFrame(displayFrame);
        displayFrame.setVisible(true);
        display.setBackdrop(Color.black);
        display.attach(grassPortrayal,"Grass");
        display.attach(rabbitsPortrayal,"Rabbit");
		// Chart.
		chart = new sim.util.media.chart.TimeSeriesChartGenerator();
		chart.setTitle("Number of rabbits in time");
		chart.setYAxisLabel("Rabbits");  
		chart.setXAxisLabel("Time");
		chartFrame = chart.createFrame(true);
		// The two commands below have an effect on the size, but not on the position.
		chartFrame.setPreferredSize(new Dimension(600,350));
		chartFrame.pack();  
		chartFrame.setVisible(true);
		c.registerFrame(chartFrame);
		// Command box.
		c.registerFrame(createCommandsFrame());

		/*
		  For registration, the frame title is helpful.
		  Without the title, you don't see much of it in the Displays tab.

		  The console automatically moves itself to the right of all
		  of its registered frames -- you might wish to rearrange the
		  location of all the windows, including the console, at this
		  point in time.  The positions will be remembered in the preferences
		  for the next time you call the app, if you push the Save button in the 
		  Displays tab. (The location of the preferences file is system-dependent.)
		*/
    }

    public void load(SimState state) {
        super.load(state);
        setupPortrayals();
    }

    public void setupPortrayals()
	{
        grassPortrayal.setField(bky.gridForGrass);
        grassPortrayal.setMap(new sim.util.gui.SimpleColorMap(0, 1, Color.black, Color.green)); 
        rabbitsPortrayal.setField(bky.gridForBackyard);
        rabbitsPortrayal.setPortrayalForAll(
		  new sim.portrayal.simple.OvalPortrayal2D(Color.white, 2)
											);
        display.reset();
        display.repaint();
    }

    public Object getSimulationInspectedObject() { return state; }
	// // We need this only if we want to see some inspected values continuously updated.
	// public Inspector getInspector()
    // {
	// 	Inspector i = super.getInspector();
	// 	//	i.setVolatile(true);
	// 	return i;
    // }		

	/*
	  If you add commands to the commands frame you will need to expand the 
	  frame manually to show.
	  Then in the Displays tab say Save, so this arrangement will be remembered
	  for the next time.
	*/
	JFrame createCommandsFrame()
	{
		JFrame f = new JFrame("Commands");
		Container p = f.getContentPane();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

		// Other buttons can be added here.
		p.add(UtilsForUI.commandButton("Print stdev", "printStdev", this));
		p.add(chart2pdfButton("Save chart", f));
	
		f.pack();
		f.setVisible(true);
		return f;
	}
	
	// Must be public to be called from the utils package.
	public void printStdev()
	{
		DecimalFormat fm = new DecimalFormat("#.##");
		System.out.println("Rabbits history standard deviation = " +
		  fm.format(bky.historyStdevRabbits) +
		  " at step "+state.schedule.getSteps());
	}

	public JButton chart2pdfButton(String buttonName, Frame f)
	{
		JButton button = new JButton(buttonName);
        button.addActionListener(new ActionListener()
            {
				public void actionPerformed(ActionEvent e)
                {
					chart2pdf(f);
                }
            });
		return button;
	}

	/*
	  f is the frame from which this is called, to be supplied as
	  an argument required to the file dialog.
	*/
	public void chart2pdf(Frame f) 
	{	
		FileDialog fd = new FileDialog(f, "Save the chart to a pdf", FileDialog.SAVE);
		fd.setFile(Backyard.savedChartFileName);
		fd.setDirectory(
		  Paths.get(Backyard.resourceDirName, Backyard.outputDirName).toString());
        fd.setVisible(true);
		String fileName = fd.getFile();
		if (null == fileName)
			return;
		Path p = null;
		try	{
			p = Paths.get(fd.getDirectory(),
			  Utilities.ensureFileEndsWith(fileName, ".pdf"));
			sim.util.media.PDFEncoder.generatePDF(chart.getChart(), chart.getWidth(),
			  chart.getHeight(),
			  p.toFile());
		} 
		catch (Exception e) {
			String s = p.toString();
			Utilities.informOfError(e, 
			  "Error while saving the chart to file " + (s == null ? " " : s), null);
		}
	}	
}
