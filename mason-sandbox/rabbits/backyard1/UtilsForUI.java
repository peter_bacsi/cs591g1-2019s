package backyard1;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.lang.*;
import java.lang.reflect.*;

public class UtilsForUI {

	public UtilsForUI(){}
	
	/* 
	   The button executes a method of obj with no argument or output.
	   Returns the frame of the button.
	*/
	public static JButton commandButton(String buttonName, String methodName, Object obj)
	{
		JButton button = new JButton(buttonName);
		Method method = null;
		try{
			method = obj.getClass().getDeclaredMethod(methodName);
		} catch (NoSuchMethodException x){
			x.printStackTrace();
		}
		final Method fMethod = method;
        button.addActionListener(new ActionListener()
            {
				public void actionPerformed(ActionEvent e)
                {
                    try {
                        fMethod.invoke(obj);
                    } catch (IllegalAccessException|InvocationTargetException x) {
                        x.printStackTrace();
                    }
                }
            });
		return button;
	}

}
