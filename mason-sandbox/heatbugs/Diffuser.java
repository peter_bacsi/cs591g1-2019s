/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information

  Gacs: reformatted for readability.
*/

package heatbugs;

import sim.engine.*;
import sim.field.grid.*;

/** This agent decreases evaporates and diffuses all the heat at each time step.   See the comments in Diffuser.java for a tutorial on how to speed up Java many-fold in classes such as Diffuser. */

public /*strictfp*/ class Diffuser implements Steppable
{
    private static final long serialVersionUID = 1;

	/*
	  Donald Knuth said that premature optimization is the root of all evil.
	  So let us just concentrate on the method where 90% of the time is spent.
	  
	  Here are some variations of the diffusion portion (dumping the
	  evaporated, diffused stuff into heatbugs.valgrid2), starting with a
	  simple and slow one.
	*/
    public void step(SimState state)
	{
        HeatBugs heatbugs = (HeatBugs)state;
        
		/*
		  Naive way: double-loop through the grid.  
		  For each position, compute the average temperature of the eight neighbors.
		  The getMooreLocations function respects toroidal boundaries.
		  Set (in the new grid 'valgrid2') the new value to include some evaporation,
		  plus some of this diffused average.
		*/

		// double average;
		// sim.util.IntBag xNeighbors = new sim.util.IntBag(9);
		// sim.util.IntBag yNeighbors = new sim.util.IntBag(9);
          
		// for(int x=0;x< heatbugs.valgrid.getWidth();x++)
		// 	for(int y=0;y< heatbugs.valgrid.getHeight();y++){
		// 		average = 0.0;
		// 		// fill in xNeighbors, yNeighbors
		// 		// Gacs: Implicitly assumes that xNeighbor, yNeighbors come in the same order.
		// 		heatbugs.valgrid.getMooreLocations(x,y,1,Grid2D.TOROIDAL,
		// 		true, // include origin
		// 		  xNeighbors,yNeighbors);
                
		// 		for(int i = 0 ; i < xNeighbors.numObjs; i++)
		// 			average += heatbugs.valgrid.get(xNeighbors.get(i), yNeighbors.get(i));
		// 		average /= 9.0;
          
		// 		heatbugs.valgrid2.set(x,y, heatbugs.evaporationRate * (
		// 		    heatbugs.valgrid.get(x,y) + 
		// 		    heatbugs.diffusionRate * (average - heatbugs.valgrid.get(x,y))
		// 		  ));
		// 	}
		
        /* ----------------------------------------------------------------------
		
		   Quite slow for a variety of reasons.  First, 
		   the getMooreLocations loads and stores integers into a large array, 
		   then clears them out, all so we can just do some simple computation with 
		   them.  Since we already know exactly what locations we want to grab, why 
		   are we asking the system to do it for us?  We can do it much faster by 
		   doing a double for-loop over the nine locations directly.  To handle 
		   toroidal stuff, we use the tx() and ty() functions which perform 
		   wrap-around computations for us automagically.
                
		   Second, we're using get() and set() functions on grids.  This is fine -- 
		   it's not too slow -- but it's a bit faster to just access the underlying 
		   arrays directly, which is acceptable in MASON.  So combining these two, 
		   we'll get a somewhat faster, and slightly less naive, approach:
		*/


		// double average;
                
		// for(int x=0;x< heatbugs.valgrid.getWidth();x++)
		// 	for(int y=0;y< heatbugs.valgrid.getHeight();y++) {
		// 		average = 0.0;
		// 		for(int dx=-1; dx< 2; dx++)
		// 			for(int dy=-1; dy<2; dy++) {
		// 				int xx = heatbugs.valgrid.tx(x+dx);
		// 				int yy = heatbugs.valgrid.ty(y+dy);
          
		// 				average += heatbugs.valgrid.field[xx][yy];
		// 			}
		// 		average /= 9.0;
                        
		// 		heatbugs.valgrid2.field[x][y] = heatbugs.evaporationRate * (
		// 			heatbugs.valgrid.field[x][y] + 
		// 			heatbugs.diffusionRate * (average - heatbugs.valgrid.field[x][y])
        //          );
		// 	}

        /* ----------------------------------------------------------------------
		   The first thing to note is that tx and ty are slower than stx and sty.
		   You use tx and ty when you expect to have to wrap toroidal values that are
		   way out there.  stx and sty are fine if your toroidal values are never off
		   the board more than one width's worth in the x direction or one height's worth 
		   in the y direction.
        
		   Also, you don't want to compute getWidth() every loop, and CERTAINLY don't want
		   to compute getHeight() width times every loop!  So now we can write:
		*/

		/*
		double average;
		final int _gridWidth = heatbugs.valgrid.getWidth();
		final int _gridHeight = heatbugs.valgrid.getHeight();
                
		for(int x=0;x< _gridWidth;x++)
			for(int y=0;y< _gridHeight;y++)	{
				average = 0.0;
				for(int dx=-1; dx< 2; dx++)
					for(int dy=-1; dy<2; dy++) {
						int xx = heatbugs.valgrid.stx(x+dx);
						int yy = heatbugs.valgrid.sty(y+dy);
                                                                
						average += heatbugs.valgrid.field[xx][yy];
					}
				average /= 9.0;
                        
				heatbugs.valgrid2.field[x][y] = heatbugs.evaporationRate * (
					heatbugs.valgrid.field[x][y] + 
                     heatbugs.diffusionRate * (average - heatbugs.valgrid.field[x][y])
					 );
			}
		*/



        /* ----------------------------------------------------------------------
		   We set _gridWidth and _gridHeight to be final mostly to remind us that
		   they don't change.  Although Java COULD take advantage of
		   them being final to improve optimization, right now it doesn't.
		   
		   Next issue: Instance data lookups, even for data in YOUR
		   instance, are much slower than locals.
		*/

		/*
		final DoubleGrid2D _valgrid = heatbugs.valgrid;
		final double[][] _valgrid_field = heatbugs.valgrid.field;
		final double[][] _valgrid2_field = heatbugs.valgrid2.field;
		final int _gridWidth = heatbugs.valgrid.getWidth();
		final int _gridHeight = heatbugs.valgrid.getHeight();
		final double _evaporationRate = heatbugs.evaporationRate;
		final double _diffusionRate = heatbugs.diffusionRate;
                
		double average;
		for(int x=0;x< _gridWidth;x++)
			for(int y=0;y< _gridHeight;y++) {
				average = 0.0;
				for(int dx=-1; dx< 2; dx++)
					for(int dy=-1; dy<2; dy++){
						int xx = _valgrid.stx(x+dx);
						int yy = _valgrid.sty(y+dy);
						average += _valgrid_field[xx][yy];
					}
				average /= 9.0;
				_valgrid2_field[x][y] = _evaporationRate * 
					(_valgrid_field[x][y] + _diffusionRate * 
					  (average - _valgrid_field[x][y]));
			}
		*/
                        
                        
        /* ----------------------------------------------------------------------
		   That was a BIG jump in speed!

		   Now, if you say foo[x][y], Java
		   first looks foo[x], itself a one-dimensional array (call it A).
		   Then it looks up A[y].  This is TWO array bounds
		   checks and you have to load the arrays into cache.
		   Get an almost 2x speedup keeping the needed arrays around.
        
		   Keep _valgrid[x] and _valgrid2[x].
		   Since we're diffusing, we also need the _valgrid[x-1] and _valgrid[x+1] rows.  
           Call these:  _valgrid[x]     ->      _current
                        _valgrid[x-1]   ->      _past
                        _valgrid[x+1]   ->      _next
                        _valgrid2[x]    ->      _put
        
			The next iteration only looks up the new _next and the new _put.
            
			Last, we'll get rid of the inner for-loop and just hand-code the nine
			lookups.
		*/      
                
		/*
		final DoubleGrid2D _valgrid = heatbugs.valgrid;
		final double[][] _valgrid_field = heatbugs.valgrid.field;
		final double[][] _valgrid2_field = heatbugs.valgrid2.field;
		final int _gridWidth = _valgrid.getWidth();
		final int _gridHeight = _valgrid.getHeight();
		final double _evaporationRate = heatbugs.evaporationRate;
		final double _diffusionRate = heatbugs.diffusionRate;

		double average;

		// Initialize:
		double[] _past = _valgrid_field[_valgrid.stx(-1)];
		double[] _current = _valgrid_field[0];
		double[] _next;
		double[] _put;
                
		for(int x=0;x< _gridWidth;x++) {
			_next = _valgrid_field[_valgrid.stx(x+1)];
			_put = _valgrid2_field[_valgrid.stx(x)];
                    
			for(int y=0;y< _gridHeight;y++)	{
				// go across top
				average = (
				_past[_valgrid.sty(y-1)] + 
				_past[_valgrid.sty(y)] // sty() not needed here
				+ _past[_valgrid.sty(y+1)] +
				_current[_valgrid.sty(y-1)] + _current[_valgrid.sty(y)] 
				+ _current[_valgrid.sty(y+1)] + 
				_next[_valgrid.sty(y-1)] + _next[_valgrid.sty(y)] + 
				_next[_valgrid.sty(y+1)]
				) / 9.0;

				_put[y] = _evaporationRate * (
					_current[y] + _diffusionRate * (average - _current[y])
					);
			}
			_past = _current;
			_current = _next;
		}
		*/


        /* ----------------------------------------------------------------------
		   Well, that bumped us up a lot!  But we can double our speed yet again,
		   simply by cutting down on the number of times we call sty().  It's called
		   NINE TIMES for each stx().  Note that we even call _valgrid.sty(y) when
		   we _know_ that y is always within the toroidal range, that's an easy fix;
		   just replace _valgrid.sty(y) with y.  We can also replace _valgrid.sty(y-1)
		   and _valgrid.sty(y+1) with variables which we have precomputed so they're not
		   each recomputed three times (Java's optimizer isn't very smart).  Last we
		   can avoid computing _valgrid.sty(y-1) at all (except once) -- just set it
		   to whatever y was last time.
        
		   We could speed this up a bit more, avoiding
		   calls to sty(y+1) and reducing unnecessary calls to stx, but it won't buy
		   us the giant leaps we're used to by now.  We could also replace the stx and sty
		   with our own functions where we pass in the width and height as local variables,
		   and that's actually a fair bit faster also, 
		   but again, it's not a giant improvement.
		*/

        final DoubleGrid2D _valgrid = heatbugs.valgrid;
        final double[][] _valgrid_field = heatbugs.valgrid.field;
        final double[][] _valgrid2_field = heatbugs.valgrid2.field;
        final int _gridWidth = _valgrid.getWidth();
        final int _gridHeight = _valgrid.getHeight();
        final double _evaporationRate = heatbugs.evaporationRate;
        final double _diffusionRate = heatbugs.diffusionRate;

        double average;
        
        double[] _past = _valgrid_field[_valgrid.stx(-1)];
        double[] _current = _valgrid_field[0];
        double[] _next;
        double[] _put;
        
        int yminus1;
        int yplus1;
        
        for(int x=0;x< _gridWidth;x++){
            _next = _valgrid_field[_valgrid.stx(x+1)];
            _put = _valgrid2_field[_valgrid.stx(x)];
            
            yminus1 = _valgrid.sty(-1);     // initialized
            for(int y=0;y< _gridHeight;y++) {
                // go across top
                yplus1 = _valgrid.sty(y+1);
                average = (_past[yminus1] + _past[y] + _past[yplus1] +
				  _current[yminus1] + _current[y] + _current[yplus1] +
				  _next[yminus1] + _next[y] + _next[yplus1]) / 9.0;

                // load the new value into HeatBugs.this.valgrid2
                _put[y] = _evaporationRate * (
				  _current[y] + _diffusionRate * (average - _current[y])
											  );
                yminus1 = y;
			}
            _past = _current;
            _current = _next;
		}
        // Last step, not to forget:
		heatbugs.valgrid.setTo(heatbugs.valgrid2);
	}


	
	/* ----------------------------------------------------------------------
	   On a multiprocessor machine, you can speed this up further by
	   dividing the work among two processors.  
	   We do that over in ThreadedDiffuser.java
	   
	   You can also avoid some of the array bounds checks by using linearized
	   double arrays -- that is, using a single array but computing the double
	   array location yourself.  That way you only have one bounds check instead
	   of two.  This is how, for example, Repast does it.  This is certainly a
	   little faster than two checks.  We use a two-dimensional array because a
	   linearized array class is just too cumbersome to use in Java right now, 
	   what with all the get(x,y) and set(x,y,v) instead of just saying foo[x][y].  
	   Plus it turns out that for SMALL (say 100x100) arrays, the double array is 
	   actually *faster* because of cache advantages.
        
	   At some point in the future Java's going to have to fix the lack of true
	   multidimensional arrays.  It's a significant speed loss.  IBM has some proposals
	   in the works but it's taking time.  However their proposals are for array classes.
	   So allow me to suggest how we can do a little syntactic sugar to make that prettier.
	   The array syntax for multidimensional arrays should be foo[x,y,z] and for
	   standard Java arrays it should be foo[x][y][z].  This allows us to mix the two:
	   a multidimensional array of Java arrays for example:  foo[x,y][z].  Further we
	   should be allowed to linearize a multidimensional array, accessing all the elements
	   in row-major order.  The syntax for a linearized array simply has empty commas:
	   foo[x,,]
	*/
	
}

