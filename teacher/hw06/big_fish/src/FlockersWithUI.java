/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information

  Reformatted for readability by P. Gacs.
*/

package big_fish;

import java.io.*;
import java.nio.file.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import sim.engine.*;
import sim.field.continuous.*;

import sim.display.*;
import sim.portrayal.*;
import sim.portrayal.simple.*;
import sim.portrayal.continuous.*;
import sim.portrayal.SimplePortrayal2D;

import sim.util.*;
import sim.util.media.chart.*;
import sim.util.media.*;
import sim.util.gui.Utilities;

import org.jfree.data.xy.*; // For time series.


public class FlockersWithUI extends GUIState
{
    public Display2D display;
    public JFrame displayFrame;
	public JFrame chartFrame;

	int ourDisplaySize = 750;

	org.jfree.data.xy.XYSeries bigFishEnergySeries;    
    sim.util.media.chart.TimeSeriesChartGenerator chart;

    public static void main(String[] args)
	{
        new FlockersWithUI().createController();  // randomizes by currentTimeMillis
	}

    public Object getSimulationInspectedObject() { return state; }  // non-volatile

    ContinuousPortrayal2D flockersPortrayal = new ContinuousPortrayal2D();
	// Show the trail of a selected flocker:
	ContinuousPortrayal2D trailsPortrayal = new ContinuousPortrayal2D(); 
	ContinuousPortrayal2D bigFishPortrayal = new ContinuousPortrayal2D(); 
	// ContinuousPortrayal2D bigFishPortrayalCenter = new ContinuousPortrayal2D(); 
    
    public FlockersWithUI()
	{
        super(new Flockers(System.currentTimeMillis()));
	}
    
    public FlockersWithUI(SimState state) 
	{
        super(state);
	}

    public static String getName() { return "Flockers"; }

    public void start()
	{
        super.start();
        setupPortrayals();

		Flockers flock = (Flockers)state;

		chart.removeAllSeries();
        bigFishEnergySeries = new org.jfree.data.xy.XYSeries("Big Fish Energy Series", false);

		// Chart setup.
        SeriesAttributes sa = chart.addSeries(bigFishEnergySeries, null);
		((TimeSeriesAttributes)sa).setStrokeColor(Color.black);

        scheduleRepeatingImmediatelyAfter(new Steppable()
            {
				private static final long serialVersionUID = 1L;

				public void step(SimState state)
				{
					long s = state.schedule.getSteps(); 
					if (s >= Schedule.EPOCH && s < Schedule.AFTER_SIMULATION && 0 == s % 100)
					{
						bigFishEnergySeries.add(s, flock.bigFish.energy, true);
						/*
						  We're in the model thread now, so we shouldn't directly
						  update the chart.  Instead we request an update to occur the next
						  time that control passes back to the Swing event thread.
						*/
						chart.updateChartLater(s);
					}
				}
			});
	}

    public void load(SimState state)
	{
        super.load(state);
        setupPortrayals();
	}
        
    public void setupPortrayals()
	{
		Flockers flock = (Flockers)state;
        Continuous2D space = flock.fineSpace;
        Object[] agentArr = space.allObjects.objs;
		Object bf = flock.bigFish;

        flockersPortrayal.setField(space);
        trailsPortrayal.setField(space);
        bigFishPortrayal.setField(flock.bfSpace);
        // bigFishPortrayalCenter.setField(flock.bfSpace);
        
		OrientedPortrayal2D bp =  new OrientedPortrayal2D(
		  new SimplePortrayal2D(),
		  0, // offset (min length)
		  20.0, // scale
		  Color.white, // will be changed
		  OrientedPortrayal2D.SHAPE_COMPASS);

		bigFishPortrayal.setPortrayalForObject(bf,
		  // This will draw the child:
		  new AdjustablePortrayal2D(
			new MovablePortrayal2D(bp)));

        // bigFishPortrayalCenter.setPortrayalForObject(bf,
		//   new sim.portrayal.simple.OvalPortrayal2D(Color.red, 2));
		
		int n = agentArr.length;
        for(int i=0; i<n; i++){
			// Will not draw the child Oriented..(), uses it only to learn about selection:
            TrailedPortrayal2D trailed = new TrailedPortrayal2D( this,
			  new OrientedPortrayal2D(
				new SimplePortrayal2D(),
				0, // offset (min length)
				4.0, // scale
				Color.black, // Will be repainted.
				OrientedPortrayal2D.SHAPE_COMPASS)
			  // Override draw() in order to change color for caught fish.
			  {
				  private static final long serialVersionUID = 1;
				  public void draw(Object object, Graphics2D graphics, DrawInfo2D info)
				  {
					  Flocker f = (Flocker)object;
					  if (f.timeTillLive>0)
						  paint = Color.red;
					  else
						  paint = f.color;
					  super.draw(object, graphics, info);
				  }
			  },
			  trailsPortrayal,
			  100 // Length of trail.
																 );
			/*
			  TrailedPortrayal added to BOTH field portrayals, to be selected 
			  even  when moving.  
			  Needed because MovablePortrayal2D bypasses the selection mechanism, 
			  but then sends selection to just its own child portrayal. 
			  But we need selection sent to the simple portrayals in both field portrayals, 
			  even after moving. So we do this by wrapping TrailedPortrayal 
			  in both field portrayals.  It's okay because the TrailedPortrayal 
			  will only draw itself in the trailsPortrayal, passed into its constructor.
			*/
                        
			Object o = agentArr[i];

            flockersPortrayal.setPortrayalForObject(o,
			  // Draws the child:
			  new AdjustablePortrayal2D(
				new MovablePortrayal2D(trailed)));
			// Only portrays the trail:
            trailsPortrayal.setPortrayalForObject(o, trailed);
		}
        
        // Confine display size into our limits.
        double w = space.getWidth();
        double h = space.getHeight();
        if (w == h)	{
			display.insideDisplay.width = display.insideDisplay.height = ourDisplaySize;
		}
        else if (w > h)	{
			display.insideDisplay.width = ourDisplaySize;
			display.insideDisplay.height = ourDisplaySize * (h/w);
		} else if (w < h) {
			display.insideDisplay.height = ourDisplaySize;
			display.insideDisplay.width = ourDisplaySize * (w/h);
		}
        display.reset();
        display.repaint();
	}

    public void init(Controller c)
	{
        super.init(c);

        display = new Display2D(750,750,this);
        display.setBackdrop(Color.black);

        displayFrame = display.createFrame();
        displayFrame.setTitle("Flockers");
        c.registerFrame(displayFrame);
        displayFrame.setVisible(true);

        display.attach(trailsPortrayal, "Trails");
		// This overlays the trails, because it is attached later:
        display.attach(flockersPortrayal, "Behold the Flock!");
        display.attach(bigFishPortrayal, "Alarm!");
        // display.attach(bigFishPortrayalCenter, "Eat!");

		chart = new sim.util.media.chart.TimeSeriesChartGenerator();
		chart.setTitle("Big fish energy");
		chart.setXAxisLabel("Steps");
		chart.setYAxisLabel("Energy");  
		chartFrame = chart.createFrame(true);
		// The two commands below have an effect on the size, but not on the position.
		chartFrame.setPreferredSize(new Dimension(600,350));
		chartFrame.pack();  
		chartFrame.setVisible(true);
		c.registerFrame(chartFrame);

		c.registerFrame(createCommandsFrame());		
		/*
		  For registration, the frame title is helpful.
		  Without the title, you don't see much of it in the Displays tab.

		  The console automatically moves itself to the right of all
		  of its registered frames -- you might wish to rearrange the
		  location of all the windows, including the console, at this
		  point in time.  The positions will be remembered in the preferences
		  for the next time you call the app, if you push the Save button in the 
		  Displays tab. (The location of the preferences file is system-dependent.)
		*/

	}
        
	// Called when the current simulation ends.
	public void finish()
	{
		state.finish();
        super.finish();

		chart.update(state.schedule.getSteps(), true);
		chart.repaint();
        chart.stopMovie();
	}

	// Called by the console when the user ends everything.
    public void quit()
	{
        super.quit();
        
        if (displayFrame!=null)
			displayFrame.dispose();
        displayFrame = null;
        display = null;

		chart.update(state.schedule.getSteps(), true);
        chart.repaint();
        chart.stopMovie();
        if (chartFrame != null)	chartFrame.dispose();
        chartFrame = null;
    }

	JFrame createCommandsFrame()
	{
		JFrame f = new JFrame("Commands");
		Container p = f.getContentPane();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

		// Other buttons can be added here.
		p.add(chart2pdfButton("Save chart", f));
	
		f.pack();
		f.setVisible(true);
		return f;
	}

	public JButton chart2pdfButton(String buttonName, Frame f)
	{
		JButton button = new JButton(buttonName);
        button.addActionListener(new ActionListener()
            {
				public void actionPerformed(ActionEvent e)
                {
					chart2pdf(f);
                }
            });
		return button;
	}

	/*
	  f is the frame from which this is called, to be supplied as
	  an argument required to the file dialog.
	*/
	public void chart2pdf(Frame f) 
	{	
		FileDialog fd = new FileDialog(f, "Save the chart to a pdf", FileDialog.SAVE);
		fd.setFile(Flockers.savedChartFileName);
		fd.setDirectory(
		  Paths.get(Flockers.resourceDirName, Flockers.outputDirName).toString());
        fd.setVisible(true);
		String fileName = fd.getFile();
		if (null == fileName)
			return;
		Path p = null;
		try	{
			p = Paths.get(fd.getDirectory(),
			  Utilities.ensureFileEndsWith(fileName, ".pdf"));
			sim.util.media.PDFEncoder.generatePDF(chart.getChart(), chart.getWidth(),
			  chart.getHeight(),
			  p.toFile());
		} 
		catch (Exception e) {
			String s = p.toString();
			Utilities.informOfError(e, 
			  "Error while saving the chart to file " + (s == null ? " " : s), null);
		}
	}			
}
