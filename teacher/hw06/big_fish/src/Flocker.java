/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information

  Reformatted for readability by P. Gacs.
*/

package big_fish;
import sim.engine.*;
import sim.field.continuous.*;
import sim.util.*;
import ec.util.*;

import java.awt.*;

public class Flocker implements Steppable,
								sim.portrayal.Orientable2D // used by the GUI
{
    private static final long serialVersionUID = 1;

    public Double2D loc = new Double2D(0,0);
    public Double2D velo = new Double2D(0,0); 	// Velocity defines orientation.
    public Continuous2D fineSpace;
    public Continuous2D roughSpace;
    public Flockers flock; // Assigned in Flockers at creation.

	int timeTillLive = 0;
	public Color color; // Now a property of the fish, not only of its portrayal.
	

	double mass = 1;
	
    public Flocker(Double2D location, Flockers f) {
		loc = location;
		flock = f;
		fineSpace = flock.fineSpace;
		roughSpace = flock.roughSpace;
	}

    public void step(SimState state)
	{        
        if (dead) return; // These are the ones dead forever.

		if (timeTillLive>0)
			// These just seem dead to the big fish temporarily.
			timeTillLive -= 1;
        
        Bag b = getNeighbors();
            
        Double2D repul = repulsion(b, fineSpace);
        Double2D nbCtr = neighborsCenter(b, fineSpace);
        Double2D rD = flock.rndDir();
        Double2D mMom = meanMom(b, fineSpace);
        Double2D mome = momentum();

        double dx = flock.cohesionWt * nbCtr.x
			+ flock.avoidanceWt * repul.x
			+ flock.consistencyWt * mMom.x
			+ flock.randomnessWt * rD.x
			+ flock.momentumWt * mome.x
			+ flock.escapeWt * escapeForce().x;
        double dy = flock.cohesionWt * nbCtr.y
			+ flock.avoidanceWt * repul.y
			+ flock.consistencyWt* mMom.y
			+ flock.randomnessWt * rD.y
			+ flock.momentumWt * mome.y
			+ flock.escapeWt * escapeForce().y;
                
        // Scale to the jump size:
        double dis = Math.sqrt(dx*dx+dy*dy);
        if (dis>0){
            dx = (dx / dis) * flock.jump;
            dy = (dy / dis) * flock.jump;
		}
        velo = new Double2D(dx,dy);
        loc = new Double2D(
		  fineSpace.stx(loc.x + dx),
		  fineSpace.sty(loc.y + dy)
						   );
        fineSpace.setObjectLocation(this, loc);
        roughSpace.setObjectLocation(this, loc);
	}

    
    public Bag getNeighbors()
	{
        return fineSpace.getNeighborsExactlyWithinDistance(loc,
		  flock.neighborhoodRadiusFine, // The distance. 
		  true // Toroidal (world wrap).
													   );
	}
	
	/*
	  Required by sim.portrayal.Orientable2D. 
	  Will allow to change the orientation by a mouse.
	  The orientation will be the negative velocity, 
	  a hack to invert the compass shape into a fish shape.
	 */ 
    public double orientation2D() 
	{
        if (velo.x == 0 && velo.y == 0) return 0;
        return Math.atan2(velo.y, velo.x)
			+ Math.PI; // To reverse.
	}	
    public void setOrientation2D(double angle) 	// In radians.
	{
        velo = new Double2D(Math.cos(angle), Math.sin(angle));
	}
	public double getOrientation() { return orientation2D(); }

	// Dead means not moving.
    public boolean dead = false;
    public boolean isDead() { return dead; }
    public void setDead(boolean val) { dead = val; }

	// Her it is the same as velocity.
    public Double2D momentum()
	{
        return velo.multiply(mass);
	}

	// Mean momentum of all live bag elements. 
    public Double2D meanMom(Bag b, Continuous2D space)
	{
        if (b==null || b.numObjs == 0) return new Double2D(0,0);
        
        double x = 0; 
        double y = 0;
        int i = 0;
        int count = 0; // Only count the live ones. 
        for(i=0; i<b.numObjs; i++) {
            Flocker other = (Flocker)(b.objs[i]);
            if (!other.dead) {
                Double2D m = ((Flocker)b.objs[i]).momentum();
                count++;
                x += m.x;
                y += m.y;
			}
		}
        if (count > 0) { x /= count; y /= count; }
        return new Double2D(x,y);
	}

	/*
	  Vector pointing from the current flocker
	  to the center (of gravity) of all live other flockers in the bag.
	*/
    public Double2D neighborsCenter(Bag b, Continuous2D space)
	{
        if (b==null || b.numObjs == 0) return new Double2D(0,0);
        
        double x = 0; 
        double y = 0;        

        int count = 0;
        int i = 0;
        for(i=0; i<b.numObjs; i++) {
            Flocker other = (Flocker)(b.objs[i]);
            if (!other.dead) {
                double dx = space.tdx(loc.x, other.loc.x); // tdx is toroidal distance.
                double dy = space.tdy(loc.y, other.loc.y);
                count++;
                x += dx;
                y += dy;
			}
		}
        if (count > 0) { x /= count; y /= count; }
        return new Double2D(-x, -y);
	}
 
	// Sum of repulsion forces proportional to the approx inverse distances
    public Double2D repulsion(Bag b, Continuous2D space)
	{
        if (b==null || b.numObjs == 0) return new Double2D(0,0);
        double x = 0;
        double y = 0;
        
        int i = 0;
        int count = 0;

        for(i=0; i<b.numObjs; i++) {
            Flocker other = (Flocker)(b.objs[i]);
            if (other != this )	{
                double dx = space.tdx(loc.x, other.loc.x);
                double dy = space.tdy(loc.y, other.loc.y);
                double lensquared = dx*dx+dy*dy;
                count++;
				// A repulsion force proportional to the approx inverse distance
                x += dx/(lensquared*lensquared + 1);
                y += dy/(lensquared*lensquared + 1);
			}
		}
        if (count > 0) { x /= count; y /= count; }
        return new Double2D(x, y);      
	}

	Double2D escapeForce()
	{
		BigFish bf = flock.bigFish;

		// v is the velocity vector of the big fish.
		double vx = bf.velo.x;
		double vy = bf.velo.y;
 		double vsq = vx * vx + vy * vy;

		// vector aa points from the big fish to me.
		double aax = loc.x - bf.loc.x;
		double aay = loc.y - bf.loc.y;
		double asq = aax * aax + aay * aay;

		/* 
		   vector va * v / vsq is the projection of aa on v.
		   vector w points away perpendicularly from the line of v.
		*/
		double va = vx * aax + vy * aay;

		double wx = aax -  va * vx / vsq;
		double wy = aay -  va * vy / vsq;

		// The size of the escape force is made (almost) inversely
		// proportional to the squared distance to the big fish.
		return  (new Double2D(wx,wy)).resize(1 / (1 + asq));
	}
}
