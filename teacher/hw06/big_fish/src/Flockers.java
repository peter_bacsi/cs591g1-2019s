/*
  Copyright 2006 by Sean Luke and George Mason University
  Licensed under the Academic Free License version 3.0
  See the file "LICENSE" for more information

  Reformatted for readability by P. Gacs.
*/

package big_fish;

import sim.engine.*;
import sim.util.*;
import sim.field.continuous.*;

import java.awt.*;

public class Flockers extends SimState
{
    private static final long serialVersionUID = 1;

	// The small fish simply know the position of the big fish (now there is only one).
	BigFish bigFish;

	// roughSpace has bigger hash buckets.
    public Continuous2D fineSpace;
    public Continuous2D roughSpace;
    public Continuous2D bfSpace;  // To portray the big fish.

	static String resourceDirName = "workspace";
	static String outputDirName = "outputs";
	static String savedChartFileName = "saved-chart.pdf";

	public double bigFishMass = 200;
	public double getBigFishMass() {return bigFishMass;}
	public void setBigFishMass(double val) {if (val > 0) bigFishMass = val;}

	// Weight of various components influencing the motion of each flocker:
	
	// Importance of staying close to the neighbors.
    public double cohesionWt = 0.1;
    public double getCohesionWt() { return cohesionWt; }
    public void setCohesionWt(double val) { if (val >= 0.0) cohesionWt = val; }

	// Not bumping into the neighbors.
    public double avoidanceWt = 400.0;
    public double getAvoidanceWt() { return avoidanceWt; }
    public void setAvoidanceWt(double val) { if (val >= 0.0) avoidanceWt = val; }

	// Scale of random direction change.
    public double randomnessWt = 0.05;
    public double getRandomnessWt() { return randomnessWt; }
    public void setRandomnessWt(double val) { if (val >= 0.0) randomnessWt = val; }

	// Keeping to the mean direction of neighbors.
    public double consistencyWt = 1;
    public double getConsistencyWt() { return consistencyWt; }
    public void setConsistencyWt(double val) { if (val >= 0.0) consistencyWt = val; }

	// Weight given to one's own velocity.
    public double momentumWt = 1.0;
    public double getMomentumWt() { return momentumWt; }
    public void setMomentumWt(double val) { if (val >= 0.0) momentumWt = val; }

	// Weight to flee from the big fish.
    public double escapeWt = 130;
    public double getEscapeWt() { return escapeWt; }
    public void setEscapeWt(double val) { if (val >= 0.0) escapeWt = val; }

	// Other parameters
	
    public int numFlockers = 300;
    public int getNumFlockers() { return numFlockers; }
    public void setNumFlockers(int val) { if (val >= 1) numFlockers = val; }

    public double width = 250;
    public double getWidth() { return width; }
    public void setWidth(double val) { if (val > 0) width = val; }

    public double height = 250;
    public double getHeight() { return height; }
    public void setHeight(double val) { if (val > 0) height = val; }

    public double neighborhoodRadiusFine = 10;
    public double getNeighborhoodRadiusFine() { return neighborhoodRadiusFine; }
    public void setNeighborhoodRadiusFine(double val)
	{ if (val > 0) neighborhoodRadiusFine = val; }

    public double neighborhoodRadiusRough = 30;
    public double getNeighborhoodRadiusRough() { return neighborhoodRadiusRough; }
    public void setNeighborhoodRadiusRough(double val)
	{ if (val > 0) neighborhoodRadiusRough = val; }

    public double deadFlockerProbability = 0.001;
    public double getDeadFlockerProbability() { return deadFlockerProbability; }
    public void setDeadFlockerProbability(double val) {
		if (val >= 0.0 && val <= 1.0) deadFlockerProbability = val;
	}

	int flockSampleSize = 6;
	public int getFlockSampleSize() {return flockSampleSize;}
	public void setFlockSampleSize(int s) {if (s>0) flockSampleSize = s;}

	// When the mean location of the neighborhood sample is smaller then this
	// then a random sample element is chosen instead.
	double foodDirLb = 1;
	public double getFoodDirLb() {return foodDirLb;}
	public void setFoodDirLb(double d) {if (d>0) foodDirLb = d;}

	double maxSpeed = 1; // For the big fish.
	public double getMaxSpeed() {return maxSpeed;}
	public void setMaxSpeed(double s) {if (s>0) maxSpeed = s;}

	// Multiplies the food direction for the force on the big fish.
	double forceCoeff = 0.1;
	public double getForceCoeff() {return forceCoeff;}
	public void setForceCoeff(double d) {if (d>0) forceCoeff = d;}
        
	// The foodDirection vector will be ignored by the big fish if it is smaller.
	double disorientationBd = 1;
	public double getDisorientationBd() {return disorientationBd;}
	public void setDisorientationBd(double d) {if (d>0) disorientationBd = d;}

	int timeoutLength = 100;
	public int getTimeoutLength() {return timeoutLength;}
	public void setTimeoutLength(int l) { if (l>0) timeoutLength = l;}
        
	double stepCost = 0.0019;
	public double getStepCost() {return stepCost;}
	public void setStepCost(double c) { if (c>0) stepCost = c;}
        
    public double jump = 0.7;  // how far do we move in a timestep?
    
    public Double2D[] getLocations()
	{
        if (fineSpace == null) return new Double2D[0];
        Bag b = fineSpace.getAllObjects();
        if (b==null) return new Double2D[0];
        Double2D[] locs = new Double2D[b.numObjs];
        for(int i =0; i < b.numObjs; i++)
            locs[i] = fineSpace.getObjectLocation(b.objs[i]);
        return locs;
	}

    public Double2D[] getInvertedLocations()
	// Reflected around the x=y axis.
	{
        if (fineSpace == null) return new Double2D[0];
        Bag b = fineSpace.getAllObjects();
        if (b == null) return new Double2D[0];
        Double2D[] locs = new Double2D[b.numObjs];
        for(int i =0; i < b.numObjs; i++)
		{
            locs[i] = fineSpace.getObjectLocation(b.objs[i]);
            locs[i] = new Double2D(locs[i].y, locs[i].x);
		}
        return locs;
	}

    public Flockers(long seed)
	{
        super(seed);
	}
    
    public void start()
	{
        super.start();

		/*
		  Set up the fineSpace.  It looks like a discretization
		  of about neighborhoodRadius / 1.5 is close to optimal for us.
		  Hmph,that's 16 hash lookups! I would have guessed that 
		  neighborhoodRadius * 2 (which is about 4 lookups on average)
		  would be optimal.  Go figure.
		*/
        fineSpace = new Continuous2D(neighborhoodRadiusFine / 1.5, // The size of a hash bucket.
		  width, height);
        
        roughSpace = new Continuous2D(neighborhoodRadiusRough / 1.5, width, height);
		// Bucket size rather irrelevant here, just one big fish.        
        bfSpace = new Continuous2D(neighborhoodRadiusRough,width, height);

		Double2D bfLocation = new Double2D(
		  random.nextDouble() * width, random.nextDouble() * height);
		double speed = random.nextDouble() * maxSpeed;
		Double2D bfVelo = rndDir().resize(speed);
		bigFish = new BigFish(bfLocation, bfVelo, this);
		bfSpace.setObjectLocation(bigFish, bfLocation);
		schedule.scheduleRepeating(bigFish);		
		
        // Make some flockers and schedule them.  A few will be dead.
        for(int x=0;x<numFlockers;x++){
            Double2D location = new Double2D(
			  random.nextDouble() * width, random.nextDouble() * height);
            Flocker flocker = new Flocker(location, this);
            if (random.nextBoolean(deadFlockerProbability))
				flocker.dead = true;
            fineSpace.setObjectLocation(flocker, location);
            roughSpace.setObjectLocation(flocker, location);
            flocker.flock = this;
            flocker.fineSpace = fineSpace;
            flocker.roughSpace = roughSpace;
			flocker.color = new Color(
			  128 + random.nextInt(128),
			  128 + random.nextInt(128),
			  128 + random.nextInt(128));
			  
            schedule.scheduleRepeating(flocker);
		}

	}

    public static void main(String[] args)
	{
        doLoop(Flockers.class, args);
        System.exit(0);
	}

	// Random direction.
    public Double2D rndDir()
	{
		double angle = random.nextDouble() * 2 * Math.PI;
        return new Double2D(Math.cos(angle), Math.sin(angle));
	}	
}
