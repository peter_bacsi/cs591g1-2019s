/*
  Extended from flockers of Copyright 2006 by Sean Luke and George Mason University  
*/

package big_fish;

import sim.engine.*;
import sim.field.continuous.*;
import sim.util.*;
import ec.util.*;

import java.text.*;

public class BigFish implements Steppable,
								sim.portrayal.Orientable2D // used by the GUI
{
    private static final long serialVersionUID = 1;

    public Double2D loc = new Double2D(0,0);
    public Double2D velo = new Double2D(0,0); 	// Velocity defines orientation.
    public Flockers flock;
    public Continuous2D fineSpace; 
    public Continuous2D roughSpace;
    public Continuous2D bfSpace;
    public double mass;
	double energy = 0;
	DecimalFormat fm = new DecimalFormat("#.###");
	

    public BigFish(Double2D location, Double2D v, Flockers f)
	{
		loc = location;
		velo = v;
		flock = f;
		mass = flock.bigFishMass;
		fineSpace = flock.fineSpace;
		roughSpace = flock.roughSpace;
		bfSpace = flock.bfSpace;
	}

    public void step(SimState state)
	{
		// if (0 == flock.schedule.getSteps() % 200)
		// 	System.out.println(fm.format(energy));
		
		energy -= flock.getStepCost();
		
		Bag b = getFood();
		int n = b.numObjs;

		// Timeout for the caught fish.
		for (int i = 0; i<n; i++) {
			Flocker f = (Flocker)b.objs[i];
			if (0 == f.timeTillLive){ // Don't eat again the fish you just caught.
				f.timeTillLive = flock.getTimeoutLength();
				energy += 1;
			}
		}
			
		// Accelerate towards food, but if needed scale to maximum speed.
		Double2D force = foodDirection().multiply(flock.forceCoeff);

		Double2D mom = momentum().add(force);
		velo = mom.multiply(1 / mass);
		if (velo.length() > flock.maxSpeed)
			velo = velo.resize(flock.maxSpeed);
        loc = new Double2D(
		  bfSpace.stx(loc.x + velo.x),
		  bfSpace.sty(loc.y + velo.y));
        bfSpace.setObjectLocation(this, loc);
	}
 
	/*
	  Required by sim.portrayal.Orientable2D. 
	  Will allow to change the orientation by a mouse.
	  The orientation will be the negative velocity, 
	  a hack to invert the compass shape into a fish shape.
	 */ 
    public double orientation2D() 
	{
        if (velo.x == 0 && velo.y == 0) return 0;
        return Math.atan2(velo.y, velo.x)
			+ Math.PI; // To reverse.
	}	

	public double getOrientation() { return orientation2D(); }

    public void setOrientation2D(double angle) 	// In radians.
	{
        velo = new Double2D(Math.cos(angle), Math.sin(angle));
	}

	/*
	  The big fish sees a square of radius neighborhoodRadiusRough, but shifted ahead of it
	  in case its speed is positive.
	 */
    public Bag getNeighbors()
	{
		Double2D center;
		if (0 == velo.x && 0 == velo.y) 
			center = loc;
		else 
			center = loc.add(velo.resize(flock.neighborhoodRadiusRough));
		
        return roughSpace.getNeighborsExactlyWithinDistance(center,
		  flock.neighborhoodRadiusRough,
		  true // Toroidal (world wrap).
															);
	}
	
    public Bag getFood()
	/* 
	   Catch the small fish close to the mouth.
	   The caught fish continues swimming, but it temporarily it looks dead.
	 */
	{
		Double2D center;
		if (0 == velo.x && 0 == velo.y) 
			center = loc;
		else 
			center = loc.add(velo.resize(flock.neighborhoodRadiusFine));
		
        return roughSpace.getNeighborsExactlyWithinDistance(center,
		  flock.neighborhoodRadiusFine/4, // Seems the right area to catch.
		  true // Toroidal (world wrap).
															);
	}
	
    public Double2D momentum()
	{
        return velo.multiply(mass);
	}

	/*
	  Mean direction of a sample from the small fish neighbors.
	  But if the vector is too small then it may be the average of opposite vectors,
	  and the last sample element is used instead.
	*/
	
	Double2D foodDirection()
	{
		int s = flock.flockSampleSize;
		double dx = 0;
		double dy = 0;

		Bag b = getNeighbors();
		int n = b.numObjs;
		if (0 == n) {
			dx = flock.random.nextDouble()*flock.disorientationBd;
			dy = flock.random.nextDouble()*flock.disorientationBd;
			return new Double2D(dx,dy);
		}

		double lx = loc.x;
		double ly = loc.y;
		Double2D sloc = new Double2D();		
		for (int i = 0; i < s; i++)	{
			sloc = ((Flocker)b.objs[flock.random.nextInt(n)]).loc;
			dx += sloc.x - lx;
			dy += sloc.y - ly;
		}
		Double2D direction = new Double2D(dx / s, dy / s);
		if (direction.length() > flock.disorientationBd)
			return direction;
		else return sloc;
	}

}
