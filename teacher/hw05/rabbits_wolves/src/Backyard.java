// Gacs: this project was started by Umang Desai.

package backyard2;

import sim.engine.*;
import sim.field.grid.*;
import ec.util.*;
import sim.util.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class Backyard extends SimState
{
 	private static final long serialVersionUID = 1L;

	Long seed;

	// Fully qualified name since sim.util.Properties also exists.
	java.util.Properties config = null;

	// Shows whether the model was restarted.
	boolean restart = false;
	public boolean getRestart() { return restart; }
	public void setRestart(boolean val) { restart = val; }

	static String resourceDirName = "workspace";
	static String outputDirName = "outputs";
	static String savedChartFileName = "saved-chart.pdf";
	static Path configFile = Paths.get(resourceDirName, "backyard.properties");

	// Grid

    int gridHeight = 170;
	public int getGridHeight(){ return gridHeight; }
	public void setGridHeight(int height){ gridHeight = height; }
	
    int gridWidth = 170;
	public int getGridWidth(){ return gridWidth; }
	public void setGridWidth(int width){ gridWidth = width; }

	// If a property is not initialized, the inspector gets null pointer exception.
	// Do a Refresh in the inspector before actually using it.

	// This grid stores the grass height at each of its position.
    public DoubleGrid2D gridForGrass = new DoubleGrid2D(gridWidth,gridHeight);

	// Frequency of larger calculations.	
	private int calcPeriod = 30; 
	public int getCalcPeriod(){ return calcPeriod; }
	public void setCalcPeriod(int g){ calcPeriod = g; }

	// Grass

	double[] grassDensityLocal = new double[gridWidth*gridHeight]; // for a histogram
	public double[] getGrassDensityLocal()
	{
		double[][] grassField = gridForGrass.field;
        for(int j = 0; j < gridHeight; j++) {
            for (int k = 0; k < gridWidth; k++) {
				grassDensityLocal[j*gridHeight + k] = grassField[j][k];
			}
        }
		return grassDensityLocal;
	}

	double initialGrassDensity = 0.5;
	public double getInitialGrassDensity(){return initialGrassDensity;}
	public void setInitialGrassDensity(double d)
	{ if (d >= 0) initialGrassDensity = d;}
		
	private double grassGrowth = 0.00035;
	public double getGrassGrowth(){ return grassGrowth; }
	public void setGrassGrowth(double g){ grassGrowth = g; }

	// The initial grass is randomly distributed between min and max.
	private double grassMin = 0;
	private double grassMax = 1;

	// Rabbits

	Bag rabbits = new Bag();

	/*
	  We could store all animals in one sparse grid, but then we would need
	  case distinctions later.
	*/
	
    public SparseGrid2D gridForRabbits = new SparseGrid2D(gridWidth, gridHeight);

    private int numRabbitsInit = 20; //default value
	public int getNumRabbitsInit(){ return numRabbitsInit;}
	public void setNumRabbitsInit(int num){ numRabbitsInit = num;}

    private int numRabbits = numRabbitsInit;
	public int getNumRabbits(){ return rabbits.numObjs;}


	// The maximum energy a rabbit can accumulate by eating.
	static double energyMaxRabbits = 4;
    public double getEnergyMaxRabbits() { return energyMaxRabbits; }
    public void setEnergyMaxRabbits(double m) {
        if (m >= 0) {
            Backyard.energyMaxRabbits = m;
        }
    }

	static double energyLossRabbits = 0.2; // In each jump.
	public double getEnergyLossRabbits() { return energyLossRabbits; }
	public void setEnergyLossRabbits(double c) { energyLossRabbits = c; }

    static int jumpSizeRabbits = 2;
	public int getJumpSizeRabbits(){ return jumpSizeRabbits; }
	public void setJumpSizeRabbits(int j){ if (j>= 0) jumpSizeRabbits = j; }

	static double reproductionRateRabbits = 0.01;
    public double getReproductionRateRabbits() { return reproductionRateRabbits; }
    public void setReproductionRateRabbits(double r) {
        if (r >= 0) {
            Backyard.reproductionRateRabbits = r;
        }
    }

   // Wolves

	Bag wolves = new Bag();
    public SparseGrid2D gridForWolves = new SparseGrid2D(gridWidth, gridHeight);

    private int numWolvesInit = 3; //default value
	public int getNumWolvesInit(){ return numWolvesInit;}
	public void setNumWolvesInit(int num){ numWolvesInit = num;}

    private int numWolves = numWolvesInit;
	public int getNumWolves(){ return wolves.numObjs;}

	// The maximum energy a rabbit can accumulate by eating.
	static double energyMaxWolves = 10;
    public double getEnergyMaxWolves() { return energyMaxWolves; }
    public void setEnergyMaxWolves(double m) {
        if (m >= 0) 
            Backyard.energyMaxWolves = m;
    }

	static double energyLossWolves = 1; // In each jump.
	public double getEnergyLossWolves() { return energyLossWolves; }
	public void setEnergyLossWolves(double c) { energyLossWolves = c; }

	static double energyGainCoeffWolves = 0.8; // From a rabbit.
	public double getEnergyGainCoeffWolves() { return energyGainCoeffWolves; }
	public void setEnergyGainCoeffWolves(double c) { energyGainCoeffWolves = c; }

    static int jumpSizeWolves = 5;
	public int getJumpSizeWolves(){ return jumpSizeWolves; }
	public void setJumpSizeWolves(int j){ if (j>= 0) jumpSizeWolves = j; }

    static int huntRadius = 5;
	public int getHuntRadius(){ return huntRadius; }
	public void setHuntRadius(int r){ if (r >= 0) huntRadius = r; }

	static double reproductionRateWolves = 0.005;
    public double getReproductionRateWolves() { return reproductionRateWolves; }
    public void setReproductionRateWolves(double r) {
        if (r >= 0) {
            Backyard.reproductionRateWolves = r;
        }
    }

	double grassTotApprox = 0;
	int historySumNumRabbits = 0;
	int historySumSqNumRabbits = 0;
	double historyStdevRabbits = 0;

	int historySumNumWolves = 0;
	int historySumSqNumWolves = 0;
	double historyStdevWolves = 0;

	/*
	  Load the initialization from the preferences here and not in start(), so that
	  they don't get overridden in start() if you changed them.
	*/
    public Backyard(long seed)
	{
        super(seed);
		this.seed = seed;

		// Don't reload the configuration.
		if (null == config){
			config = new java.util.Properties();
			loadConfig(config, configFile);
		}
		// In start() we only initialize again if Restart is checked.
		initialState();
    }

    public static void main(String[] args)
    {
        doLoop(Backyard.class, args);
        System.exit(0);
    }

    public void start() {

		boolean restart = getRestart();
		if (restart && 0L != seed)
			random.setSeed(seed); // to restart exactly.

	    super.start();

		if (restart){
			initialState();  // This schedules the rabbits, too.
			restart();
		} else {
			scheduleAnimals();
		}

		grassTotApprox = 0;
		historySumNumRabbits = 0;
		historySumSqNumRabbits = 0;
		historyStdevRabbits = 0;
		historySumNumWolves = 0;
		historySumSqNumWolves = 0;
		historyStdevWolves = 0;
		
		/* 
		   An agent performing some global tasks, like growing the grass
		   and computing some sums.
		   An anonymous class modeled on the decreaser of trails of particles 
		   in Tutorial 3.
		*/
        Steppable global = new Steppable()  
			{
				private static final long serialVersionUID = 1;

				public void step(SimState state)
				{
					int steps = (int)state.schedule.getSteps();

					if (0 == numWolves)
						finish();

					gridForGrass.add(grassGrowth);
					gridForGrass.upperBound(1);
					
					numRabbits = rabbits.numObjs;
					historySumNumRabbits += numRabbits;
					historySumSqNumRabbits += numRabbits * numRabbits;

					numWolves = wolves.numObjs;
					historySumNumWolves += numWolves;
					historySumSqNumWolves += numWolves * numWolves;

					// Recalculate some things only every calcPeriod steps.
					if (0 == (int)state.schedule.getSteps() % calcPeriod) {
						double meanRabbits = historySumNumRabbits / (1.0 + steps);
						double meanSqRabbits = historySumSqNumRabbits / (1.0 + steps);
						historyStdevRabbits = Math.sqrt(meanSqRabbits - meanRabbits*meanRabbits);
						
						double meanWolves = historySumNumWolves / (1.0 + steps);
						double meanSqWolves = historySumSqNumWolves / (1.0 + steps);
						historyStdevWolves = Math.sqrt(meanSqWolves - meanWolves*meanWolves);
						
						int grassHeight = gridForGrass.getHeight();;
						int grassWidth = gridForGrass.getWidth();;
						double[][] grassField = gridForGrass.field;

						grassTotApprox = 0;
						for(int j = 0; j < grassHeight; j++) {
							for (int k = 0; k < grassWidth; k++) 
								grassTotApprox += grassField[j][k];
						}
					}
				}
			};
        schedule.scheduleRepeating(Schedule.EPOCH, // The start of the schedule
		  2, // Ordering (to execute after those with ordering 1).
		  global, // What to schedule.
		  1 // Time between repeat steps.
								   );		
    }

	// Called at the end of the simulation.
    public void finish()
	{
		// kill() can also be called by one of the agents to finish everything.
        kill();  
	}

	// In case you want to do something before restarting:
	void restart()
	{
		System.out.println("Restart.");
	} 

	void initialState()
	{
		propsFromConfig();
        gridForRabbits = new SparseGrid2D(gridWidth, gridHeight);
        gridForWolves = new SparseGrid2D(gridWidth, gridHeight);
        gridForGrass = new DoubleGrid2D(gridWidth, gridHeight);
		grassDensityLocal = new double[gridWidth * gridHeight];
		
		rabbits = new Bag();
		wolves = new Bag();

        for(int i=0 ; i < numRabbitsInit ; i++) {
			addNewRabbit(new Int2D(random.nextInt(gridWidth),random.nextInt(gridHeight)));
        }
        for(int i=0 ; i < numWolvesInit ; i++) {
			addNewWolf(new Int2D(random.nextInt(gridWidth),random.nextInt(gridHeight)));
        }
		double[][] grassField = gridForGrass.field;
        for(int j = 0; j < gridHeight; j++) {
            for (int k = 0; k < gridWidth; k++) {
				grassField[j][k] = grassMin + (grassMax - grassMin)*random.nextDouble();
			}
        }
	}

	/* 
	   scheduleRepeating() returns an object of type Stoppable, that can be used
	   to stop the repetition: we will use it when killing an agent.
	*/
	void scheduleAnimals()
	{
		int n = rabbits.numObjs;
		for (int i=0; i<n; i++){
			Rabbit r = (Rabbit) rabbits.get(i);
			r.stopper = schedule.scheduleRepeating(r); 
		}
		n = wolves.numObjs;
		for (int i=0; i<n; i++){
			Wolf w = (Wolf) wolves.get(i);
			w.stopper = schedule.scheduleRepeating(w); 
		}
	}


	/*
	  Ordinary removal from a bag involves a linear search.
	  To avoid it, each member remembers its index in the bag, and then
	  uses the procedure below.
	  This hack relies on knowing that an index will change only
	  when a removed member is replaced with the last one.
	  (I did not measure whether this makes a difference in the present application.)

	  It should be possible to write only one remove and add function for both rabbits and
	  wolves, but this might need a class Animal.
	*/
	void removeFastFromRabbits(Rabbit r)
	{
		int n = rabbits.numObjs;
		if (1 >= n)
			rabbits.remove(r);
		else {
			int i = r.index;
			((Rabbit)rabbits.getValue(n-1)).index = i;
			rabbits.remove(i);
		}
	}

	void removeFastFromWolves(Wolf r)
	{
		int n = wolves.numObjs;
		if (1 >= n)
			wolves.remove(r);
		else {
			int i = r.index;
			((Wolf)wolves.getValue(n-1)).index = i;
			wolves.remove(i);
		}
	}

	void addNewRabbit(Int2D location)
	{
		Rabbit r = new Rabbit();
		r.index = rabbits.numObjs;
		r.backyard = this;
		r.stopper = schedule.scheduleRepeating(r); 
		rabbits.add(r);
		gridForRabbits.setObjectLocation(r, location);
	}

	void addNewWolf(Int2D location)
	{
		Wolf r = new Wolf();
		r.index = wolves.numObjs;
		r.backyard = this;
		r.stopper = schedule.scheduleRepeating(r); 
		wolves.add(r);
		gridForWolves.setObjectLocation(r, location);
	}

    private void propsFromConfig()
    {
		numRabbitsInit = Integer.parseInt(config.getProperty("num-rabbits-init", "10"));
		jumpSizeRabbits = Integer.parseInt(config.getProperty("jump-size-rabbits", "2"));
		jumpSizeWolves = Integer.parseInt(config.getProperty("jump-size-wolves", "5"));
		energyLossRabbits = Double.parseDouble(config.getProperty("energy-loss-rabbits", "0.1"));
		energyLossWolves = Double.parseDouble(config.getProperty("energy-loss-wolves", "0.2"));
		grassGrowth = Double.parseDouble(config.getProperty("grass-growth", "0.001"));
		numWolvesInit = Integer.parseInt(config.getProperty("num-wolves-init", "5"));
		reproductionRateRabbits = Double.parseDouble(
		  config.getProperty("reproduction-rate-rabbits", "0.01"));
		reproductionRateWolves = Double.parseDouble(
		  config.getProperty("reproduction-rate-wolves", "0.005"));
		huntRadius = Integer.parseInt(config.getProperty("hunt-radius", "10"));
		initialGrassDensity = Double.parseDouble(config.getProperty("initial-grass-density", "1.0"));
	}

    public static void loadConfig(java.util.Properties config, Path configFile)
    {
		BufferedReader configStream = null;
		try {
			configStream = Files.newBufferedReader(configFile);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		try{
			config.load(configStream);
			configStream.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
    }

	static void storeConfig(java.util.Properties config, Path savedConfigFile, String comment)
    {
		BufferedWriter configStream;
		try {
			configStream = Files.newBufferedWriter(savedConfigFile);
			config.store(configStream, comment);
			configStream.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
    }

}
