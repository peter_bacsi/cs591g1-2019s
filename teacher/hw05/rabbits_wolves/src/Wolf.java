package backyard2;

import sim.engine.*;
import sim.field.grid.*;
import sim.util.*;
import ec.util.*;

public class Wolf implements Steppable
{
	private static final long serialVersionUID = 1L;

	Backyard backyard; // Gets assigned by Backyard.addNewWolf()	

	int index; // in the bag called wolves.
	//  A Stoppable is returned when scheduling: can remove the agent from the schedule.
	Stoppable stopper; 

	private double energy = 2;  // If it decreases below 0 the wolf dies.
	
	// Renamed from Backyard, the main model.
	static double energyMax; 
	static double energyLoss;
    static int jumpSize;
	static double reproductionRate;
	static int huntRadius;

    Wolf()
	{
		energyMax = Backyard.energyMaxWolves;
		energyLoss = Backyard.energyLossWolves;
		jumpSize = Backyard.jumpSizeWolves;
		reproductionRate = Backyard.reproductionRateWolves;
		huntRadius = Backyard.huntRadius;
	}

    public void step(SimState state)
	{
        Int2D location = backyard.gridForWolves.getObjectLocation(this);

		energy -= energyLoss;

		Bag rabbitsHere = backyard.gridForRabbits.getMooreNeighbors(
		  location.x, location.y, huntRadius, Grid2D.TOROIDAL,
		  true // includeOrigin
																	);
		int n = 0;
		if (null != rabbitsHere)
			n = rabbitsHere.numObjs;
		if (0 != n){
			Rabbit r = (Rabbit)rabbitsHere.getValue(n-1);
			r.die();
			energy += ((Backyard)state).energyGainCoeffWolves * r.energy;
			if (energyMax < energy)
				energy = energyMax;
		}
		if (energy<0){
			die();
			return;
		}
		if (reproduce(energy))
			backyard.addNewWolf(location);
		jump(location);
    }

	private void jump(Int2D location)
	{
        int x = location.x;
        int y = location.y;

        int newx = x - jumpSize + backyard.random.nextInt(2*jumpSize+1);
        int newy = y - jumpSize + backyard.random.nextInt(2*jumpSize+1);

		// Explicit world wrap.
        if (newx < 0)
            newx += backyard.gridWidth;
        if (newx >= backyard.gridWidth)
            newx -= backyard.gridWidth;
        if (newy < 0)
            newy += backyard.gridHeight;
        if (newy >= backyard.gridHeight)
            newy -= backyard.gridHeight;
        backyard.gridForWolves.setObjectLocation(this, new Int2D(newx, newy));
	}

	// Reproduce with a probability depending on the energy.
	boolean reproduce(double energy)
	{
		return (backyard.random.nextDouble() < reproductionRate*energy/energyMax);
	}

	void die()
	{
		stopper.stop(); // Remove from the schedule.
		backyard.removeFastFromWolves(this);
		backyard.gridForWolves.remove(this);
	}	
}
