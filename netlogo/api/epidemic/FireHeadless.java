package epidemic;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.lang.*;

import org.nlogo.headless.HeadlessWorkspace;

public class FireHeadless
{

	static Path workDir = Paths.get("..","..","libraries","Netlogo 6.0.4",
	  "models", "Sample Models", "Earth Science");
	static String workDirStr = workDir.toString();

	public static void main(String[] argv) {
		HeadlessWorkspace workspace = HeadlessWorkspace.newInstance() ;
		try {
			workspace.open(workDirStr + File.separator + "Fire.nlogo");
			workspace.command("set density 62");
			workspace.command("random-seed 0");
			workspace.command("setup");
			workspace.command("repeat 50 [ go ]") ;
			System.out.println("burned trees: " +  workspace.report("burned-trees"));
			workspace.dispose();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}
