package sheep_wolves;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.lang.*;
import java.text.*;

import org.nlogo.headless.HeadlessWorkspace;
// Needs Mason in the classpath.
import ec.util.*;

// Read the comment of main() for what this program does.
public class SheepWolvesHeadless
{
	static MersenneTwisterFast random = new MersenneTwisterFast();
	static DecimalFormat fm2 = new DecimalFormat("#.##");

	static HeadlessWorkspace workspace = HeadlessWorkspace.newInstance() ;

	static Path workDir = Paths.get("..","..","libraries","Netlogo 6.0.4");
	static Path modelPath = Paths.get("models", "Sample Models","Biology",
	  "Wolf Sheep Predation.nlogo");
	static Path wholePath = workDir.resolve(modelPath);

	static Path outputDir = Paths.get("sheep_wolves", "out");
	static String outputFileNameRoot = "savedParams";

	static int numRandomTrials = 100; // Number of random parameter sets to try.
	static int numRepetitions = 10; // Experiments in each trial, with the same parameters.
	static int stepsToRun = 1000; // In each exeriment.

	static String[] paramNames = {
		"sheep-gain-from-food",
		"wolf-gain-from-food",
		"sheep-reproduce",
		"wolf-reproduce",
	};

	static String outputName = "maxMinWolves";

	static String measuredReporter = "count wolves";

	// Take the parameters from here:
	static double[][] paramRanges = {{2, 20}, {10,100}, {1,50}, {1,50}}; 
	// Just to start from somewhere:
	static double[] goodValues = {4, 20, 4, 5};
	// In the second series of trials the ranges are narrowed around the best parameters:
	static double rangeScaleFactor = 0.3;

	// How many best parameter sets will we keep?
	static int numBest = 3;

	// Must be static in order to be instantiated, since the outer class is not instantiated.
	static class Experiment
	// A parameter set and a result.
	{
		double[] input;
		double output;

		Experiment(double[] input_, double output_)
		{
			input = input_;
			output = output_;
		}

		public String toString()
		{
			String out = "";
			for (int i = 0; i < input.length; i++){
				out += fm2.format(input[i]) + ", ";
			}
			out += fm2.format(output);
			return out;
		}
	}
	
	public static void main(String[] argv)
	{
	/*
	  For numRandomTrials times:
         choose random values for the parameters 
	     paramNames in the ranges paramRanges.
		 For numRepetitions times:
	         Run the model for stepsToRun steps.
         Take the minimum value of the measuredReporter in these repetitions.		 
	   Record the numBest parameter sets in which these minima were largest.
	   The initialization with goodValues assures that 
	   each recorded parameter set is in the ranges.

	   As an example of how to continue, the best parameter set, 
	   scale down the ranges around it by rangeScaleFactor, and repeat the above,
	   starting from the numBest parameter sets.
	   Record the best parameter set found this way as well.
	 */
		// In case you want repeatability:
		// random.setSeed(1947);

		// Initialization.
		Experiment[] bestExps = new Experiment[numBest];
		for (int i = 0; i<numBest; i++)
			bestExps[i] = new Experiment(goodValues, 0);

		// Try the original parameter ranges.
		printParamRanges(paramRanges);
		try{
			workspace.open(wholePath.toString());
			setupWorkspace();
			// This is the call that takes long:
			bestExps = trySomeRandomParams(paramRanges, numRandomTrials, bestExps);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}

		System.out.println("Best parameters, scale 1:");
		for (int i = 0; i < numBest; i++){
			System.out.println(bestExps[i].toString());
		}
		String outputFileName = outputFileNameRoot + "-" + 1 + ".csv";
		saveParamsToPath(bestExps, paramNames, outputName,
		  outputDir.resolve(outputFileName));

		// Repeat on the scaled-down ranges, Then dispose of the workspace.

		paramRanges = scaleRanges(paramRanges, bestExps[0].input, rangeScaleFactor);		
		printParamRanges(paramRanges);
		try{
			bestExps = trySomeRandomParams(paramRanges, numRandomTrials, bestExps);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}

		System.out.println("Best parameters, scale 2:");
		for (int i = 0; i < numBest; i++){
			System.out.println(bestExps[i].toString());
		}
		outputFileName = outputFileNameRoot + "-" + 2 + ".csv";
		saveParamsToPath(bestExps, paramNames, outputName,
		  outputDir.resolve(outputFileName));

		// Clean up.
		try{
			workspace.dispose();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	static void setupWorkspace()
	{
		workspace.command("set model-version \"sheep-wolves-grass\"");
		setParameter("initial-number-sheep", 100);
		setParameter("initial-number-wolves", 50);
		setParameter("grass-regrowth-time", 30);
	}

	static void setParameter(String parameter, double value)
	{
		workspace.command("set "+ parameter + " " + String.valueOf(value));
	}

	static double[] randomParams(double[][] ranges)
	{
		int n = ranges.length;
		double[] params = new double[n];
		for (int i=0; i<n; i++){
			params[i] = ranges[i][0] + (ranges[i][1] - ranges[i][0])*random.nextDouble();
			System.out.println(paramNames[i] + " = " + fm2.format(params[i]));
		}
		return params;
	}

	static double tryRandomParams(double[][] ranges)
	{
		return tryValues(randomParams(ranges));
	}

	/*
	  Return the minimum number of wolves at the end of 'stepsToRun' steps, after
	  'numRepetitions' trials.
	*/
	static double tryValues(double[] params)
	{
		int n = params.length;
		for (int i=0; i<n; i++)
			setParameter(paramNames[i], params[i]);

		int rep = 0;
		double minMeasured = 10000;  // Stands for infinity.
		double measured = 0;
		for (rep = 0; rep < numRepetitions; rep++){
			workspace.command("setup");
			// This is much slower!
			// int step = 0;
			// for (step = 0; step < stepsToRun; step++){
			// 	workspace.command("go");
			// 	countWolves = ((Double)workspace.report(measuredReporter)).intValue();
			// 	if (0 == countWolves)
			// 		break;
			// }
			workspace.command("repeat " + stepsToRun + " [go]");
			measured = ((Double)workspace.report(measuredReporter)).intValue();
			if (measured < minMeasured){
				minMeasured = measured;
			}				
			System.out.println(measuredReporter + " = " + fm2.format(measured));
			if (0 == measured){ // We know the minimum, no reason to continue.
				System.out.println("rep = " + rep);
				break;
			}
		}
		System.out.println("Min = " + minMeasured);
		return minMeasured;
	}

	static void insertAmongLargest(Experiment newExp, Experiment[] bestExps)
	/* 
	   Insert into the array of parameter sets, 
	   ordered in decreasing order by their last element.
	 */
	{
		int numBest = bestExps.length;
		for (int i = 0; i < numBest; i++){
			if (newExp.output > bestExps[i].output){
				for (int j = numBest-1; j > i; j--){
					bestExps[j] = bestExps[j-1];
				}
				bestExps[i] = newExp;
				break;
			}
		}
	}

	static Experiment[] trySomeRandomParams(double[][] ranges, int times,
	  Experiment[] starters)
	/* 
	   Returns the best numBest parameter sets among the 'times' ones tried,
	   ordered by decreasing output, inserted into the starter parameter sets.
	 */
	{
		int numBest = starters.length;
		Experiment[] bestExps = new Experiment[numBest];
		for (int i=0; i<numBest; i++)
			bestExps[i] = starters[i];
		for (int i=0; i<times; i++){
			System.out.println("trial "+ i);
			double[] params = randomParams(ranges);
			double v = tryValues(params);
			if (v > bestExps[numBest-1].output){
				insertAmongLargest(new Experiment(params, v), bestExps);
			}
		}
		return bestExps;
	}

	/* 
	   Save to a comma-separated file some parameter choices.
	   The column headers are the parameter names, and each line is a set of choices.
	 */
	
	static void saveParamsToPath(Experiment[] bestExps, String paramNames[],
	  String outputName, Path path)
	{
		int numBest = bestExps.length;
		int n = paramNames.length;
		if (!(0 < numBest && n == bestExps[0].input.length)){
			System.err.println("Length mismatch.");
			System.exit(1);
		}
		FileWriter fw = null;
		try{
			fw = new FileWriter(path.toString());
			fw.write(paramNames[0]);
			for (int j = 1; j< n; j++){
				fw.write(", " + paramNames[j]);
			}
			fw.write(", " + outputName + "\n");
			for (int i = 0; i < numBest; i++){
				fw.write(bestExps[i].toString() + "\n");
			}
			fw.close();
		} catch (IOException x)
		{ x.printStackTrace(); }
	}

	static double[] scaleRange(double[] range, double center, double rangeScaleFactor)
	{
		if (2 != range.length || rangeScaleFactor < 0 || rangeScaleFactor > 1
		  || !(range[0] <=  center && center <= range[1])){
			System.out.println("Scaling operation is illegal.");
			System.exit(1);
		}
		double[] out = new double[2];
		for (int j = 0; j<2; j++)
			out[j] = center + (range[j] - center) * rangeScaleFactor;
		return out;
	}

	static double[][] scaleRanges(double[][] ranges, double[] centers,
	  double rangeScaleFactor)
	{
		int numRanges = ranges.length;
		double[][] out = new double[numRanges][2];
		for (int i = 0; i < numRanges; i++){
			out[i] = scaleRange(ranges[i], centers[i], rangeScaleFactor);
		}
		return out;
	}

	static void printParamRanges(double[][] ranges)
	{
		int n = ranges.length;

		String out = "{{" + fm2.format(ranges[0][0]) + ", "
			+ fm2.format(ranges[0][1]) + "}";
		for (int i=1; i<n; i++)
			out += ", {" + fm2.format(ranges[i][0]) + ", "
				+ fm2.format(ranges[i][1]) + "}";
		out += "}";
 		System.out.println("Ranges: " + out);
	}
}
