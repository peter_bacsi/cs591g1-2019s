globals [
	segregation             ;; Different globals for measuring the segregation
	segregation-list         ;; List of Segregation at different time steps
	seg50                    ;; Average Segregation after 50 rounds
	seg100                   ;; Average Segregation after 100 rounds
]

turtles-own [
	trustworthy?
	partnered?             ;; whether there is a current partner
	trust-expectation      ;; Expectation of meeting trustworty turtles based partly upon experience.
	partner                ;; nobody if not partnered
	last_partner
	trustor?                
	last-trust-expectation ;; diagnostic
]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;; prepare a field ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



to draw-cells                                                   ;create geographical obstacles
	let erasing? [pcolor = yellow] of patch mouse-xcor mouse-ycor
	while [mouse-down?]
	[ ask patch mouse-xcor mouse-ycor
		[ ifelse erasing?
			[set pcolor black ]
		[ set pcolor yellow ] ]
	display ]
end

to setup
	ca
	;  user-message("draw landscape"),
end

to finish-draw ;; part of setup
	;;ca
	ask turtles [die]
	;; set-default-shape turtles "face happy"
	set-default-shape turtles "circle"
	
	
	if num-persons > count patches with [pcolor = black] [
		user-message (word
		"In this world there is only space for  " count patches " turtles.")
		;; control number of agents in the world
		stop
	]
	
	ask n-of num-persons patches with [pcolor = black] [
		sprout 1 [
			set trustworthy? true
			set trustor? false
			set partnered? false
			set last_partner []
			set trust-expectation random-normal initial-trust-expectation  .2
			;; Priors for trust-memory are scattered around starting-trust-degree;
			;; normal distribution with standard deviation .2
			;; Can yield values outside the interval [0,1], but this is OK.
			ask turtles with [ trust-expectation >= .5 ] [ set color green ]
			ask turtles with [ trust-expectation < .5 ] [ set color red ]
		]
	]
	
	ask n-of int(percent-untrustworthy * num-persons / 100) turtles [
		set trustworthy?  false
		;; set shape "face sad"
		set shape "x"
		set size 1.5
	]
	set segregation-list []
	reset-ticks
end

to go
	ask turtles [
		set partnered? false
		set trustor? false
	]
	ask turtles [ partner-up ]
	let partnered-turtles turtles with [ partnered? ]
	if any? partnered-turtles [
		ask partnered-turtles [select-action]
	]
	ask turtles [
		ifelse trust-expectation >= .5 [set color green] [set color red]
	]
	ask turtles [  find-new-spot  ]
	;; Move around randomly; distance covered each round is controlled by variable mobility.
	if all? turtles [trustworthy?] [ stop ]
	if all? turtles [not trustworthy?] [ stop ]
	if (ticks mod 5 = 0) [update-segregation]
	if ticks > 999 [stop]
	if ticks > 220 [if all? turtles [color = red] [stop]]
	if ticks > 220 [
		if count turtles with [color = green] > 1490 [stop]
		;; Gacs: this is hard-wiring...
	]
	tick
end

to partner-up ; turtle
	let l last_partner
	if not partnered? [
		let o other turtles in-radius 1 with [ not partnered? and self != l ]
		if any? o [
			set partner one-of o
			set trustor? true
			;; The one who found the partner becomes the trustor.
			set partnered? true
			ask partner	[
				set partnered? true
				set partner myself
			]
		]
	]
	set last_partner partner
end

to find-new-spot ; turtle
	rt random-float 360
	let k 0
	while [k < mobility][
		fd 1
		if pcolor = yellow [ ; obstacle
			rt 180
			fd 1
			rt random-float 360
		]
		set k k + 1
	]
	if any? other turtles-here [ find-new-spot ]
	move-to patch-here
end


to select-action ; turtle: which strategy
	if trustor?
	[
		ifelse trust-expectation >= .5 [
			play-trust-game partner
		]
		[ defect partner ]		
	]
end

to play-trust-game [ agent2 ] ; turtle
	;; willing to place trust
	ifelse [trustworthy?] of agent2
	[ coop agent2 ]
	[ mix-strategy agent2 ]
end

to-report update-trust [trust new-info weight social-factor]
	let new-trust (trust * (1 - weight * social-factor) + new-info * weight * social-factor)
	report new-trust
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; consequences of strategy choices
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to defect [agent2] ; turtle
	;; self learns nothing, agent2 learns that self thinks others are untrustworthy
	let w weight-new-information / 100

	ask agent2 [
		let new-info 0
		set trust-expectation (update-trust trust-expectation new-info w social-learning-factor)
	]
end

to coop [agent2] ; turtle
	;; trusting trustor and trustworthy trustee
	;; self learns about trustworthiness of agent2,
	;; agent2 learns about trust expectation of agent1.
	let w weight-new-information / 100

	let new-info-1 1
	set trust-expectation (update-trust trust-expectation new-info-1 w 1)

	ask agent2 [
		let new-info-2 1
		set trust-expectation (update-trust trust-expectation new-info-2 w social-learning-factor)
	]
end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


to mix-strategy [agent2] ; turtle
	;; Negative feedback for agent 1 (trustor), positive indirect feedback for trustee
	let w weight-new-information / 100

	let new-info-1 0
	set trust-expectation (update-trust trust-expectation new-info-1 w 1)

	ask agent2 [
		let new-info-2 1
		set trust-expectation (update-trust trust-expectation new-info-2 w social-learning-factor)
	]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; level of segregation;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to update-segregation

	;; let sizeseg 17
	;; size of the boxes for segregation measure
	let seg max([pxcor] of patches)
	let segregationcalc 0
	ifelse ((2 * seg + 1) mod sizeseg != 0)   or  (max([pxcor] of patches) != max([pycor] of patches))
	;; Segregation only defined if field is quadratic and length divisible by sizeseg
	[
		user-message("Cannot calculate seg: see code.")
		set segregationcalc 1000
	]
	[
		let numseg ( 2 * seg + 1) / sizeseg   ;Number of boxes in one column
		ifelse count turtles with [ color = green ] < numseg * numseg
		or count turtles with [ color = red ] < numseg * numseg
		;; If there are two few turtles for segregation measure to be meaningful set 0
		;; (too few = number of squares)
		[set segregationcalc 0]
		[
			set segregationcalc 0
			let x 0
			let y 0
			let trustall  count turtles with [color = green]
			let suspall   count turtles with [color = red]
			while [ y < numseg]
			;; Calculates the segregation on subfields in the shape (0;0), (1;0), (2,0)... (0,1),(1,1)...
			[
				let trusthere count turtles with [
					xcor >= (- seg + x * sizeseg)  and
					xcor < (- seg + (x + 1) *  sizeseg)	and
					ycor >= (- seg + y * sizeseg) and
					ycor < (- seg + (y + 1) *  sizeseg)  and
					color = green
				]
				;; measure for trusting turtles in the respective field
				let susphere  count turtles with [
					xcor >= (- seg + x * sizeseg)  and
					xcor < (- seg + (x + 1) *  sizeseg)  and
					ycor >= (- seg + y * sizeseg)  and
					ycor < (- seg + (y + 1) *  sizeseg)  and
					color = red
				]
				set segregationcalc segregationcalc +  abs (trusthere / trustall - susphere / suspall )
				;; Last summand is the measure for segregation
				ifelse x < numseg - 1
				;; Enumerate Blocks by first counting up in the x-dimension, then y
				[set x   x + 1]
				[
					set x   0
					set y   y + 1
				]
			]
		]
		
	]
	set segregationcalc segregationcalc / 2
	set segregation precision segregationcalc 3
	
	
	if ticks = 50  [set segregation-list lput segregation segregation-list set seg50 segregation]
	;; List clustering coefficient every 50 rounds
	if ticks = 100 [set segregation-list lput segregation segregation-list set seg100 segregation]
	if ticks = 150 [set segregation-list lput segregation segregation-list]
	if ticks = 200 [set segregation-list lput segregation segregation-list]
	if ticks = 250 [set segregation-list lput segregation segregation-list]
	if ticks = 300 [set segregation-list lput segregation segregation-list]
	if ticks = 350 [set segregation-list lput segregation segregation-list]
	if ticks = 400 [set segregation-list lput segregation segregation-list]
	if ticks = 450 [set segregation-list lput segregation segregation-list]
	if ticks = 500 [set segregation-list lput segregation segregation-list]
	
	
end
@#$#@#$#@
GRAPHICS-WINDOW
600
24
1094
519
-1
-1
6.0
1
10
1
1
1
0
1
1
1
-40
40
-40
40
0
0
1
ticks
30.0

BUTTON
14
33
83
66
setup
setup\nfinish-draw
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
94
34
157
67
step
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
172
35
235
68
  go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
12
78
184
111
num-persons
num-persons
0
2500
2500.0
1
1
NIL
HORIZONTAL

SLIDER
12
119
218
152
initial-trust-expectation
initial-trust-expectation
0
1
0.48
0.01
1
NIL
HORIZONTAL

MONITOR
232
249
416
294
Distrusting Turtles
count turtles with [color = red]
1
1
11

SLIDER
185
78
357
111
mobility
mobility
0
20
1.0
1
1
NIL
HORIZONTAL

SLIDER
12
161
208
194
percent-untrustworthy
percent-untrustworthy
0
100
30.0
1
1
NIL
HORIZONTAL

SLIDER
12
201
208
234
weight-new-information
weight-new-information
0
15
3.0
1
1
NIL
HORIZONTAL

MONITOR
13
248
210
293
Trusting Turtles
count turtles with [color = green]
0
1
11

BUTTON
247
36
359
69
external shock
ask turtles [set trust-expectation trust-expectation - ((random 5) / 10)]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
118
306
320
351
Number of Trustworthy turtles
Count turtles with [trustworthy?]
0
1
11

MONITOR
15
306
94
351
NIL
segregation
3
1
11

SLIDER
312
121
498
154
social-learning-factor
social-learning-factor
0
2
1.0
0.2
1
NIL
HORIZONTAL

BUTTON
445
34
593
67
Setup with drawing
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
468
80
587
113
finish drawing
finish-draw
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
448
164
580
197
create obstacles
draw-cells
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
237
172
299
232
sizeseg
9.0
1
0
Number

PLOT
47
381
247
531
optimists
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot count turtles with [color = green]"

@#$#@#$#@
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
0
Rectangle -7500403 true true 151 225 180 285
Rectangle -7500403 true true 47 225 75 285
Rectangle -7500403 true true 15 75 210 225
Circle -7500403 true true 135 75 150
Circle -16777216 true false 165 76 116

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.4
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="MASTER SIMULATION SECOND ORDER" repetitions="1" runMetricsEveryStep="false">
    <setup>setup
finish-draw</setup>
    <go>go</go>
    <metric>count turtles with [color = green] / num-persons</metric>
    <metric>seg50</metric>
    <metric>seg100</metric>
    <metric>mean [trust-expectation] of turtles with [ shape = "x" ]</metric>
    <metric>mean [trust-expectation] of turtles with [ shape = "circle"]</metric>
    <steppedValueSet variable="initial-trust-expectation" first="0.4" step="0.05" last="0.8"/>
    <steppedValueSet variable="percent-untrustworthy" first="30" step="2" last="60"/>
    <steppedValueSet variable="mobility" first="1" step="1" last="20"/>
    <enumeratedValueSet variable="num-persons">
      <value value="1500"/>
    </enumeratedValueSet>
    <steppedValueSet variable="weight-new-information" first="3" step="1" last="10"/>
    <enumeratedValueSet variable="random-seed">
      <value value="1000"/>
      <value value="1001"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-learning-factor">
      <value value="0"/>
      <value value="0.5"/>
      <value value="1.5"/>
      <value value="2"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="MASTER SIMULATION" repetitions="1" runMetricsEveryStep="false">
    <setup>setup
finish-draw</setup>
    <go>go</go>
    <metric>count turtles with [color = green] / num-persons</metric>
    <metric>seg50</metric>
    <metric>seg100</metric>
    <metric>mean [trust-expectation] of turtles with [ shape = "x" ]</metric>
    <metric>mean [trust-expectation] of turtles with [ shape = "circle"]</metric>
    <steppedValueSet variable="initial-trust-expectation" first="0.4" step="0.05" last="0.8"/>
    <steppedValueSet variable="percent-untrustworthy" first="30" step="2" last="60"/>
    <steppedValueSet variable="mobility" first="1" step="1" last="20"/>
    <enumeratedValueSet variable="num-persons">
      <value value="1500"/>
    </enumeratedValueSet>
    <steppedValueSet variable="weight-new-information" first="3" step="1" last="10"/>
    <enumeratedValueSet variable="random-seed">
      <value value="1000"/>
      <value value="1001"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-learning-factor">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="MASTER SIMULATION Shocks" repetitions="1" runMetricsEveryStep="false">
    <setup>setup
finish-draw</setup>
    <go>go
if ticks = 200 [ask turtles [set trust-expectation trust-expectation - (random 5 / 10)]]</go>
    <metric>count turtles with [color = green] / num-persons</metric>
    <metric>seg50</metric>
    <metric>seg100</metric>
    <metric>mean [trust-expectation] of turtles with [ shape = "x" ]</metric>
    <metric>mean [trust-expectation] of turtles with [ shape = "circle"]</metric>
    <steppedValueSet variable="initial-trust-expectation" first="0.4" step="0.05" last="0.8"/>
    <steppedValueSet variable="percent-untrustworthy" first="30" step="2" last="60"/>
    <steppedValueSet variable="mobility" first="1" step="1" last="20"/>
    <enumeratedValueSet variable="num-persons">
      <value value="1500"/>
    </enumeratedValueSet>
    <steppedValueSet variable="weight-new-information" first="3" step="1" last="10"/>
    <enumeratedValueSet variable="random-seed">
      <value value="1000"/>
      <value value="1001"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-learning-factor">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
