;;______________________________________________________________________________
;;This source code accompanies:
;;Premo, L. S. and J. R. Brown (2019) The opportunity cost of walking away in the spatial iterated prisoner's dilemma. Theoretical Population Biology.

;;Corresponding Author and Programmer:
;;L.S. Premo (luke.premo@wsu.edu)
;;Department of Anthropology
;;Washington State University
;;Pullman, WA 99164-4910

;;Please see "Info" tab for full model description.

;;See Tools->BehaviorSpace to run the experiments published in the paper.

;;This model runs on NetLogo version 6.0.2--modifications may be required to run on later versions of NetLogo.
;;______________________________________________________________________________



globals [                   ;;global variables
	total-exploitation
	;;the total number of times the Sucker's payoff is awarded throughout the simulation
	total-births              ;;the total number of births throughout the simulation
	total-deaths              ;;the total number of deaths throughout the simulation
	error-count               ;;the total number of times that the "wrong" strategy is played due to error
	mutation-count            ;;the total number of mutations throughout the simulation
	games-played-count
	;;cumulative number of strategies played by all agents
	;; (one game between 2 players equates to 2 strategies played)
	WAC-won                   ;;binary; 0=WAC did not become fixed in the pop, 1=WAC did become fixed
	WAD-won                   ;;binary; 0=WAD did not become fixed in the pop, 1=WAD did become fixed
	NC-won                    ;;binary; 0=NC did not become fixed in the pop, 1=NC did become fixed
	ND-won                    ;;binary; 0=ND did not become fixed in the pop, 1=ND did become fixed
	WAC-percent-paired
	;; the proportion of WACs that had a partner and thus played the prisoner's dilemma
	;; during the current time step. (If 10 out of 20 WACs have a partner,
	;; then WAC-percent-paired = 10/20 = 0.50.)
	WAD-percent-paired        ;;the proportion of WADs that had a partner and thus played the prisoner's dilemma during the current time step. (If 10 out of 30 WADs have a partner, then WAD-percent-paired = 10/30 = 0.33.)
	NC-percent-paired
	;;the proportion of NCs that had a partner and thus played the prisoner's dilemma during the current time step.
	ND-percent-paired
	;;the proportion of NDs that had a partner and thus played the prisoner's dilemma during the current time step.
	total-percent-paired
	;;the proportion of all agents that had a partner and thus played the prisoner's dilemma during the current time step.
	WAC-assortment
	;;the proportion of WACs with a partner that played against another WAC during the current time step
	;; (If a) 10 out of 20 WACs have a partner and
	;; b) only 2 of those 10 WACs are in a dyad together, then WAC-assortment = 2/10 = 0.20.)
	WAD-assortment
	;;the proportion of WADs with a partner that played against another WAD during the current time step (If a) 10 out of 30 WADs have a partner and b) all 10 of them are paired with a fellow WAD, resulting in 5 WAD-WAD dyads, then WAD-assortment = 10/10 = 1.)
	NC-assortment
	;;the proportion of NCs with a partner that played against another NC during the current time step
	ND-assortment
	;;the proportion of NDs with a partner that played against another ND during the current time step
	total-assortment
	;;the proportion of dyads in which BOTH partners are the same breed (e.g., dyads of WAD-WAD, WAC-WAC, NC-NC, or ND-ND) during the current time step
	mean-cluster-size         ;;the average number of other agents within dispersal-radius of each agent
	sd-cluster-size           ;;the standard deviation of the number of other agents within dispersal-radius of each agent
]

breed [WACs WAC]            ;;walk away cooperators
breed [WADs WAD]            ;;walk away defectors
breed [NCs NC]              ;;naïve cooperators
breed [NDs ND]              ;;naïve defectors

;;___________________________________________

turtles-own [               ;;agent variables
	strategy                  ;;0=defect, 1=cooperate
	exploited                 ;;0=false, 1=true
	partner                   ;;nobody or the agent who is the partner
	played-already            ;;0=false, 1=true
	moving                    ;;0=false, 1=true
	energy                    ;;set between 0 and 49 at intialization.
	times-played              ;;count the number of games played
	times-without-partner     ;;count the number of time steps without a partner
	tried-to-pair
	;;0=I have not tried to pair yet or 1=I have tried to pair already this time step
	potential-breed-list      ;;for use with mutation
	sum-of-payoffs            ;;the sum of all payoffs awarded throughout the agent's lifetime
	cluster-size              ;;number of other agents within dispersal-radius
]



;;___________________________________________

to setup                                      ;;Observer method
	clear-all                                   ;;reset the simulation
	resize-world 0 world-max-xy 0 world-max-xy
	;;this resizes the lattice at the start.
	;; Use "world-max-xy" in behaviorSpace to vary world-size accordingly.
	random-seed seed                            ;;set the seed of the random number generator
	setup-players                               ;;initialize the agents
	set total-exploitation 0                    ;;set the initial values of the global variables
	set total-births 0
	set total-deaths 0
	set error-count 0
	set games-played-count 0
	set mutation-count 0
	set WAC-won 0
	set WAD-won 0
	set NC-won 0
	set ND-won 0
	set WAC-percent-paired 0
	set WAD-percent-paired 0
	set NC-percent-paired 0
	set ND-percent-paired 0
	set total-percent-paired 0
	set WAC-assortment 0
	set WAD-assortment 0
	set NC-assortment 0
	set ND-assortment 0
	set total-assortment 0
	with-local-randomness
	;; Gacs: no randomness here, but seems that local randomness is set at all diagnostics.
	[
		set mean-cluster-size mean [cluster-size] of turtles
		;;this must come AFTER setup-players
		set sd-cluster-size standard-deviation [cluster-size] of turtles
		;;this must come AFTER setup-players
	]
	reset-ticks
end                                          ;;ends "setup" method

;;___________________________________________





to setup-players
	;;Observer method called by "setup" to initialize agents
	;;First, create the appropriate number of agents for WAC, WAD, NC, and ND, which each get a different color.
	;;Note that each type is created as a different "breed" of turtle.
	;;Cooperators have strategy=1 and defectors have strategy=0.
	create-WACs n-WACs
	[
		set color cyan
		set strategy 1
	]
	
	create-WADs n-WADs
	[
		set color red
		set strategy 0
	]
	
	create-NCs n-NCs
	[
		set color lime
		set strategy 1
	]
	
	create-NDs n-NDs
	[
		set color orange
		set strategy 0
	]
	
	;;Second, ask all agents (regardless of breed) to set their state variables
	ask turtles
	[
		setxy random-xcor random-ycor         ;;each agent is placed randomly on the lattice
		set heading one-of [0 90 180 270]     ;;each agent randonly gets one of four headings
		set energy (random 50)                ;;each agent starts with a randomly chosen integer between 0 and 50 (not including 50); this is how it was done in Aktipis 2004
		set partner nobody                    ;;agents start out with no partner
		set size 2                            ;;for visualization
		set tried-to-pair 0                   ;;agents have not tried to pair at the start
		set sum-of-payoffs 0                  ;;agents have not earned any payoffs at the start
	]
	
	with-local-randomness
	[
		ask turtles
		;;Third, after all agents are placed in space, calculate "cluster-size" for each
		[
			set cluster-size count other turtles in-radius dispersal-radius
		]
	]
	
end                                       ;;ends "setup-players" method

;;___________________________________________





to go
	
	if ticks = 1
	[
		write-data-to-textfile                 ;;collect data after tick 1 to record the initial conditions
	]
	
	tick                                    ;;time passes
	
	ask turtles
	;;Some basic "house cleaning": Deal with turtle variables that need to be reset before each time step
	[
		set moving 0                        ;;reset moving to 0
		set partner nobody                  ;;no one has a partner at the start of each tick
		set played-already 0                ;;no one has played at the start of each tick
		set tried-to-pair 0                 ;;no one has tried to pair at the start of each tick
		if breed = WACs [set color cyan]
		;;reset the colors to match the strategy; this is just for visualization and visual debugging
		if breed = WADs [set color red]
		if breed = NCs [set color lime]
		if breed = NDs [set color orange]
	]
	
	;;Find a partner: each turtle gets the chance to pair with a partner--any one of the currently unpaired turtles inhabiting the same cell
	while [any? turtles with [partner = nobody and tried-to-pair = 0]]
	;;while there are any players that do not have a partner AND have not tried to pair yet
	[
		ask one-of turtles with [partner = nobody and tried-to-pair = 0]
		;;then ask one-of the players that does not have a partner and has not yet tried this tick, to attempt to pair
		[
			pair
		]
	]
	
	;;Ask agents without a partner to update two of their state variables
	ask turtles with [partner = nobody]
	[
		set times-without-partner times-without-partner + 1
		;;increment the number of time steps spent without a partner
		set energy energy + non-player-pay
		;;this allows for the possibility that agents can get energy from the environment
		;; without playing the PD. non-player-pay is set to 0 for all results presented in Premo and Brown (2019).
	]
	
	
	;;Before playing the prisoner's dilemma, introduce "trembling hand" error.
	;; Note that all agents' strategy values will be reset at the end of the tick.
	ask turtles
	;;Even though not all agents will play the prisoner's dilemma, we ask all of them to undergo error.
	;;This is a bit faster and it makes the job of checking the observed to the expected error rate easier.
	[
		;;show strategy                                                            ;;LSP for testing
		if random-float 1 < error-prob                                             ;;if there is an error
		[
			set error-count error-count + 1
			;;increment the number of errors made, this provides a way to check to make sure the
			;; observed error rate is the same as the expected
			ifelse strategy = 1
			;;if ego is a cooperator...
			[
				set strategy 0
				;;then set the strategy to defect...
			]
			[
				set strategy 1
				;;else, ego is a defector, so set strategy to cooperate
			]
			;;show "There was an error.  My strategy is now:"                        ;;LSP for testing
			;;show strategy                                                          ;;LSP for testing
		]
	]
	
	
	;;Players with partners play the prisoner's dilemma
	while [count turtles with [partner != nobody and played-already = 0] > 0]
	;;while there are still paired partners who have yet to play
	[
		
		ask one-of turtles with [partner != nobody and played-already = 0]
		;; ask one of them to play with their partner, note that we do not ask every agent with a partner
		;; to play, because this would end up with each pair playing the PD twice per time step.
		[
			play
			;;this method updates energy and allows walk away types
			;; to set "moving" to 1 if they are defected upon by their partner
		]
	]
	
	
	
	;;Mortality due to "starvation"
	ask turtles with [energy <= 0]
	;;if energy level is less than or equal to 0
	[
		set total-deaths total-deaths + 1
		;;increment total-deaths (a global variable)
		die
		;;die from lack of energy, the turtle is removed from the simulation
	]
	
	
	;;Movement for agents who need to move either because
	;; 1) they did not have partner to play during this time step or
	;; 2) they are either WAC or WAD and their partner defected on them this time step
	ask turtles with [moving = 1]
	;;ask turtles who have set their "moving" flag to 1
	[
		move
	]
	
	
	
	;;Reproduction
	ask turtles with [energy >= 100]
	;;ask agents with energy greater than 100 to reproduce
	[reproduce]
	
	
	
	;;Carrying capacity mortality: population size is reduced according to the method described in
	;; Aktipis 2004
	if count turtles > carrying-capacity
	[
		enforce-carrying-capacity
	]
	
	
	;;Undo the effects of "trembling hand" error:
	;; reset the strategy of all agents to again accurately reflect their breed.
	ask turtles
	[
		ifelse breed = NCs or breed = WACs
		[set strategy 1]
		;;as a result of this, every cooperator that experienced error will go back to its default strategy
		[set strategy 0]
		;;as a result of this, every defector that experienced error will go back to its default strategy
	]
	
	
	;;Calculate assortment and percent paired metrics
	assortment-calc
	
	
	;;Collect data to an external textfile every 1000 ticks
	if ticks mod 1000 = 0
	;;data are collected every 1000 ticks. Change 1000 to collect data more or less frequently as you wish.
	[
		write-data-to-textfile
	]
	
	
	;;Plot data to the interface
	if plot-data = TRUE
	[
		with-local-randomness [plotting]
		;;Note that I use "with-local-randomness" here so that the run is the same
		;;regardless of whether the plots are used.
	]
	
	
	;;Stop conditions for all experiments
	ifelse mu > 0
	;;if there is mutation in the simulation (i.e., mu>0),
	[
		if ticks = 500000
		;;then we stop the run after a given number of time steps, in our case 500000.
		[
			write-data-to-textfile
			;;collect data to the textfile one last time
			stop
			;;stop the simulation run
		]
	]	;ends if part of ifelse mu > 0
	[
		;;else, if there is no mutation in the simulation (i.e., mu=0), then stop the run when one of the strategies has become "fixed" in the population.
		
		;;Stop condition for simulations initialized with all four strategies: NC, ND, WAC, and WAD
		if n-NCs > 0 and n-NDs > 0 and n-WACs > 0 and n-WADs > 0
		[
			if (count NCs = 0 and count WADs = 0 and count WACs = 0) or (count NDs = 0 and count WADs = 0 and count WACs = 0) or (count NDs = 0 and count NCs = 0 and count WACs = 0) or (count NDs = 0 and count NCs = 0 and count WADs = 0)
			;;exp1
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				if count NDs > 0 [set ND-won 1]
				if count WACs > 0 [set WAC-won 1]
				if count WADs > 0 [set WAD-won 1]
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop ;stop the simulation run
			]
		] ;ends if
		
		
		;;Stop condition for simulations initialized with three strategies: NC, ND, and WAD
		if n-NCs > 0 and n-NDs > 0 and n-WACs = 0 and n-WADs > 0
		[
			if (count NCs = 0 and count NDs = 0) or (count NDs = 0 and count WADs = 0) or (count WADs = 0 and count NCs = 0)  ;exp1b
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				if count NDs > 0 [set ND-won 1]
				if count WACs > 0 [set WAC-won 1]
				;;this one should not be possible
				if count WADs > 0 [set WAD-won 1]
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop   ;stop the simulation run
			]
		]                                                                      ;;ends if
		
		;;Stop condition for simulations initialized with three strategies: ND, WAC, and WAD
		if n-NCs = 0 and n-NDs > 0 and n-WACs > 0 and n-WADs > 0
		[
			if (count WACs = 0 and count WADs = 0) or (count NDs = 0 and count WACs = 0) or (count WADs = 0 and count NDs = 0) ;exp1c
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				;;this one should not be possible
				if count NDs > 0 [set ND-won 1]
				if count WACs > 0 [set WAC-won 1]
				if count WADs > 0 [set WAD-won 1]
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop
				;;stop the simulation run
			]
		]     ;;ends if
		
		;;Stop condition for simulations initialized with three strategies: NC, ND, and WAC
		if n-NCs > 0 and n-NDs > 0 and n-WACs > 0 and n-WADs = 0
		[
			if (count NCs = 0 and count WACs = 0) or (count NDs = 0 and count WACs = 0) or (count NCs = 0 and count NDs = 0) ;;exp1d
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				if count NDs > 0 [set ND-won 1]
				if count WACs > 0 [set WAC-won 1]
				if count WADs > 0 [set WAD-won 1]
				;;this one should not be possible
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop     ;;stop the simulation run
			]
		]                                                                      ;;ends if
		
		;;Stop condition for simulations initialized with three strategies: NC, WAC, and WAD
		if n-NCs > 0 and n-NDs = 0 and n-WACs > 0 and n-WADs > 0
		[
			if (count NCs = 0 and count WADs = 0) or (count NCs = 0 and count WACs = 0) or (count WADs = 0 and count WACs = 0) ;;exp1e
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				if count NDs > 0 [set ND-won 1]
				;;this one should not be possible
				if count WACs > 0 [set WAC-won 1]
				if count WADs > 0 [set WAD-won 1]
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop   ;;stop the simulation run
			]
		]             ;;ends if
		
		
		;;Stop condition for NC vs. WAC sims
		if n-NCs > 0 and n-NDs = 0 and n-WACs > 0 and n-WADs = 0
		[
			if count NCs = 0 or count WACs = 0 ;;exp2
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				if count NDs > 0 [set ND-won 1]
				;;this one should not be possible
				if count WACs > 0 [set WAC-won 1]
				if count WADs > 0 [set WAD-won 1]
				;;this one should not be possible
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop   ;;stop the simulation run
			]
		]             ;;ends if
		
		
		;;Stop condition for NC vs. ND sims
		if n-NCs > 0 and n-NDs > 0 and n-WACs = 0 and n-WADs = 0
		[
			if count NCs = 0 or count NDs = 0 ;;exp3
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				if count NDs > 0 [set ND-won 1]
				if count WACs > 0 [set WAC-won 1]
				;;this one should not be possible
				if count WADs > 0 [set WAD-won 1]
				;;this one should not be possible
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop          ;;stop the simulation run
			]
		]                      ;;ends if
		
		
		;;Stop condition for ND vs. WAC sims
		if n-NCs = 0 and n-NDs > 0 and n-WACs > 0 and n-WADs = 0
		[
			if count WACs = 0 or count NDs = 0 ;;exp4
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				;;this one should not be possible
				if count NDs > 0 [set ND-won 1]
				if count WACs > 0 [set WAC-won 1]
				if count WADs > 0 [set WAD-won 1]
				;;this one should not be possible
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop              ;;stop the simulation run
			]
		]                             ;;ends if
		
		
		;;Stop condition for ND vs. WAD sims
		if n-NCs = 0 and n-NDs > 0 and n-WACs = 0 and n-WADs > 0
		[
			if count NDs = 0 or count WADs = 0 ;;exp5
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				;;this one should not be possible
				if count NDs > 0 [set ND-won 1]
				if count WACs > 0 [set WAC-won 1]
				;;this one should not be possible
				if count WADs > 0 [set WAD-won 1]
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop
				;;stop the simulation run
			]
		]       ;ends if
		
		
		;;Stop condition for NC vs. WAD sims
		if n-NCs > 0 and n-NDs = 0 and n-WACs = 0 and n-WADs > 0
		[
			if count NCs = 0 or count WADs = 0 ;;exp6
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				if count NDs > 0 [set ND-won 1]
				;;this one should not be possible
				if count WACs > 0 [set WAC-won 1]
				;;this one should not be possible
				if count WADs > 0 [set WAD-won 1]
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop         ;;stop the simulation run
			]
		]              ;;ends if
		
		
		;;Stop condition for WAC vs. WAD sims
		if n-NCs = 0 and n-NDs = 0 and n-WACs > 0 and n-WADs > 0
		[
			if count WACs = 0 or count WADs = 0 ;;exp7
			[
				;;record the winner -- the strategy that evolved to fixation
				if count NCs > 0 [set NC-won 1]
				;;this one should not be possible
				if count NDs > 0 [set ND-won 1]
				;;this one should not be possible
				if count WACs > 0 [set WAC-won 1]
				if count WADs > 0 [set WAD-won 1]
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop        ;;stop the simulation run
			]
		]           ;;ends if
		
		
		
		;;Stop condition for NC-only sims
		if n-NCs > 0 and n-NDs = 0 and n-WACs = 0 and n-WADs = 0
		[
			if total-births >= 5000
			;;stop after 5,000 births, this controls for the number of births rather than the number of ticks
			[
				if count NCs > 0 [set NC-won 1]
				;;Note they didn't really "win" because they were the only type present, but we don't use these data anyway.
				
				with-local-randomness
				[
					ask turtles
					[
						set cluster-size count other turtles in-radius dispersal-radius
						;;ask each agent to calculate cluster-size
					]
					set mean-cluster-size mean [cluster-size] of turtles
					;;record the mean cluster size as a global variable
					set sd-cluster-size standard-deviation [cluster-size] of turtles
					;;record the standard deviation of the cluster size as a global variable
				]
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop                      ;;stop the simulation run
			]
		]                                 ;;ends if
		
		
		
		;;Stop condition for WAC-only sims
		if n-NCs = 0 and n-NDs = 0 and n-WACs > 0 and n-WADs = 0
		[
			if total-births >= 5000
			;;stop after 5,000 births, this controls for the number of births rather than the number of ticks
			[
				if count WACs > 0 [set WAC-won 1]
				;;Note they didn't really "win" because they were the only type present, but we don't use these data anyway.
				
				with-local-randomness
				[
					ask turtles
					[
						set cluster-size count other turtles in-radius dispersal-radius
						;;ask each agent to calculate cluster-size
					]
					set mean-cluster-size mean [cluster-size] of turtles
					;;record the mean cluster size as a global variable
					set sd-cluster-size standard-deviation [cluster-size] of turtles
					;;record the standard deviation of the cluster size as a global variable
				]
				write-data-to-textfile
				;;collect data to the textfile one last time
				stop
				;;stop the simulation run
			]
		]              ;;ends if
		
	]     ;;ends else part of ifelse mu > 0
	
end
;;ends "go" method

;;___________________________________





to pair
	;;This is a turtle method called from "go" method
	
	set tried-to-pair 1
	;;sets tried-to-pair to 1 to record the fact that by the end of this method the agent will have had an attempt to find a partner this time step
	
	ifelse any? other turtles with [partner = nobody]
	;;If there is at least one potential partner out there... [This provides an extra layer of safety to guard against the "odd man out" problem whereby in the case of an odd number of agents, the last one without a partner just keeps searching forever to no avail.]
	[
		
		if partner = nobody
		;;If I do not have a partner...[This is just a safety check that should never be false. If I have a partner already, then the next command block is skipped. But this should never be the case.]
		[
			ifelse any? other turtles-here with [partner = nobody]
			;;...and there is someone else here on this very same cell that could be my partner
			[
				set partner one-of other turtles-here with [partner = nobody]
				;;set my partner to one of the other unpaired agents on my cell
				set color white
				;;for visual debugging, set color to white to reflect that I have paired with a partner
				;;show partner                                                ;;LSP for testing
				
				ask partner
				;;now ask my new partner to update its variables to reflect that fact this it is now paired as well
				[
					set partner myself
					;;set partner to myself
					set color gray
					;;for visual debugging, set color to gray to reflect that I have paired with a partner, and that I was found in this case
					;;show partner                                             ;;LSP for testing
				]
			]
			[
				;;else, there is no one here that could be my partner...
				set moving 1
				;;so I will instead simply set my "moving" flag to 1 so that later on in the time step I will move randomly left or right.
			]
		]
		;;ends if partner = nobody
	]
	;;ends if part of ifelse
	[
		;;else, I'm the last one to the party--everyone else has a partner
		if partner = nobody
		;;if i do not have a partner...this is another safety check that should never be false
		[
			set moving 1
			;;set my "moving" flag to 1 so that later on in the time step I will move randomly one cell to my left or right
		]
	]
end                                                               ;;ends "pair" method

;;___________________________________________




to play
	;;turtle method called by "go".  Only turtles that have a partner are asked to "play".  More specifically, one member of each pair is asked to "play".
	
	;;update ego's state variables
	set times-played times-played + 1              ;;increment times played
	set played-already 1
	;;set the "played-already" flag to 1 to indicate that ego has played this time step
	set games-played-count games-played-count + 1
	;;increment totals games-played-count (global variable)
	
	
	ask partner
	;;update ego's partner's state variables
	[
		set times-played times-played + 1             ;;increment times played
		set played-already 1
		;;set the "played-already" flag to 1 to indicate that ego has played this time step
		set games-played-count games-played-count + 1
		;;increment totals games-played-count (global variable)
	]
	
	
	;;Assign payoffs to ego and partner, and allow walk away types to set their "move" flag if they were defected upon by their partner while playing the PD
	
	if strategy = 1 and [strategy] of partner = 1
	;;If ego and partner cooperated, both get the reward payoff and no one moves
	[
		set energy energy + reward                   ;;ego gets reward
		set sum-of-payoffs sum-of-payoffs + reward   ;;update ego's sum-of-payoffs
		ask partner
		[
			set energy energy + reward                 ;;ego's partner gets reward
			set sum-of-payoffs sum-of-payoffs + reward ;;update ego's partner's sum-of-payoffs
		]
	]
	
	
	
	if strategy = 1 and [strategy] of partner = 0
	;;If ego cooperated but partner defected, ego gets S, partner gets T, and ego might need to move in retaliation
	[
		set energy energy + sucker                    ;;ego gets sucker's payoff
		set sum-of-payoffs sum-of-payoffs + sucker    ;;update ego's sum-of-payoffs
		set exploited exploited + 1                   ;;update number of times ego was exploited
		set total-exploitation total-exploitation + 1 ;;update total number of exploitations in the simulation (global variable)
		
		if breed = WACs or breed = WADs
		;;if ego is a WAC or WAD, then ego will need to move in retaliation to the defection
		[
			set moving 1
			;;set "moving" flag to 1 so that ego will move later in the tick
			;;show moving                               ;;LSP for testing
			set color yellow                            ;;LSP for visual debugging
		]
		
		ask partner
		[
			set energy energy + temptation                  ;;ego's partner gets temptation
			set sum-of-payoffs sum-of-payoffs + temptation  ;;update ego's partner's sum-of-payoffs
		]
		
	]
	
	
	if strategy = 0 and [strategy] of partner = 0
	;;If ego and partner defected, then both get the punishment payoff and both may need to move in retaliation to the defection by the other
	[
		set energy energy + punishment                     ;;ego gets punishment payoff
		set sum-of-payoffs sum-of-payoffs + punishment     ;;update ego's sum-of-payoffs
		
		if breed = WADs or breed = WACs
		;;if ego is a WAC or WAD, then ego will need to move in retaliation to the defection
		[
			set moving 1
			;;set "moving" flag to 1 so that ego will move later in the tick
			set color yellow                                 ;;LSP for visual debugging
		]
		
		ask partner
		[
			set energy energy + punishment                     ;;partner gets punishment payoff
			set sum-of-payoffs sum-of-payoffs + punishment     ;;update partner's sum-of-payoffs
			
			if breed = WADs or breed = WACs
			;;if partner is a WAC or WAD, then partner will need to move in retaliation to the defection
			[
				set moving 1
				;;set "moving" flag to 1 so that partner will move later in the tick
				set color yellow                                 ;;LSP for visual debugging
			]
		]
	]
	
	
	if strategy = 0 and [strategy] of partner = 1

	;;If ego defected and partner cooperated, then ego gets temptation, partner gets sucker's payoff, and partner may need to retaliate to the defection by ego
	[
		set energy energy + temptation                     ;;ego gets temptation payoff
		set sum-of-payoffs sum-of-payoffs + temptation     ;;update ego's sum-of-payoffs
		
		ask partner
		[
			set energy energy + sucker                         ;;partner gets sucker's payoff
			set sum-of-payoffs sum-of-payoffs + sucker         ;;update partner's sum-of-payoffs
			set exploited exploited + 1
			;;increment number of times partner was exploited
			set total-exploitation total-exploitation + 1
			;;update total number of exploitations in the simulation (global variable)
			
			if breed = WACs or breed = WADs
			;;if partner is a WAC or WAD, then partner will need to move in retaliation to the defection
			[
				set moving 1
				;;set "moving" flag to 1 so that partner will move later in the tick
				set color yellow                                ;;LSP for visual debugging
			]
		]
	]
	
end                                                  ;;ends "play" method

;;___________________________________________





to move

	;;Turtle method called in "go" method.  This code operationalizes the "side-step" random movement used in Aktipis (2004).
	ifelse random 2 < 1                  ;;With equal probability...
	[
		move-to patch-right-and-ahead 90 1   ;;the agent moves one cell to its right
		set heading heading + 90             ;;and resets its heading to face the direction it moved
	]
	[                                    ;;OR
		move-to patch-left-and-ahead 90 1    ;;the agent moves one cell to its left
		set heading heading - 90             ;;and resets its heading to face the direction it moved
	]
	
end                                    ;;ends "move" method

;;___________________________________________





to reproduce
	;;Asexual reproduction with mutation: turtle method called by "go" method
	set energy (energy / 2)
	;;reproduction requires that half of the parent's energy goes to the offspring, using "hatch" later in the method ensures that the offspring's energy will be set to the same value as the parent's
	
	;;show "This is the parent's breed:"                         ;;LSP for testing
	;;show breed                                                 ;;LSP for testing
	
	;;Note that mutation can only "reintroduce" strategies that were included at the start of the simulation run.
	ifelse random-float 1 < mu                                   ;;If there is a mutation at this birth...
	[
		set mutation-count mutation-count + 1                  ;;increment mutation count
		set potential-breed-list []
		;;reset this list each time just to be sure it is correct
		
		;;Build the list of potential types one could mutate to.
		;;The list can only contain types that were included at the start of the simulation.
		;;On this list 1=NC, 2=ND, 3=WAC, and 4=WAD.
		if n-NCs > 0
		;;if there were NCs at the start of the simulation
		[
			set potential-breed-list fput 1 potential-breed-list
			;;then add 1 to the potential-breed-list
			;;'show potential-breed-list                            ;;LSP for testing
		]
		if n-NDs > 0
		;;if there were NDs at the start of the simulation
		[
			set potential-breed-list fput 2 potential-breed-list  ;;then add 2 to the potential-breed-list
			;;show potential-breed-list                            ;;LSP for testing
		]
		if n-WACs > 0
		;;if there were WACs at the start of the simulation
		[
			set potential-breed-list fput 3 potential-breed-list  ;;then add 3 to the potential-breed-list
			;;show potential-breed-list                            ;;LSP for testing
		]
		if n-WADs > 0
		;;if there were WADs at the start of the simulation
		[
			set potential-breed-list fput 4 potential-breed-list  ;;then add 4 to the potential-breed-list
			;;show potential-breed-list                            ;;LSP for testing
		]
		
		;;show potential-breed-list                             ;;LSP for testing
		
		
		;;Now we need to remove the the parent's breed from this list of potential breeds so that mutation can not result in the same breed as the parent.
		;;show "I'm removing my breed from the list."  ;;LSP for testing
		if breed = NCs                                          ;;if I am NC
		[
			set potential-breed-list remove 1 potential-breed-list
			;;remove 1 from the potential-breed-list
			;;show potential-breed-list                              ;;LSP for testing
		]
		if breed = NDs                                          ;;if I am ND
		[
			set potential-breed-list remove 2 potential-breed-list
			;;remove 2 from the potential-breed-list
			;;show potential-breed-list                              ;;LSP for testing
		]
		if breed = WACs                                         ;;if I am WAC
		[
			set potential-breed-list remove 3 potential-breed-list
			;;remove 3 from the potential-breed-list
			;;show potential-breed-list                              ;;LSP for testing
		]
		if breed = WADs                                         ;;if I am WAD
		[
			set potential-breed-list remove 4 potential-breed-list
			;;remove 4 from the potential-breed-list
			;;show potential-breed-list                              ;;LSP for testing
		]
		
		
		let offspring-breed one-of potential-breed-list
		;;local variable called offspring-breed records what the offspring's breed will be as a result of mutation.
		;;show "A mutation occurred. This is the breed that will be assigned to the offspring as a result of mutation:"
		;;LSP for testing
		;;show offspring-breed                            ;;LSP for testing
		
		if offspring-breed = 1                            ;;remember that 1=NC
		[
			hatch-NCs 1
			;;the offspring hatches with same characteristics as the parent, *except* for the breed
			[
				;;show "I'm the offspring, and my breed is:"  ;;LSP for testing
				;;show breed                                  ;;LSP for testing
				ifelse offspring-dispersal = "global"         ;;global dispersal
				[
					setxy random-xcor random-ycor
					;;offspring appears anywhere in the lattice
				]
				[
					;;else, local dispersal: Hatched offspring appear in the cell with their parent, so here we simply ask the offspring to move to a new patch
					;;show "My X and Y were:"                    ;;LSP for testing
					;;show xcor                                  ;;LSP for testing
					;;show ycor                                  ;;LSP for testing
					move-to one-of other patches in-radius dispersal-radius
					;;the use of "other" here ensures that the offspring does not stay in the patch with its parent
					;;show "My X and Y after local dispersal:"   ;;LSP for testing
					;;show xcor                                  ;;LSP for testing
					;;show ycor                                  ;;LSP for testing
				]
				set times-played 0
				;;reset so that it is not the same as the parent's
				set times-without-partner 0
				;;reset so that it is not the same as the parent's
				set played-already 0
				;;reset so that it is not the same as the parent's
				set partner nobody
				;;reset so that it is not the same as the parent's
				set moving 0
				;;reset so that it is not the same as the parent's
				set sum-of-payoffs 0
				;;reset so that it is not the same as the parent's
			]
		]
		
		if offspring-breed = 2                          ;;remember that 2=ND
		[
			hatch-NDs 1
			;;the offspring hatches with same characteristics as the parent, *except* for the breed
			[
				;;show "I'm the offspring, and my breed is:" ;;LSP for testing
				;;show breed                                 ;;LSP for testing
				ifelse offspring-dispersal = "global"        ;;global dispersal
				[
					setxy random-xcor random-ycor               ;;offspring appears anywhere in the lattice
				]
				[
					;;else, local dispersal: Hatched offspring appear in the cell with their parent, so here we simply ask the offspring to move to a new patch
					;;show "My X and Y were:"                   ;;LSP for testing
					;;show xcor                                 ;;LSP for testing
					;;show ycor                                 ;;LSP for testing
					move-to one-of other patches in-radius dispersal-radius
					;;the use of "other" here ensures that the offspring does not stay in the patch with its parent
					;;show "My X and Y after local dispersal:"  ;;LSP for testing
					;;show xcor                                 ;;LSP for testing
					;;show ycor                                 ;;LSP for testing
				]
				set times-played 0
				;;reset so that it is not the same as the parent's
				set times-without-partner 0
				;;reset so that it is not the same as the parent's
				set played-already 0
				;;reset so that it is not the same as the parent's
				set partner nobody
				;;reset so that it is not the same as the parent's
				set moving 0
				;;reset so that it is not the same as the parent's
				set sum-of-payoffs 0
				;;reset so that it is not the same as the parent's
			]
		]
		
		
		if offspring-breed = 3                           ;;remember that 3=WAC
		[
			hatch-WACs 1
			;;the offspring hatches with same characteristics as the parent, *except* for the breed
			[
				;;show "I'm the offspring, and my breed is:"  ;;LSP for testing
				;;show breed                                  ;;LSP for testing
				ifelse offspring-dispersal = "global"         ;;global dispersal
				[
					setxy random-xcor random-ycor               ;;offspring appears anywhere in the lattice
				]
				[
					;;else, local dispersal: Hatched offspring appear in the cell with their parent, so here we simply ask the offspring to move to a new patch
					;;show "My X and Y were:"                   ;;LSP for testing
					;;show xcor                                 ;;LSP for testing
					;;show ycor                                 ;;LSP for testing
					move-to one-of other patches in-radius dispersal-radius
					;;the use of "other" here ensures that the offspring does not stay in the patch with its parent
					;;show "My X and Y after local dispersal:"  ;;LSP for testing
					;;show xcor                                 ;;LSP for testing
					;;show ycor                                 ;;LSP for testing
				]
				set times-played 0
				;;reset so that it is not the same as the parent's
				set times-without-partner 0
				;;reset so that it is not the same as the parent's
				set played-already 0
				;;reset so that it is not the same as the parent's
				set partner nobody
				;;reset so that it is not the same as the parent's
				set moving 0
				;;reset so that it is not the same as the parent's
				set sum-of-payoffs 0
				;;reset so that it is not the same as the parent's
			]
		]
		
		
		if offspring-breed = 4                           ;;remember that 4=WAD
		[
			hatch-WADs 1
			;;the offspring hatches with same characteristics as the parent, *except* for the breed
			[
				;;show "I'm the offspring, and my breed is:"  ;;LSP for testing
				;;show breed                                  ;;LSP for testing
				ifelse offspring-dispersal = "global"         ;;global dispersal
				[
					setxy random-xcor random-ycor               ;;offspring appears anywhere in the lattice
				]
				[
					;;else, local dispersal: Hatched offspring appear in the cell with their parent, so here we simply ask the offspring to move to a new patch
					;;show "My X and Y were:"                   ;;LSP for testing
					;;show xcor                                 ;;LSP for testing
					;;show ycor                                 ;;LSP for testing
					move-to one-of other patches in-radius dispersal-radius
					;;the use of "other" here ensures that the offspring does not stay in the patch with its parent
					;;show "My X and Y after local dispersal:"  ;;LSP for testing
					;;show xcor                                 ;;LSP for testing
					;;show ycor                                 ;;LSP for testing
				]
				set times-played 0                  ;;reset so that it is not the same as the parent's
				set times-without-partner 0         ;;reset so that it is not the same as the parent's
				set played-already 0                ;;reset so that it is not the same as the parent's
				set partner nobody                  ;;reset so that it is not the same as the parent's
				set moving 0                        ;;reset so that it is not the same as the parent's
				set sum-of-payoffs 0                ;;reset so that it is not the same as the parent's
			]
		]
		
	]                                        ;;ends the if part of ifelse random-float 1 < mu
	[
		;;else, there was no mutation in this birth, so the offspring hatches with same characteristics of parent, including breed
		hatch 1
		[
			;;show "There was no mutation. I'm the offspring and my breed is:"  ;;LSP for testing
			;;show breed                           ;;LSP for testing
			ifelse offspring-dispersal = "global"  ;;global dispersal
			[
				setxy random-xcor random-ycor          ;;offspring appears anywhere in the lattice
			]
			[
				;;else, local dispersal: Hatched offspring appear in the cell with their parent, so here we simply ask the offspring to move to a new patch within dispersal-radius of its current cell
				;;show "My X and Y were:"              ;;LSP for testing
				;;show xcor                            ;;LSP for testing
				;;show ycor                            ;;LSP for testing
				move-to one-of other patches in-radius dispersal-radius
				;;the use of "other" here ensures that the offspring does not stay in the patch with its parent
				;;show "My X and Y after local dispersal:"  ;;LSP for testing
				;;show xcor                                 ;;LSP for testing
				;;show ycor                                 ;;LSP for testing
			]
			set times-played 0                          ;;reset so that it is not the same as the parent's
			set times-without-partner 0                 ;;reset so that it is not the same as the parent's
			set played-already 0                        ;;reset so that it is not the same as the parent's
			set partner nobody                          ;;reset so that it is not the same as the parent's
			set moving 0                                ;;reset so that it is not the same as the parent's
			set sum-of-payoffs 0                        ;;reset so that it is not the same as the parent's
		]
	]                                             ;;ends the else part of ifelse random-float 1 < mu
	
	
	set total-births total-births + 1             ;;increment the number of total births (global variable)
end                                                  ;;ends "reproduce" method

;;___________________________________________




to enforce-carrying-capacity                     ;;Observer method called by the "go" method
	
	while [count turtles > carrying-capacity]
	;;While the population size is greater than the carrying capacity,
	[
		ask one-of turtles                           ;;ask one randomly chosen agent to...
		[
			set energy energy - 10                     ;;decrease its energy by 10 units.
			if energy <= 0
			;;If that decrease results in an energy level <= 0, then...
			[
				set total-deaths total-deaths + 1
				;;increment the total number of deaths in the simulation before...
				die
				;;killing and removing the agent from the simulation.
			]
		]
		;;repeat this random sample with replacement until enough agents have died to bring the population size back down to carrying-capacity.
	]
	
end                                              ;;ends "enforce-carrying-capacity" method

;;__________________________________________




to assortment-calc
	;;Observer method called by "go", this method calculates percent paired and assortment metrics by type as well as for the entire population
	
	;;Reset all of these global variables at the start of the method, so that any breed that might have gone extinct during the time step is updated as well.
	set WAC-percent-paired 0
	set WAD-percent-paired 0
	set NC-percent-paired 0
	set ND-percent-paired 0
	set total-percent-paired 0
	set WAC-assortment 0
	set WAD-assortment 0
	set NC-assortment 0
	set ND-assortment 0
	set total-assortment 0
	
	;;percent paired data
	set total-percent-paired count turtles with [partner != nobody] / count turtles
	;;The proportion of all agents that played the prisoner's dilemma during this time step.
	
	if count WACs > 0
	[
		set WAC-percent-paired count WACs with [partner != nobody] / count WACs
		;;The proportion of WACs that played the PD during this time step.
	]
	
	if count WADs > 0
	[
		set WAD-percent-paired count WADs with [partner != nobody] / count WADs
		;;The proportion of WADs that played the PD during this time step.
	]
	
	if count NCs > 0
	[
		set NC-percent-paired count NCs with [partner != nobody] / count NCs
		;;The proportion of NCs that played the PD during this time step.
	]
	
	if count NDs > 0
	[
		set ND-percent-paired count NDs with [partner != nobody] / count NDs
		;;The proportion of NDs that played the PD during this time step.
	]
	
	;;assortment data
	if count WACs with [partner != nobody] > 0
	[
		set WAC-assortment count WACs with
		[partner != nobody and breed = [breed] of partner] / count WACs with [partner != nobody]
		;;The proportion of WACs with a partner that happen to be partnered with another WAC this time step.
	]
	
	if count WADs with [partner != nobody] > 0
	[
		set WAD-assortment count WADs with [partner != nobody and breed = [breed] of partner] / count WADs with [partner != nobody]
		;;The proportion of WADs with a partner that happen to be partnered with another WAD this time step.
	]
	
	if count NCs with [partner != nobody] > 0
	[
		set NC-assortment count NCs with [partner != nobody and breed = [breed] of partner] / count NCs with [partner != nobody]
		;;The proportion of NCs with a partner that happen to be partnered with another NC this time step.
	]
	
	if count NDs with [partner != nobody] > 0
	[
		set ND-assortment count NDs with [partner != nobody and breed = [breed] of partner] / count NDs with [partner != nobody]
		;;The proportion of NDs with a partner that happen to be partnered with another ND this time step.
	]
	
	if count turtles with [partner != nobody] > 0
	[
		set total-assortment count turtles with [partner != nobody and breed = [breed] of partner] / count turtles with [partner != nobody]
		;;The proportion of agents with a partner that happen to be partnered with an agent of the same breed this time step.
	]
	
	
	if ticks mod 1000 = 0
	;;data are collected every 1000 ticks
	[
		with-local-randomness
		[
			ask turtles
			[
				set cluster-size count other turtles in-radius dispersal-radius
			]
			set mean-cluster-size mean [cluster-size] of turtles
			;;record the mean cluster size as a global variable
			set sd-cluster-size standard-deviation [cluster-size] of turtles
			;;record the standard deviation of the cluster size as a global variable
		]
	]
	
end                                                          ;;ends "assortment-calc" method

;;__________________________________________





to plotting
	;;Observer method called by the "go" method only when the global parameter "plot-data" = TRUE.
	
	set-current-plot "mean payoff/tick"                           ;;mean payoff/tick per agent, by type
	
	set-current-plot-pen "WACs"
	ifelse count WACs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot mean [sum-of-payoffs / (times-played + times-without-partner)] of WACs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	set-current-plot-pen "WADs"
	ifelse count WADs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot mean
		[sum-of-payoffs / (times-played + times-without-partner)]
		of WADs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	set-current-plot-pen "NCs"
	ifelse count NCs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot mean [sum-of-payoffs / (times-played + times-without-partner)] of NCs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	set-current-plot-pen "NDs"
	ifelse count NDs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot mean [sum-of-payoffs / (times-played + times-without-partner)] of NDs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]

	;; Gacs: why don't you just loop over the breeds?
	set-current-plot "CV payoff/tick"
	;;CV payoff/tick per agent, by type
	
	set-current-plot-pen "WACs"
	ifelse count WACs with [(times-played + times-without-partner) > 0] > 1
	and mean [sum-of-payoffs] of WACs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot standard-deviation [sum-of-payoffs / (times-played + times-without-partner)]
		of WACs with [(times-played + times-without-partner) > 0]
		/ mean [sum-of-payoffs / (times-played + times-without-partner)]
		of WACs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	set-current-plot-pen "WADs"
	ifelse count WADs with [(times-played + times-without-partner) > 0] > 1 and mean [sum-of-payoffs] of WADs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot standard-deviation [sum-of-payoffs / (times-played + times-without-partner)] of WADs with [(times-played + times-without-partner) > 0] / mean [sum-of-payoffs / (times-played + times-without-partner)] of WADs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	set-current-plot-pen "NCs"
	ifelse count NCs with [(times-played + times-without-partner) > 0] > 1 and mean [sum-of-payoffs] of NCs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot standard-deviation [sum-of-payoffs / (times-played + times-without-partner)] of NCs with [(times-played + times-without-partner) > 0] / mean [sum-of-payoffs / (times-played + times-without-partner)] of NCs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	set-current-plot-pen "NDs"
	ifelse count NDs with [(times-played + times-without-partner) > 0] > 1 and mean [sum-of-payoffs] of NDs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot standard-deviation [sum-of-payoffs / (times-played + times-without-partner)] of NDs with [(times-played + times-without-partner) > 0] / mean [sum-of-payoffs / (times-played + times-without-partner)] of NDs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	
	
	set-current-plot "mean % time playing game"
	;;mean proportion of lifetime spent playing PD per agent, by type
	
	set-current-plot-pen "WACs"
	ifelse count WACs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot mean [times-played / (times-played + times-without-partner)] of WACs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	set-current-plot-pen "WADs"
	ifelse count WADs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot mean [times-played / (times-played + times-without-partner)] of WADs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	set-current-plot-pen "NCs"
	ifelse count NCs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot mean [times-played / (times-played + times-without-partner)] of NCs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	set-current-plot-pen "NDs"
	ifelse count NDs with [(times-played + times-without-partner) > 0] > 0
	[
		plot-pen-down
		plot mean [times-played / (times-played + times-without-partner)] of NDs with [(times-played + times-without-partner) > 0]
	]
	[
		plot-pen-up
		plot 0
	]
	
	
	
	
	set-current-plot "assortment"
	
	set-current-plot-pen "WACs"
	plot WAC-assortment
	
	set-current-plot-pen "WADs"
	plot WAD-assortment
	
	set-current-plot-pen "NCs"
	plot NC-assortment
	
	set-current-plot-pen "NDs"
	plot ND-assortment
	
	set-current-plot-pen "Total"
	plot total-assortment
	
	
	
	set-current-plot "% paired"
	
	set-current-plot-pen "WACs"
	plot WAC-percent-paired
	
	set-current-plot-pen "WADs"
	plot WAD-percent-paired
	
	set-current-plot-pen "NCs"
	plot NC-percent-paired
	
	set-current-plot-pen "NDs"
	plot ND-percent-paired
	
	set-current-plot-pen "Total"
	plot total-percent-paired
	
	
end                                                         ;;ends "plotting" method

;;_______________________________________



to write-data-to-textfile
	;;Observer method, called by "go" method every 1000 ticks
	
	file-open "walk_away_data.txt"
	;;Note that all data are appended to the file each time this method is called. The file is created if it does not already exist
	;;uniquely identifying simulation run numbers
	file-type behaviorspace-experiment-name
	;;prints name without quotation marks,  V1 = Vector1
	file-write behaviorspace-run-number                        ;;V2
	file-write ticks                                           ;;V3
	;;parameter values
	file-write seed                                            ;;V4
	file-write reward                                          ;;V5
	file-write sucker                                          ;;V6
	file-write temptation                                      ;;V7
	file-write punishment                                      ;;V8
	ifelse offspring-dispersal = "global"                      ;;V9
	[
		file-write 0
		;;0=global dispersal. The parameter dispersal-radius is ignored when dispersal=global.
	]
	[
		file-write 1
		;;1=local dispersal. The parameter dispersal-radius is used ONLY when dispersal=local.
	]
	file-write dispersal-radius                                ;;V10
	file-write world-max-xy                                    ;;V11
	file-write non-player-pay                                  ;;V12
	file-write n-WACs                                          ;;V13
	file-write n-WADs                                          ;;V14
	file-write n-NCs                                           ;;V15
	file-write n-NDs                                           ;;V16
	file-write carrying-capacity                               ;;V17
	file-write error-prob                                      ;;V18
	file-write mu                                              ;;V19
	file-write carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))
	;;V20  ;;d=population density (max agents / number of patches in world)
	;;simulation data
	file-write count WACs                                      ;;V21
	file-write count WADs                                      ;;V22
	file-write count NCs                                       ;;V23
	file-write count NDs                                       ;;V24
	file-write total-deaths                                    ;;V25
	file-write total-births                                    ;;V26
	file-write total-exploitation                              ;;V27
	file-write games-played-count                              ;;V28 ;;total number of strategies played
	file-write error-count / (carrying-capacity * ticks)       ;;V29
	;;observed error rate, note that all agents are exposed to the chance for error each tick, even if they do not end up playing in a game that tick.
	ifelse total-births > 0                                    ;;V30
	[
		file-write mutation-count / total-births                   ;;observed mutation rate
	]
	[
		file-write 0
		;;else, there is no mutation rate because there have been no births
	]
	file-write total-percent-paired                            ;;V31
	file-write WAC-percent-paired                              ;;V32
	file-write WAD-percent-paired                              ;;V33
	file-write NC-percent-paired                               ;;V34
	file-write ND-percent-paired                               ;;V35
	file-write total-assortment                                ;;V36
	file-write WAC-assortment                                  ;;V37
	file-write WAD-assortment                                  ;;V38
	file-write NC-assortment                                   ;;V39
	file-write ND-assortment                                   ;;V40
	
	;;mean proportion of lifetime spent playing PD per agent, by type
	ifelse count WACs with [(times-played + times-without-partner) > 0] > 0   ;;V41
	[
		file-write mean [times-played / (times-played + times-without-partner)] of WACs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	ifelse count WADs with [(times-played + times-without-partner) > 0] > 0   ;;V42
	[
		file-write mean [times-played / (times-played + times-without-partner)] of WADs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	ifelse count NCs with [(times-played + times-without-partner) > 0] > 0    ;;V43
	[
		file-write mean [times-played / (times-played + times-without-partner)] of NCs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	ifelse count NDs with [(times-played + times-without-partner) > 0] > 0    ;;V44
	[
		file-write mean [times-played / (times-played + times-without-partner)] of NDs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	
	
	;;mean payoff/tick per agent, by type
	
	ifelse count WACs with [(times-played + times-without-partner) > 0] > 0  ;;V45
	[
		file-write mean [sum-of-payoffs / (times-played + times-without-partner)] of WACs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	ifelse count WADs with [(times-played + times-without-partner) > 0] > 0  ;;V46
	[
		file-write mean [sum-of-payoffs / (times-played + times-without-partner)] of WADs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	ifelse count NCs with [(times-played + times-without-partner) > 0] > 0  ;;V47
	[
		file-write mean [sum-of-payoffs / (times-played + times-without-partner)] of NCs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	ifelse count NDs with [(times-played + times-without-partner) > 0] > 0  ;;V48
	[
		file-write mean [sum-of-payoffs / (times-played + times-without-partner)] of NDs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	
	;;CV payoff/tick per agent, by type
	ifelse count WACs with [(times-played + times-without-partner) > 0] > 1 and mean [sum-of-payoffs] of WACs with [(times-played + times-without-partner) > 0] > 0  ;;V49
	[
		file-write standard-deviation [sum-of-payoffs / (times-played + times-without-partner)] of WACs with [(times-played + times-without-partner) > 0] / mean [sum-of-payoffs / (times-played + times-without-partner)] of WACs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	ifelse count WADs with [(times-played + times-without-partner) > 0] > 1 and mean [sum-of-payoffs] of WADs with [(times-played + times-without-partner) > 0] > 0  ;;V50
	[
		file-write standard-deviation [sum-of-payoffs / (times-played + times-without-partner)] of WADs with [(times-played + times-without-partner) > 0] / mean [sum-of-payoffs / (times-played + times-without-partner)] of WADs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	ifelse count NCs with [(times-played + times-without-partner) > 0] > 1 and mean [sum-of-payoffs] of NCs with [(times-played + times-without-partner) > 0] > 0  ;;V51
	[
		file-write standard-deviation [sum-of-payoffs / (times-played + times-without-partner)] of NCs with [(times-played + times-without-partner) > 0] / mean [sum-of-payoffs / (times-played + times-without-partner)] of NCs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	ifelse count NDs with [(times-played + times-without-partner) > 0] > 1 and mean [sum-of-payoffs] of NDs with [(times-played + times-without-partner) > 0] > 0  ;;V52
	[
		file-write standard-deviation [sum-of-payoffs / (times-played + times-without-partner)] of NDs with [(times-played + times-without-partner) > 0] / mean [sum-of-payoffs / (times-played + times-without-partner)] of NDs with [(times-played + times-without-partner) > 0]
	]
	[
		file-write -99
	]
	
	file-write mean-cluster-size  ;;V53
	file-write sd-cluster-size    ;;V54
	
	file-print ""
	;;carriage return to move to the next line
	file-close                                                    ;;close the text file
	
end                                                           ;;ends "write-data-to-textfile"

;;_______________________________________
@#$#@#$#@
GRAPHICS-WINDOW
224
10
397
184
-1
-1
13.8
1
10
1
1
1
0
1
1
1
0
11
0
11
1
1
1
ticks
30.0

BUTTON
5
50
71
83
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
79
50
142
83
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
5
10
97
43
seed
seed
0
1000
9.0
1
1
NIL
HORIZONTAL

BUTTON
151
51
214
84
step
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
224
354
307
399
Total births
total-births
0
1
11

PLOT
548
10
773
178
freq of strategies
time
count
0.0
10.0
0.0
40.0
true
true
"" ""
PENS
"WACs" 1.0 0 -11221820 true "" "plot count WACs"
"WADs" 1.0 0 -2674135 true "" "plot count WADs"
"NCs" 1.0 0 -13840069 true "" "plot count NCs"
"NDs" 1.0 0 -955883 true "" "plot count NDs"

MONITOR
486
11
543
56
# WACs
count WACs
0
1
11

MONITOR
486
58
543
103
# WADs
count WADs
0
1
11

MONITOR
487
106
543
151
# NCs
count NCs
0
1
11

MONITOR
487
153
544
198
# NDs
count NDs
0
1
11

MONITOR
224
304
307
349
Total deaths
total-deaths
0
1
11

SLIDER
7
90
99
123
n-WACs
n-WACs
0
100
25.0
1
1
NIL
HORIZONTAL

SLIDER
103
90
195
123
n-WADs
n-WADs
0
100
25.0
1
1
NIL
HORIZONTAL

SLIDER
8
126
100
159
n-NCs
n-NCs
0
100
25.0
1
1
NIL
HORIZONTAL

SLIDER
103
126
195
159
n-NDs
n-NDs
0
100
25.0
1
1
NIL
HORIZONTAL

MONITOR
310
304
464
349
Total Exploitation Events
total-exploitation
0
1
11

SLIDER
7
295
126
328
error-prob
error-prob
0
.1
0.01
.001
1
NIL
HORIZONTAL

MONITOR
393
354
462
399
% error
error-count / (carrying-capacity * ticks)
5
1
11

SLIDER
8
338
100
371
mu
mu
0
.1
0.0
.001
1
NIL
HORIZONTAL

MONITOR
310
354
390
399
% mutation
mutation-count / total-births
4
1
11

CHOOSER
6
423
126
468
offspring-dispersal
offspring-dispersal
"global" "local"
0

SLIDER
6
472
128
505
dispersal-radius
dispersal-radius
0
30
3.0
1
1
NIL
HORIZONTAL

MONITOR
224
404
380
449
Density (agents per patch)
carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))
4
1
11

PLOT
548
350
773
507
mean payoff/tick
time
mean payoff/tick
0.0
10.0
0.0
1.0
true
true
"" ""
PENS
"WACs" 1.0 0 -11221820 true "" ""
"WADs" 1.0 0 -2674135 true "" ""
"NCs" 1.0 0 -10899396 true "" ""
"NDs" 1.0 0 -955883 true "" ""

PLOT
777
350
994
506
mean % time playing game
time
% time playing
0.0
10.0
0.0
1.0
true
true
"" ""
PENS
"WACs" 1.0 0 -11221820 true "" ""
"WADs" 1.0 0 -2674135 true "" ""
"NCs" 1.0 0 -10899396 true "" ""
"NDs" 1.0 0 -955883 true "" ""

SWITCH
441
418
543
451
plot-data
plot-data
0
1
-1000

PLOT
548
179
773
347
CV payoff/tick
time
CV payoff / tick
0.0
10.0
0.0
1.0
true
true
"" ""
PENS
"WACs" 1.0 0 -11221820 true "" ""
"WADs" 1.0 0 -2674135 true "" ""
"NCs" 1.0 0 -10899396 true "" ""
"NDs" 1.0 0 -955883 true "" ""

PLOT
776
10
993
177
assortment
time
% similar partner
0.0
10.0
0.0
1.0
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" ""
"WACs" 1.0 0 -11221820 true "" ""
"WADs" 1.0 0 -2674135 true "" ""
"NCs" 1.0 0 -10899396 true "" ""
"NDs" 1.0 0 -955883 true "" ""

PLOT
776
179
993
348
% paired
time
% paired
0.0
10.0
0.0
1.0
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" ""
"WACs" 1.0 0 -11221820 true "" ""
"WADs" 1.0 0 -2674135 true "" ""
"NCs" 1.0 0 -10899396 true "" ""
"NDs" 1.0 0 -955883 true "" ""

MONITOR
944
109
1001
154
Total
total-assortment
4
1
11

MONITOR
945
278
1002
323
Total
total-percent-paired
4
1
11

SLIDER
101
10
218
43
world-max-xy
world-max-xy
1
99
11.0
1
1
NIL
HORIZONTAL

SLIDER
7
255
99
288
sucker
sucker
-10
100
-1.0
1
1
NIL
HORIZONTAL

SLIDER
7
218
99
251
reward
reward
0
10
3.0
1
1
NIL
HORIZONTAL

SLIDER
101
218
193
251
temptation
temptation
0
10
5.0
1
1
NIL
HORIZONTAL

SLIDER
102
255
194
288
punishment
punishment
0
100
0.0
1
1
NIL
HORIZONTAL

SLIDER
8
379
124
412
non-player-pay
non-player-pay
0
10
0.0
1
1
NIL
HORIZONTAL

TEXTBOX
40
204
170
222
T>R>P>S   &   R > (T+S)/2
10
0.0
1

SLIDER
8
164
149
197
carrying-capacity
carrying-capacity
1
1000
100.0
1
1
NIL
HORIZONTAL

TEXTBOX
140
439
653
549
Directions: \n1. Use the sliders and switches to set the parameter values as you like.\n2. Hit \"setup\".\n3. Hit \"go\" to start and again to stop the simulation.\n4. \"step\" executes a single time step.\n5. See Tools->BehaviorSpace to run the experiments published in Premo and Brown (2019).\n6. Note that data are collected in a .txt file in the same directory as this .nlogo file.\n7. Complete ODD model description is under the Info tab.
11
0.0
1

@#$#@#$#@
# What is it?
Op_Cost_Walk_Away.nlogo
VERSION 1.0 (November 2018)

This version runs on NetLogo 6.0.2. Note that the code may need to be modified to run on later versions of NetLogo.


# Full Model Description

This model description follows the guidelines of the ODD protocol (Grimm et al. 2010).

## Purpose
Aktipis (2004) implemented a spatially explicit, iterated prisoner's dilemma simulation in Starlogo 1.2.2 to test whether unconditional cooperators (always play ‘C’) could persist among, or even outcompete, defectors if cooperators were able to simply "walk away" from partners who defected upon them (played ‘D’). This model is a replication and extension of Aktipis’ basic simulation experiment, which included just four strategies for playing the prisoner’s dilemma (described below). In general, the model allows one to explore the evolution of cooperation in the spatial iterated prisoner’s dilemma under a range of socio-ecological conditions. More specifically, the purpose of the model is to address the following research question: how does population density, error rate, and offspring dispersal affect the success of "walk away" cooperators in the spatial iterated prisoner's dilemma?

## Entities, state variables, and scales
The model has two types of agent entities: mobile players (turtles in NetLogo) and square cells (patches in NetLogo) on a toroidal lattice. Cell size is not ecologically meaningful. There are four types of players, each represented by a different breed. Each type corresponds to one of four strategies (see below). Players are characterized by many state variables (described below). The number of players that can occupy a given patch is unrestricted. The dimensions of the lattice and the number of players of each type can be adjusted. A time step represents the time required to either play one round of the prisoner's dilemma and move one cell to one's left or right (relative to one's heading) in response to a defection or to simply move one cell to one's left or right (relative to one's heading) in search of partner.


### Global Variables

#### seed
  * Specifies the value of random-seed. Each unique simulation run gets its own unique seed value. The seed values used in Premo and Brown (2019) are provided in the experiments available in BehaviorSpace.

#### world-max-xy
  * Sets the square dimensions of the toroidal lattice.
  * Modifying world-max-xy while holding constant the total number of players allows one to adjust population density without affecting the strength of genetic drift relative to the strength of selection.
  * We used values of 11, 24, 49, and 99, which correspond to lattices of 12x12, 25x25, 50x50, and 100x100, respectively.

#### n-WACs, n-NCs, n-WADs, n-NDs
  * Number of players of each breed present at the start of the simulation.

#### carrying-capacity
  * The maximum number of players allowed at the end of any time step during the simulation.
  * Maintained by the enforce-carrying-capacity procedure (described below).
  * We set carrying-capacity to 100 for all of our experiments.

#### error-prob
  * The probability that a cooperator (WAC, NC) will erroneously defect or a defector (WAD, ND) will erroneously cooperate when playing the prisoner’s dilemma. This type of error has been described as the “trembling hand.”
  * We used values of 0.001, 0.01, and 0.1.
  * Note that an error-prob of 0.5 means that there is no difference in strategy between a cooperator and a defector.  Values greater than 0.5 mean that cooperators defect more often than they cooperate and defectors cooperate more often than they defect.

#### mu
  * The probability that one’s offspring inherits a breed other than the parent’s breed due to mutation.
  * We used values of 0, 0.001, and 0.01.

#### non-player-pay
  * The amount added to the energy budget of each player that does not have a partner during the current time step.
  * We held this constant at 0.

#### offspring-dispersal
  * This variable defines how far afield from the parent an offspring disperses at birth.
  * The model allows for one of two types of offspring dispersal:
    1. **global**: the offspring can be placed anywhere within the lattice
    2. **local**: the offspring can placed anywhere within a local neighborhood of cells surrounding the parent. The parent’s local neighborhood is defined as the cells within a radius of dispersal-radius, excluding the parent’s cell.

#### dispersal-radius
  * The extent of the boundary (in patch-widths) within which an offspring must be placed when offspring-dispersal = local.
  * Premo and Brown (2019) holds dispersal-radius constant at 3.

#### plot-data
  * This switch turns on visual plots of assortment, CV payoff/tick, % paired, mean payoff/tick, and mean % time playing game.  The simulation runs faster when this switch is turned off.


#### Prisoner’s dilemma payoffs
To be consistent with the assumptions of prisoner's dilemma the four PD payoffs must meet both of the following conditions: T > R > P > S, and R > (T+S)/2.

##### Temptation to cheat payoff (T)
  * Payoff to ego when ego defects and partner cooperates.
  * We used a value of 5, as in Aktipis (2004).

##### Reward for mutual cooperation payoff (R)
  * Payoff to ego when ego cooperates and partner cooperates.
  * We used a value of 3, as in Aktipis (2004).

##### Punishment for mutual defection (P)
  * Payoff to ego when ego defects and partner defects.
  * We used a value of 0, as in Aktipis (2004).

##### Sucker’s payoff (S)
  * Payoff to ego when ego cooperates and partner defects.
  * We used a value of -1, as in Aktipis (2004).


#### Other global variables used for code verification, data analysis, or visualization (described further with comments in the source code):
total-exploitation, total-births, total-deaths, error-count, mutation-count, games-played-count, WAC-won, WAD-won, NC-won, ND-won, WAC-percent-paired, WAD-percent-paired, NC-percent-paired, ND-percent-paired, total-percent-paired, WAC-assortment, WAD-assortment, NC-assortment, ND-assortment, total-assortment, mean-cluster-size, sd-cluster-size



### 4 player types (i.e., four different “breeds” in NetLogo)
#### "Naive" Cooperator (breed=NCs)
  * When playing prisoner’s dilemma, cooperate with probability (1 - error-prob) and defect with probability error-prob. Do not move in response to a partner's defection.

#### "Naive" Defector (breed=NDs)
  * When playing prisoner’s dilemma, defect with probability (1 - error-prob) and cooperate with probability error-prob. Do not move in response to a partner's defection.

#### "Walk Away" Cooperator (breed=WACs)
  * When playing prisoner’s dilemma, cooperate with probability (1 - error-prob) and defect with probability error-prob. Respond to a partner's defection by moving one cell to one's left or right.

#### "Walk Away" Defector (breed=WADs)
  * When playing prisoner’s dilemma, defect with probability (1 - error-prob) and cooperate with probability error-prob. Respond to a partner's defection by moving one cell to one's left or right.



### Player State Variables
#### xcor, ycor
* The x, y coordinate of each player marks its location on the lattice. The relevant scale for location in this model is the cell (or patch)—a player can only interact with other players that inhabit the same cell, and all player movement is between cells rather than within a cell.

#### heading
  * Players are initialized with one of the four cardinal headings (0, 90, 180, 270).
  * Heading is updated each time a player moves (the agent's heading is reset to face the direction in which it moved: 0, 90, 180, or 270).

#### energy
  * This represents the energy budget of the player.
  * At initialization, this value is pulled from uniform distribution bound by 0 and 50, not including 50.
  * This dynamic state variable is updated under three conditions:
    1. As a result of playing the prisoner’s dilemma, the relevant payoff is added to energy (note that the payoff can be a negative value)
    2. 10 units are subtracted from the energy budget of any player selected by the enforce-carrying-capacity submodel (described in more detail below)
    3. When a player reproduces, its current energy is split evenly with its new offspring

#### strategy
  * 1 = cooperate (play “C”) in the prisoner's dilemma
  * 0 = defect (play “D”) in the prisoner's dilemma
  * The value set at the start of each time step corresponds to the player’s breed (for WAC & NC, strategy = 1, and for WAD & ND, strategy = 0).
  * The value set at the start of the time step changes only if a player makes an error. In the event of an error, cooperators change their strategy to 0 and defectors change their strategy to 1 (see more in **Go** description).

#### color
  * Color is used to indicate the breed of the player: WACs = cyan, WADs = red, NCs = lime, and NDs = orange
  * Player color changes to grey or white when agents are paired during the course of a time step, but reverts to the breed's color at the beginning of each time step.
  * Color is used only for visual debugging.

#### size
  * This static state variable is purely cosmetic. It is set to 2 for all players, regardless of breed.

#### tried-to-pair
  * The value of this variable indicates whether the player has already attempted to find a partner during the current time step. This variable serves as a fail-safe to ensure that each player cannot run the pair submodel more than once per time step.
  * The value is set to 0 for all players at the start of each time step.
  * When a player runs the pair submodel, its tried-to-pair is set to 1 regardless of whether the player finds a partner in its cell.

#### partner
  * If an unpaired player (ego) finds another unpaired player in its cell, ego sets its partner to that player and that player sets its partner to ego. The agent set as ego’s partner will serve as ego’s opponent (and vice versa) when playing the prisoner’s dilemma during the current time step.
  * All players set partner to “nobody” at the start of each time step.  Thus, players do not remember their partners from the previous time step.

#### played-already
  * This variable prevents paired players from playing the prisoner’s dilemma more than once per time step.
  * All players set played-already to 0 at beginning of each time step. Players set this value to 1 as soon as they play the prisoner’s dilemma to ensure that they cannot play the PD more than once per time step.

#### moving
  * The value of this variable indicates whether the player should run the move submodel (described below) during the current time step.
  * 0 = do not move, 1 = move
  * All players set moving to 0 at the beginning of each time step.
  * A player sets moving to 1 under two conditions:
    1. The player did not have a partner during the current time step.
    2. The player is a WAC or WAD whose partner defected during play of the prisoner’s dilemma.

#### potential-breed-list
  * The list of breeds that can be assigned to a parent’s offspring as a result of mutation during reproduction.
  * The list contains the breeds present at the beginning of the simulation, minus the parent’s breed.  The maximum length of this list is 3, but it can be a short as 1.
  * The offspring is given one of the breeds on this list (chosen randomly) instead of its parent’s breed.

#### player state variables used for data analysis and visualization
Each player keeps track of the number of other players within dispersal-radius (cluster-size). Players keep track of the cumulative number of times they play and the cumulative number of times they fail to find a partner. They also keep a running total of payoffs from play in the prisoner's dilemma. These data are used to calculate parameters used in three figures displayed on the interface: mean payoff/tick, CV payoff/tick, mean % time playing game.


## Process overview and scheduling
The **Go** procedure provides the schedule of procedures called during each time step. No player moves on to the next procedure until all eligible players complete the current procedure. The “ask” primitive ensures that player order is randomized during each procedure. The names of _**submodels**_ are bolded and italicized.  Each submodel is described in more detail below in the “Submodels” section of the model description.

At the start of the **Go** procedure, all players are asked to reset the values of player state variables that may have been modified during the previous time step (e.g., color, partner, moving, tried-to-pair, and played-already) to their default values. Next, every player who does not already have a partner (partner = nobody) runs the _**pair**_ submodel once. Every player that does not find a partner is asked to add 1 to its times-without-partner tally and add non-player-pay to its energy budget. Every player is then asked to compare a real number drawn randomly from a uniform distribution bound by 0 and 1 to error-prob. If the random value is less than error-prob, the player is asked to change its strategy from 1 to 0 or vice versa (from 0 to 1 if its strategy is 0 at the start of the time step). Next, those players who have a partner (partner != nobody) and have not yet played the prisoner’s dilemma in the current time step (played-already = 0) are asked to run the _**play**_ submodel. After all eligible players finish the _**play**_ submodel, those players whose energy has dropped to (or below) 0 die. Next, those players who need to move (moving = 1) are asked run the _**move**_ submodel. Then, those players with energy greater than or equal to 100 are asked to _**reproduce**_. If the population size exceeds the carrying-capacity after all eligible players have reproduced, the observer runs _**enforce-carrying-capacity**_ until the number of players equals carrying-capacity. Next, all cooperators (WACs, NCs) are asked to set strategy equal to 1 and all defectors (WADs, NDs) are asked to set strategy equal to 0. This ensures that errors made in the current time step are not carried forward into the next time step. The last few procedures in **Go** concern data collection, plotting values to the interface, and enforcing the stop conditions of the simulation. When mu = 0, simulations stop when there is only one breed remaining in the population. When mu > 0, simulations stop after 500,000 time steps (ticks). **Go** is repeated until the appropriate stop condition is met.


## Design concepts

The _basic principle_ addressed in this model is the robustness of contingent movement as a strategy that could promote the evolution of cooperation in a spatially explicit iterated prisoner's dilemma. Premo and Brown (2019) explores how various socio-ecological conditions affect the evolutionary success of "walking away" from defection in the spatially explicit prisoner's dilemma. We varied population density (by adjusting the size of the lattice while holding carrying capacity constant), the probability of committing an error while playing the prisoner’s dilemma, the probability of mutation during reproduction, and the geographic extent of offspring dispersal.

As one might suspect, player _interaction_ is a key component of this model. Players must be located on the same patch in order to play against each other in the prisoner’s dilemma. Given that players employ a random walk (i.e., "sidestep") to search for PD partners, the likelihood of finding a partner to play is affected by population density.

As long as carrying capacity is sufficiently high to render drift a weak force of evolutionary change relative to the strength of selection, breeds that increase in frequency are evolutionarily adaptive for a given socio-ecological context, which includes the relative frequency of other breeds. In short, the relative frequency of each breed is related to its relative _fitness_.

All players share the _objective_ of finding a partner and playing the prisoner's dilemma in order to gain energy required to reproduce. One might say that WACs and WADs have an additional objective—to respond to partners that defect during the prisoner’s dilemma. However, the ultimate _objective_ for all players is to gain energy so as to reproduce as frequently as possible.

Players have limited _sensing_ abilities. A player can _sense_ other players that inhabit the same patch it inhabits.

The model is initiated with a user-defined seed, which affects the _stochastic_ processes in the model. The model includes a number of stochastic processes, including the initial assortment of players on the lattice, their initial energy values, “trembling hand” errors in strategy, the direction of player movement, and mutation during reproduction. Also note that players are chosen randomly (and with replacement) by the enforce-carrying-capacity submodel.

Important _observations_ in this model include the proportion of times each breed “wins” (i.e., evolves to fixation in the population at the expense of all other strategies included at the start of the simulation) when mu = 0, or is "successful" (represented by at least 95 agents after 500,000 time steps) in simulations with mu > 0. The model records many other types of observations that were not discussed in the paper, some of which are displayed on the interface as monitors and graphs. Some important metrics include the relative frequencies of each breed, the total number of births and deaths, mean payoff per tick, and mean % of time spent playing the PD, and mean "cluster" size. Data collected intermittently (every 1000 ticks) during the simulation run is written to a file named “walk_away_data.txt” in the same folder that contains this nlogo file. NetLogo will prompt you for the directory in which to save the results from BehaviorSpace experiments. Simulations without mutation (mu = 0) run until just one breed remains. Simulations with mutation (mu > 0) run for 500,000 time steps.


## Initialization
Table 1 provides the parameter values used to initialize the simulations reported in Premo and Brown (2019). These values can be found in the published paper as well as in the experiments saved in BehaviorSpace. At the start of each simulation, players are randomly distributed about the lattice, each player’s energy is set to a unique random value drawn from a uniform distribution bound by 0 and 50 (not including 50), and each player’s heading is set to one of the four cardinal points on the compass (0, 90, 180, and 270) with equal probability. Cooperators (NC and WAC) start with strategy=1 and defectors (ND and WAD) start with strategy=0.

Table 1. Parameter values used to initialize the simulations reported in Premo and Brown (2019).
**Parameter**  _______________________  **Value(s)**
carrying-capacity . . . . . . . . . . . . . . . . . . . . . . . . . . . .100
reward . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .   3
temptation . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  5
sucker . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . -1
punishment . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 0
dispersal-radius . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 3
non-player-pay . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 0
world-max-xy . . . . . . . . . . . . . . . . . . . . .  11, 24, 49, 99
error-prob . . . . . . . . . . . . . . . . . . . . . .  0.001, 0.01, 0.1
mu . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  0, 0.001, 0.01
offspring-dispersal . . . . . . . . . . . . . . . . .  global, local


## Replication
To replicate the findings presented in Premo and Brown (2019), first save the NetLogo model to your computer. Then run the experiments listed in BehaviorSpace, accessed from the Tools drop-down menu.

## Input Data
The model does not require any input data or additional files to initialize a simulation run.


## Submodels (listed in procedural order)

### pair
At the start of this submodel, the player sets tried-to-pair equal to 1, which disallows them from running _**pair**_ more than once per time step. The prisoner’s dilemma is played between 2 partners. Partners are players located on the same cell in the lattice. Each player who does not have a partner and who hasn't already attempted to pair with a partner during the current time step, sets its _partner_ to one of the other partnerless players on the same patch. The player identified as _partner_ is then asked to set its _partner_ to ego. These two players are now paired, and they will play the prisoner’s dilemma against each other later in the time step. If there are no other players on ego’s patch or if all of the other players on ego’s patch already have partners, then ego will remain un-paired during the current time step. All players that do not find a partner to pair with set moving to 1, making them eligible to run _**move**_ later in the time step.

### error
Every agent (including those that are unpaired, though it doesn't matter in the end for them because they won't be playing the prisoner's dilemma during the current time step) is asked to compare a real number drawn randomly from a uniform distribution bound by 0 and 1 to error-prob. If the random value is less than error-prob, the player changes its strategy from 1 to 0 or from 0 to 1, as the case may be. Note that in either case the strategy is reset to correctly match the agent's breed near the end of the time step, well after all paired agents have played the prisoner's dilemma.

### play
Each player that has a partner (partner != nobody) and has not already played the prisoner’s dilemma during the current time step (played-already=0) compares its strategy to the strategy of its partner and updates its energy by adding the relevant prisoner's dilemma payoff amount (reward, temptation, punishment, or sucker's) to its energy budget. If ego is a WAC or WAD and its partner defected (i.e., its partner’s strategy = 0), ego sets moving to 1, making itself eligible to run _**move**_ later in the time step. Next, ego asks its _partner_ to update its energy by adding the relevant prisoner’s dilemma payoff amount to its current energy level. If _partner_ is a WAC or WAD and ego’s strategy = 0, then the _partner_ sets moving to 1 to retaliate in response to ego’s defection. Both players are marked as having played the prisoner’s dilemma during the current time step (that is, both set played-already=1), preventing them from playing the prisoner’s dilemma more than once during the time step.

### move
Every player with moving = 1 moves one patch to the right or one patch to the left (each direction is chosen with equal probability--.5 to the right and .5 to the left) of its current cell. If the player moves to the left, it subtracts 90 from its heading so as to face the direction it just moved. If the player moves to the right, it adds 90 to its heading so as to face the direction it just moved. After moving to the right or left, the player sets moving = 0 to ensure that it cannot move more than once per time step.

### reproduce
Players reproduce asexually. Players with energy >= 100 are asked to run _**reproduce**_. First, the parent halves its energy. Next, the parent compares a random floating point value between 0 and 1 to mu. If the random value is less than mu, the parent hatches a new player (an offspring) that adopts a breed drawn randomly from the parent’s potential-breeds-list. If the random value is greater than mu, then the parent hatches a new player (an offspring) with the same breed as the parent. At birth, the offspring’s energy is equivalent to its parent’s energy, which was halved at the start of the _**reproduce**_ submodel. There are two different types of offspring dispersal. Offspring can be placed randomly throughout the entire lattice (offspring-dispersal = global) or randomly within a local neighborhood around the parent (offspring-dispersal = local), where the size of the local neighborhood is defined by dispersal-radius.

### enforce-carrying-capacity
This submodel runs for as long as the number of players in the population (after play and reproduction are completed) exceeds the carrying-capacity of the environment. The observer randomly selects one player and then subtracts ten units from its energy. If the selected player’s energy is less than or equal to zero as a result of this reduction, it dies and is removed from the simulation. If the population size remains greater than carrying-capacity after this player has been selected and its energy reduced, the entire process is repeated (i.e., a player is randomly chosen from the population and its energy is reduced by 10 units). This procedure continues iteratively until enough players have died to return the population size to carrying-capacity. Although all players have an equal probability of being selected during each iteration of this submodel, players with lower energy values are on average more likely to die due to density dependent mortality than players with higher energy values.


## References
Aktipis, C. A. (2004) Know when to walk away: contingent movement and the evolution of cooperation. _Journal of Theoretical Biology_ 231(2):249-260.

Grimm, V., et al. (2010) The ODD protocol: A review and first update. _Ecological Modelling_ 221:2760-2768.

Premo, L. S. and J. R. Brown (2019) The opportunity cost of walking away in the spatial iterated prisoner's dilemma. _Theoretical Population Biology_










# Contact Information

This model was programmed by Luke Premo (Department of Anthropology, Washington State University and the Department of Human Evolution, Max Planck Institute for Evolutionary Anthropology) with conceptual contributions by Justin Brown (Department of Anthropology, Washington State University).

One of the best things about computer simulation programs is that they can often be improved with the help of other researchers. The more sets of eyes that pass over the code, the better. So, please, have a look at the source code under the "info" tab. Any comments, questions, or corrections are always welcome. You can contact me at:

Luke Premo
 Department of Anthropology
 Washington State University
 Pullman, WA 99164-4910
luke.premo at wsu dot edu
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="2way_NC_ND_table3" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
  <experiment name="2way_WAC_ND_table3" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
  <experiment name="2way_WAD_ND_table3" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
  <experiment name="2way_WAC_WAD_table3" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
  <experiment name="2way_WAC_NC_table3" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
  <experiment name="2way_NC_WAD_table3" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
  <experiment name="3way_NC_WAD_ND_table7" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="33"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="33"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="33"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
  <experiment name="4way_table2" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
  <experiment name="4way_with_mutation_table4" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="500001"/>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0.001"/>
      <value value="0.01"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="100"/>
  </experiment>
  <experiment name="4way_with_mutation_local_dispersal_table6" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="500001"/>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0.001"/>
      <value value="0.01"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="100"/>
  </experiment>
  <experiment name="4way_with_local_dispersal_table5" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="25"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;local&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
  <experiment name="1way_NC_table9" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <metric>mean-cluster-size</metric>
    <metric>sd-cluster-size</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
      <value value="&quot;local&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="100"/>
  </experiment>
  <experiment name="1way_WAC_table9" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <metric>mean-cluster-size</metric>
    <metric>sd-cluster-size</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;global&quot;"/>
      <value value="&quot;local&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="11"/>
      <value value="24"/>
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="100"/>
  </experiment>
  <experiment name="3way_NC_WAD_ND_with_local_dispersal_table8" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>WAC-won</metric>
    <metric>WAD-won</metric>
    <metric>NC-won</metric>
    <metric>ND-won</metric>
    <metric>count WACs</metric>
    <metric>count WADs</metric>
    <metric>count NCs</metric>
    <metric>count NDs</metric>
    <metric>total-deaths</metric>
    <metric>total-births</metric>
    <metric>total-exploitation</metric>
    <metric>games-played-count</metric>
    <metric>error-count / (carrying-capacity * ticks)</metric>
    <metric>mutation-count / total-births</metric>
    <metric>carrying-capacity / ((max-pxcor + 1) * (max-pycor + 1))</metric>
    <metric>total-percent-paired</metric>
    <metric>WAC-percent-paired</metric>
    <metric>WAD-percent-paired</metric>
    <metric>NC-percent-paired</metric>
    <metric>ND-percent-paired</metric>
    <metric>total-assortment</metric>
    <metric>WAC-assortment</metric>
    <metric>WAD-assortment</metric>
    <metric>NC-assortment</metric>
    <metric>ND-assortment</metric>
    <metric>mean-cluster-size</metric>
    <metric>sd-cluster-size</metric>
    <enumeratedValueSet variable="reward">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="sucker">
      <value value="-1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="temptation">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="punishment">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="non-player-pay">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WACs">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-WADs">
      <value value="33"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NCs">
      <value value="33"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NDs">
      <value value="33"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrying-capacity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-data">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="offspring-dispersal">
      <value value="&quot;local&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="dispersal-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mu">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="error-prob">
      <value value="0.001"/>
      <value value="0.01"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="world-max-xy">
      <value value="49"/>
      <value value="99"/>
    </enumeratedValueSet>
    <steppedValueSet variable="seed" first="1" step="1" last="1000"/>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
