#!//anaconda/bin/python

import csv
import sys, os, re
import numpy as np

# import matplotlib
# from matplotlib import *
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import contour_rc
from contour_rc import *

"""
interactively, call pythonw.
From pythonw, say

from contour import *
fig1,fig2 = pic()

fig1.show()
fig2.show()

fig1.savefig('f1')


If you killed fig1 and want to see it again, you have to recreate it by
fig1,fig2 = pic()

"""

def utility(wealth, profit, failure_rate, horizon):
	"""
	An investor uses this function to decide where to invest.
	She has an initial wealth, has some time horizon in mind.
	Then she looks at what is yearly profit and the yearly failure rate
	and plugs into this (questionable) formula.
	"""
	return((wealth + horizon * profit)*((1-failure_rate+0.0)**horizon))

def pic():
	"""
	Plot the utility as a function of profit and failure-rate/
	(Wealth and horizon are fixed in contour_rc.py.)
	"""
	delta_x = 10000
	delta_y = 0.01
	
	x = np.arange(-5000, 10000, delta_x)
	y = np.arange(0.01, 0.1, delta_y)
	X, Y = np.meshgrid(x, y)
	Z = utility(wealth, X, Y, horizon)

	fig1, ax1 = plt.subplots()
	# ax1.contour(X,Y,Z)
	CS1 = ax1.contour(X,Y,Z) # in case you want to customize CS1

	fig2, ax2 = plt.subplots()
	CS2 = ax2.contourf(X,Y,Z) # filled by default colors

	# CS1, CS2 are not used.
	return fig1, fig2


# just so we can run this as a program
def main():
	progname = os.path.basename(sys.argv[0])
	usage_str = """Usage: %s 
	""" % (progname)

	if 1 != len(sys.argv):
		print(usage_str, file=sys.stderr)
		sys.exit(1)

	f1,f2 = pic()

	f1.savefig("contour-plot")
	f2.savefig("filled-contour-plot")
	plt.show() # Blocks, shows both.

# The executed part

if __name__ == "__main__":
    main()
