#!/usr/bin/env python3

in_file = "num-flocks.csv"

out_file = "num-flocks out.csv"

fieldnames = [
	"[run number]",
	"[step]",
	# "ticks",
	"length flock-list"
	]
delimiter = ','
# delimiter = '\t' # for readability
