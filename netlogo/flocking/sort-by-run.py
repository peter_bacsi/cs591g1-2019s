#!/usr/bin/env python3

import csv
import sys, os, re

from num_flocks_rc import *

def csv2dicts(filename):
	data = []
	with open(filename, newline='') as csvfile:
		dr = csv.DictReader(csvfile)
		for row in dr:
			data.append(row)
	return data

def dicts2csv(data, filename, fieldnames, delimiter=','):
	with open(filename, 'w', newline='') as csvfile:
		
		dw = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter = delimiter,
							extrasaction='ignore') # allows to omit entries not in fieldnames
		dw.writeheader()
		for row in data:
			try:
				dw.writerow(row)
			except ValueError:
				continue
			
def main():
	progname = os.path.basename(sys.argv[0])
	usage_str = """Usage: %s (make sure the input file has only the column headers) 
	""" % (progname)

	if 1 != len(sys.argv):
		print(usage_str, file=sys.stderr)
		sys.exit(1)

	data = csv2dicts(in_file)
	if 0 == len(data):
		return
	"""For some reason, the order of the ordered dictionaries can be different from the original order of the columns.
	If this is a problem, give the fieldnames below explicitly.
	"""
	data.sort(key = lambda line: (int(line["[run number]"]),int(line["[step]"])) )

	dicts2csv(data,out_file, fieldnames, delimiter=delimiter)
	

# The executed part

if __name__ == "__main__":
    main()
