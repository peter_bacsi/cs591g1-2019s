#!/usr/bin/env python3

"""Right now this program does not do anything interesting.
"""

import csv
import sys, os, re

import processing_config
from processing_config import *

# mylib = os.path.join(os.environ["HOME"], "src", "python")
# sys.path.append(mylib)
# from Util import *

def csv2lists(filename):
	data = []
	with open(filename, newline='') as csvfile:
		dr = csv.reader(csvfile)
		for row in dr:
			data.append(row)
	return data

def lists2csv(data,filename,fieldnames):
	with open(filename, 'w', newline='') as csvfile:
		dw = csv.writer(csvfile, fieldnames=fieldnames)
		dw.writeheader()
		for row in data:
			dw.writerow(row)
			
def main():
	"""Eventually, we should use a configuration file instead of command-line arguments.
	"""
	progname = os.path.basename(sys.argv[0])
	usage_str = """Usage: %s <in_file> <out_file>
	Read a csv file into a list of dictionaries using the csv module, and write it out.
	Sorting by the run number field.
	Use the csv library module.
	""" % (progname)

	if 2 != len(sys.argv):
		print(usage_str, file=sys.stderr)
		sys.exit(1)

	in_file, out_file = sys.argv

	data = csv2lists(in_file)
	if 0 == len(data):
		return

	lists2csv(data, )
	
	# for line in data:
	# 	print(line)

# The executed part

if __name__ == "__main__":
    main()

