#!//anaconda/bin/python

import csv
import sys, os, re
import numpy as np

import matplotlib.pyplot as plt
import processing_config
from processing_config import *

def csv2dicts(filename):
	data = []
	with open(filename, newline='') as csvfile:
		dr = csv.DictReader(csvfile)
		for row in dr:
			data.append(row)
	return data

def dicts2csv(data, filename,fieldnames):
	with open(filename, 'w', newline='') as csvfile:
		
		dw = csv.DictWriter(csvfile, fieldnames=fieldnames)
		dw.writeheader()
		for row in data:
			dw.writerow(row)
			
def main():
	progname = os.path.basename(sys.argv[0])
	usage_str = """Usage: %s 
	""" % (progname)

	if 1 != len(sys.argv):
		print(usage_str, file=sys.stderr)
		sys.exit(1)

	data = csv2dicts(in_file)
	if 0 == len(data):
		return
	"""
	The order of the ordered dictionaries can be different from
	the original order of the columns.
	If this is a problem, give the fieldnames below explicitly.
	"""
	# data.sort(key = lambda line: (int(line["[step]"]), int(line["[run number]"])) )

	"""
	10 runs, each going 500 steps.
	Value of the variable in question in a list of length 500 of lists of length 10.
	"""
	stdev_heading_lists = [[0 for j in range(10)] for i in range(501)]
	mean_flockmate_num_lists = [[0 for j in range(10)] for i in range(501)]

	for line in data:
		step = int(line["[step]"])
		run = int(line["[run number]"])
		# runs start from 1, so subtract 1 for the array index.
		stdev_heading_lists[step][run-1] = float(line["standard-deviation [heading] of turtles"])
		mean_flockmate_num_lists[step][run-1] = float(line["mean [count flockmates] of turtles"])

	# numpy for statistics.
	np_stdev_heading_lists = np.array(stdev_heading_lists)
	# Mean and stdev over the 10 runs (second coordinate is axis 1).
	mean_stdevs = np.mean(np_stdev_heading_lists, axis = 1)
	std_stdevs = np.std(np_stdev_heading_lists, axis = 1)
	
	np_mean_flockmate_num_lists = np.array(mean_flockmate_num_lists)
	mean_flockmate_nums = np.mean(np_mean_flockmate_num_lists, axis = 1)
	std_flockmate_nums = np.std(np_mean_flockmate_num_lists, axis = 1)
	
	# plt.plot(mean_flockmate_nums)
	# plt.ylabel('mean flockmate numbers by step')
	# plt.show()

	# fig = plt.figure()
	# x = range(501)
	# y = mean_flockmate_nums
	# plt.errorbar(x,y, yerr = std_flockmate_nums, errorevery = 20)
	# plt.show()

	# The object that plt works on is implicit, best understood when used interactively.
	fig = plt.figure()
	x = range(501)
	y = mean_stdevs
	# Without error bar:
	# plt.plot(x,y)
	plt.errorbar(x,y, yerr = std_stdevs, capsize = 2, # Size of caps at the end of errorbars
				 errorevery = 20)
	plt.show() # This will block, and you cannot use fig.show().
	fig.savefig("stdevs-fig")
	

# The executed part

if __name__ == "__main__":
    main()
