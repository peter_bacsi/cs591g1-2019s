#!/usr/bin/env python3

in_file = "headless flocking experiment-table.csv"

out_file = "flocking experiment-table out.csv"

fieldnames = [
	"[run number]",
	"[step]",
	"standard-deviation [heading] of turtles",
	"count turtles with [any? flockmates]",
	"mean [count flockmates] of turtles",
	"mean[min [distance myself] of other turtles] of turtles",
	"max-cohere-turn",
	"max-separate-turn",
	"vision",
	"minimum-separation",
	"population",
	"max-align-turn"
	]
