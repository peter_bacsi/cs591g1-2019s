#!/usr/bin/env python3

import csv
import sys, os, re

import processing_config
from processing_config import *

# mylib = os.path.join(os.environ["HOME"], "src", "python")
# sys.path.append(mylib)
# from Util import *

def csv2dicts(filename):
	data = []
	with open(filename, newline='') as csvfile:
		dr = csv.DictReader(csvfile)
		for row in dr:
			data.append(row)
	return data

def dicts2csv(data,filename,fieldnames):
	with open(filename, 'w', newline='') as csvfile:
		
		dw = csv.DictWriter(csvfile, fieldnames=fieldnames)
		dw.writeheader()
		for row in data:
			dw.writerow(row)
			
def main():
	progname = os.path.basename(sys.argv[0])
	usage_str = """Usage: %s
	Read a csv file into a list of dictionaries using the csv module, and write it out.
	processing_config.py contains the filenames.
	
	Sorting by the step number and run number fields.

 	Use the csv library module.
	""" % (progname)

	if 1 != len(sys.argv):
		print(usage_str, file=sys.stderr)
		sys.exit(1)

	data = csv2dicts(in_file)
	if 0 == len(data):
		return
	"""For some reason, the order of the ordered dictionaries can be different from the original order of the columns.
	If this is a problem, give the fieldnames below explicitly.
	"""
	data.sort(key = lambda line: (int(line["[run number]"]),int(line["[step]"])))

	# fieldnames =list(data[0].keys())
	# fieldnames from processing_config.py	
	# sys.stderr.write(", ".join(fieldnames)+"\n")
	
	dicts2csv(data, out_file, fieldnames)

	# for line in data:
	# 	print(line)

# The executed part

if __name__ == "__main__":
    main()

