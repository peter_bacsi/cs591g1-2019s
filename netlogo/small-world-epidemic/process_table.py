#!/usr/bin/env python3
import csv
import sys, os, re, importlib

"""
The current program needs no change when the experiment is changed, and
table_process_config.py needs only one line to change,
importing the actual experiment configuration, of which there can be several ones.
"""
from table_process_config import *

# mylib = os.path.join(os.environ["HOME"], "src", "python")
# sys.path.append(mylib)
# from Util import *

def main():
	"""Eventually, we should use a configuration file instead of command-line arguments.
	"""
	progname = os.path.basename(sys.argv[0])
	usage_str = """Usage: %s <experiment configuration file>
	
	Uses the parameters in <experiment configuration file>.py (a Python module).

	- Reads a csv file into a list of dictionaries using the csv module.
	Sort by the run number field, since the outputs of different threads are mixed up.
	Output the result.

	- Experiments are repeated a certain number of times.
	Average the values of the measured quantities, over these repetitions.
	Output the result.

	- As we increase the variable varied_field, we are looking for a first record when another
	quantity thresholded_field gets above/below a certain threshold value.
	The value of varied_field when this happens is called the critical value.

	- Output the file containing the critical values.
	Some other files (like the one with name starting with cropped-head) may also be written.
	
	- Uses the csv library module.

	""" % (progname)

	if 2 != len(sys.argv):
		print(usage_str, file=sys.stderr)
		sys.exit(1)
	experiment = sys.argv[1]
	xp = importlib.import_module(experiment)

	data = csv2dicts(xp.in_file, xp.num_ignored_headers)
	if 0 == len(data):
		return

	data.sort(key = xp.sort_by)
	dicts2csv(data, xp.sorted_file, xp.fieldnames)

	avg_data = averages(data, xp.measured_fields, xp.num_repetitions)
	dicts2csv(avg_data, xp.averaged_file, xp.fieldnames_avg)

	crit_values_data = crit_values(avg_data, xp.varied_field, xp.num_values,
								   xp.thresholded_field, xp.threshold, xp.above)
	dicts2csv(crit_values_data, xp.crit_values_file, xp.crit_values_fieldnames)


############ The procedures used.
	
"""Returns a list of records, one for each row.  Each record is an (ordered) dictionary
indexed with the row headers.
In what follows, 'data' is always such a list of records.
"""
def csv2dicts(filename, num_ignored_headers):
	# Write a new file without the ignored headers.
	data_list = file2list(filename)
	list2file(data_list[num_ignored_headers:], "cropped-head-"+filename)
	
	data = []
	with open("cropped-head-"+filename, newline='') as csvfile:
		dr = csv.DictReader(csvfile)
		for row in dr:
			data.append(row)
	return data

def dicts2csv(data, filename, fieldnames):
	# Write only the fields we want, ignore the rest.
	with open(filename, 'w', newline='') as csvfile:
		dw = csv.DictWriter(csvfile, fieldnames=fieldnames, extrasaction='ignore')
		# tab may be more readable:
		# dw = csv.DictWriter(csvfile, fieldnames=fieldnames, extrasaction='ignore', delimiter='\t')
		dw.writeheader()  # if we want a header.
		for row in data:
			dw.writerow(row)

"""Each experiment is repeated some number of times.
Average over these repetitions.
"""
def averages(data, measured_fields, num_repetitions):
	out_data = []
	k = int(len(data) / num_repetitions)
	for i in range(k):
		start_index  = num_repetitions  * i
		record = data[start_index]
		for field in measured_fields:
			s = 0
			for j in range(start_index, start_index + num_repetitions):
				s += float(data[j][field]) # It was a string.
			# This changes the data but we don't need the originals anymore:
			record[field] = s / (num_repetitions + 0.0) 
		out_data.append(record)
	return out_data

"""
The data is a list of records in which several fields change, one at a time.
The variable called varied_field goes through a fixed number of increasing values
for each fixed set of values for the other variables, while the other fields are fixed.
Find the first value of varied_field
when thresholded_field gets above or below threshold (above if above == True).
Return the list containing the records having these threshold values.
"""

def crit_values(data, varied, num_values, thresholded_field, threshold, above = True):
	out_data = []
	k = int(len(data) / num_values)
	m = 0
	if above:
		for i in range(k):
			for n in range(i*num_values,(i+1)*num_values):
				m = n
				if data[n][thresholded_field] > threshold:
					break
			out_data.append(data[m])
	else:
		for i in range(k):
			for n in range(i*num_values,(i+1)*num_values):
				m = n
				if data[n][thresholded_field] < threshold:
					break
			out_data.append(data[m])
	return out_data

######### Utilities
			
def file2list(file):
    "Makes a list consisting of the lines found in the file."
    
    try: 
        fh = open(file)
    except IOError as detail:
        die("Could not open "+file, detail)
    list = fh.readlines()
    fh.close()
    return list

def list2file(list, file):
    """Writes a list of lines into a file.  Does not add line separators.
    """
    try: 
        fh = open(file, "w")
    except IOError as detail:
        die("Could not open "+file, detail)
    fh.writelines(list)
    fh.close()

########### The executed part

if __name__ == "__main__":
    main()
