#!/usr/bin/env python3

"""
This is the configuration file to be imported by process_table.py.
It sets all the parameters.
"""
in_file_root = "experiment-2-table"

in_file = in_file_root + ".csv"

"""
The first 6 lines of the behavior-space experiments will be ignored.
The 7th one contains the field names as column headers.
"""
num_ignored_headers = 6 

# The columns to keep in the output.
fieldnames = [
	"links-per-person",	
	"resistant-density",
	"[run number]",
	"sick-proportion"
	]

def sort_by(record):
	return (int(record["links-per-person"]),
			float(record["resistant-density"]),
			int(record["[run number]"]))

fieldnames_avg = [
	"links-per-person",	
	"resistant-density",
	"sick-proportion"
	]

sorted_file = in_file_root + "-sorted.csv"

# Averaging measured_fields (possibly more than one) in the list over a number of repetitions.
num_repetitions = 5
measured_fields = ["sick-proportion"]

averaged_file = in_file_root + "-averaged.csv"

"""
We go through the values of the variable varied_field upwards,
looking for the first value when another field gets above/below threshold.
It is below if 'above' == False.
This value is called the critical value.
The result is output to critical_values_file.
"""
lowest_value = 0.2
highest_value = 0.95
increment = 0.05
num_values = 1 + int((highest_value - lowest_value) / increment)
thresholded_field = "sick-proportion"
varied_field = "resistant-density"
threshold = 0.1
above = False  # We want first value getting below the threshold.

crit_values_file = in_file_root + "-crit.csv"

# Column headers in this file.
crit_values_fieldnames = [
	"links-per-person",	
	"resistant-density"
	]

